
jQuery.sap.declare("model.HistoryCart");



model.HistoryCart = ( function ()
{

      
      /******************************************************************/
      //Temp Code
     
          
    HistoryCart = function(data)
      {
        this.orderId  = undefined;
        this.customerId = undefined;
        this.customerName = undefined;
        this.rifOrder = undefined;
        this.basketType = undefined;
        this.destination = undefined;
        this.paymentMethod = undefined;
        this.paymentCondition = undefined;
        this.resa1 = undefined;
        this.resa2 = undefined;
        this.meansShipping = undefined;
        this.totalEvasion = undefined;
        this.appointmentToDelivery = undefined;
        this.deliveryType = undefined;
        this.chargeTrasport = undefined;
        this.IVACode = undefined;
        this.favorite = false;
        this.fullEvasionAvailableDate = undefined;
        this.validDateList = undefined;
        this.requestedDate = undefined;
        this.orderReason = undefined;
        this.positions = [];
        this.billsNotes = [];
        this.salesNotes = [];

        this.update = function(data)
        {
          for(var prop in data)
          {
            this[prop]= data[prop];
          }
        };

        this.create = function(orderId)
        {
          this.orderId= orderId;
          
        };

//        this.addPosition = function(position)
//        {
//          if(!this.positions || this.positions.length === 0)
//            this.positions = [];
//
//          this.positions.push(position);
//        };
        this.setPositions = function(positionsArr)
        {
          if(positionsArr && positionsArr.length > 0)
          {
            this.positions = positionsArr;
          }
        };
        this.getPositions = function()
        {
          if(!this.positions || this.positions.length === 0)
            this.positions = [];

          return this.positions;
        };
        this.getModel = function()
        {
          var model = new sap.ui.model.json.JSONModel(this);
          return model;
        };


      if(data)
        this.update(data);

      return this;
    }
    
    return HistoryCart;
        
     
      
      
      
      
      /*******************************************************************/


})();
