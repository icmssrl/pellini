jQuery.sap.declare("model.Destination");

model.Destination = (function () {

  Destination = function (serializedData) {
    this.address = undefined;
    this.city = undefined;
    this.company = undefined;
    this.country = undefined;
    //    this.country_text = undefined;
    this.customer = undefined; //codice cliente
    this.destinationId = undefined; //codice destinazione
    //    this.destination_text  = undefined;
    //    this.distribution_channel = undefined;
    //    this.division = undefined;
    this.email = undefined;
    this.fax = undefined;
    this.postal_code = undefined;
    this.region = undefined;
    //    this.region_text = undefined;
    //    this.sales_organization = undefined;
    this.telephone = undefined;

    this.initialize = function (codice) {
      this.customer = codice;
    };

    this.getModel = function () {

      var model = new sap.ui.model.json.JSONModel(this);
      return model;

    };

    this.getId = function () {
      return this.destination;
    };


    this.update = function (data) {
      for (var prop in data) {
        this[prop] = data[prop];
      }
    };
    //se viene passato un oggetto, vengono settati i dati, else viene settato solo il codice cliente.
    if (_.isObject(serializedData)) {
      this.update(serializedData);
    } else {
      this.initialize(serializedData);
    }

    return this;
  };
  return Destination;


})();
