jQuery.sap.declare("model.Shippment");

model.Shippment = (function () {

  Shippment = function (serializedData) {
    this.orderId = undefined;
    this.customerId = undefined;
    this.customerName = undefined;
    this.shippmentDate = undefined;
    this.expectedDeliveryDate = undefined;
    this.shippmentId = undefined;
    this.rifNumber = undefined;
    this.shippmentStatus = undefined;
    this.payerName = undefined;
    this.billNumber = undefined;
    this.billDate = undefined;
    this.commercialInvoice = undefined;
    this.accountingInvoice = undefined;
    this.invoiceDate = undefined;
    this.products = [];
    this.mittente = {};
    this.destinatario = {};
      
    this.setMittente = function(address,city,region,regionCode,postal_code,country,email,fax,telephone, name){
        this.mittente.name = name; 
        this.mittente.address = address;
        this.mittente.city = city;
        this.mittente.country = country;
        this.mittente.email = email;
        this.mittente.fax = fax;
        this.mittente.postal_code = postal_code;
        this.mittente.region = region;
        this.mittente.regionCode = regionCode;
        this.mittente.telephone = telephone;
        
    };
      
    this.setDestinatario = function(address,city,region,regionCode,postal_code,country,email,fax,telephone, name){
        this.destinatario.name = name; 
        this.destinatario.address = address;
        this.destinatario.city = city;
        this.destinatario.country = country;
        this.destinatario.email = email;
        this.destinatario.fax = fax;
        this.destinatario.postal_code = postal_code;
        this.destinatario.region = region;
        this.destinatario.telephone = telephone;
    };
      
    this.getProducts = function(orderId)
    {

        var pDefer = Q.defer();
        var pSuccess = function(result)
        {
        if(this.products.length>1)
          this.products = [];
          this.products.push(result.positions.results);
          pDefer.resolve({"items":this.products});
        };
        pSuccess = _.bind(pSuccess, this);

        var pError = function(err)
        {
          this.products = [];
          pDefer.reject(err);
        };
        pError = _.bind(pError, this);

        model.collections.TestOrders.loadTestOrdersById(orderId)
        .then(pSuccess, pError);

      return pDefer.promise;


    };

   
    this.getModel = function () {

      var model = new sap.ui.model.json.JSONModel(this);
      return model;

    };
      
    

    this.getOrderId = function () {
      return this.orderId;
    };
      
    this.getId = function () {
      return this.shippmentId;
    };


    this.update = function (data) {
      for (var prop in data) {
        this[prop] = data[prop];
      }
    };
    //se viene passato un oggetto, vengono settati i dati, else viene settato solo il codice cliente.
    if (_.isObject(serializedData)) {
      this.update(serializedData);
    } else {
      this.initialize(serializedData);
    }

    return this;
  };
  return Shippment;


})();
