jQuery.sap.declare("model.praticheAnomale.RichiestaRicambi");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.praticheAnomale.ProductAP");
jQuery.sap.require("model.collections.praticheAnomale.ProductsAP");

model.praticheAnomale.RichiestaRicambi = (function () {




  RichiestaRicambi = function (serializedData) {

    this.renderType="Richiesta Ricambi";
    this.orderId = undefined;
    this.shippmentId = undefined;
    this.committerId = undefined;
    this.committerName = undefined;
    this.payerName = undefined;
    this.receiverName = undefined;
    this.areaManager = undefined;
    this.territoryManager = undefined;
    this.billNumber = undefined;
    this.billDate = undefined;
    this.commercialInvoice = undefined;
    this.accountingInvoice = undefined;
    this.invoiceDate = undefined;
    this.renderReason = undefined;
    this.appointment = undefined;
    this.notes = undefined;
    this.notesText = undefined;
    this.productsAP = [];
    

    this.initialize = function(newForm)
    {
      this.renderType="RichiestaRicambi";
      this.orderId = newForm.orderId ? newForm.orderId : "";
      this.shippmentId = newForm.shippmentId ? newForm.shippmentId : "";
      this.committerName = newForm.committerName ? newForm.committerName : "";
      this.payerName = newForm.payerName ? newForm.payerName : "";
      this.receiverName = newForm.receiverName ? newForm.receiverName : "";
      this.areaManager = newForm.areaManager ? newForm.areaManager : "";
      this.territoryManager = newForm.territoryManager ? newForm.territoryManager : "";
      this.billNumber = newForm.billNumber ? newForm.billNumber : "";
      this.billDate = newForm.billDate ? newForm.billDate : "";
      this.commercialInvoice = newForm.commercialInvoice ? newForm.commercialInvoice : "";
      this.accountingInvoice = newForm.accountingInvoice ? newForm.accountingInvoice : "";
      this.invoiceDate = newForm.invoiceDate ? newForm.invoiceDate : "";
        
    };



    this.getRenderType = function () {
      return this.renderType;
    };


    this.getModel = function () {

      var model = new sap.ui.model.json.JSONModel(this);

      return model;

    };


    this.getProductsAP = function(shippmentId)
    {
        var thisDefer = Q.defer();
        model.collections.praticheAnomale.ProductsAP.getByShippmentId(shippmentId)
          .then(
            _.bind(function(result){
                this.productsAP = result;
                thisDefer.resolve(this.productsAP);
//                return this.productsAP;
            },this));
        return thisDefer.promise;
    };
      
      
    this.update = function (data) {
      for (var prop in data) {
        this[prop] = data[prop];
      }
      

    };


    if (serializedData) {

      this.update(serializedData);


    }


    return this;
  };
  return RichiestaRicambi;


})();
