
jQuery.sap.declare("model.collections.VisitsTour");

model.collections.VisitsTour = ( function ()
{
  var sellingPoints = [];
  var defer = Q.defer();

  return {

    loadSellingPoints : function()
    {
      var deferId= Q.defer();
     
//      var obj={};
  
      var fSuccess  = function(result)
      {

       
        deferId.resolve(result);
      }
      fSuccess = _.bind(fSuccess, this);

      var fError  = function(err)
      {
//        obj={};
        deferId.reject(err);
      }
      fError = _.bind(fError, this);

      //this.loadPositions()
      //.then(fSuccess, fError);
      $.getJSON("custom/model/mock/data/SellingPoints.json")
            .success(fSuccess)
            .fail(fError);
        
      return deferId.promise;
    },
      
    loadSellingPointsByChannel : function(channel)
    {
      var deferId= Q.defer();
      var obj={};
//      var arr= [];
      var fSuccess  = function(result)
      {
//        var filteredOrders = _.find(result, _.bind(function(item)
//        {
//          //return item.getProductId() === code;
//          return item.customerId === customerId;
//            
//        }, this));
//        arr.push(filteredOrders);
        var res = result[channel];
        obj={"items":res};
          
        deferId.resolve(obj);
      }
      fSuccess = _.bind(fSuccess, this);

      var fError  = function(err)
      {
//        arr=[];
        obj={};
        deferId.reject(err);
      }
      fError = _.bind(fError, this);

      //this.loadPositions()
      //.then(fSuccess, fError);
      $.getJSON("custom/model/mock/data/SellingPoints.json")
            .success(fSuccess)
            .fail(fError);
        
      return deferId.promise;
    },





//    getModel : function()
//    {
//      var model = new sap.ui.model.json.JSONModel();
//      var data = {"orderlist":[]};
//      for(var i = 0; i< orders.length; i++)
//      {
//        data.orders.push(orders[i].getModel().getData());
//      }
//      model.setData(data);
//      return model;
//    }

  };
})();