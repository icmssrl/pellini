jQuery.sap.declare("utils.DateFormat");

utils.DateFormat = {

   formatDate: function (date) {
        var d = date;
        var gg = d.getDate();
        var mm = d.getMonth() + 1;
        var yyyy = d.getFullYear();
        if (gg < 10) {
            gg = "0" + gg;
        }
        if (mm < 10) {
            mm = "0" + mm;
        }
        return gg + "/" + mm + "/" + yyyy;
    },
    
    orarioDaStringa: function (orario) {
        var data = new Date();
        data.setHours(parseInt(orario.split(":")[0]));
        data.setMinutes(parseInt(orario.split(":")[1]));
        data.setSeconds(00);
        return data;
    },

}