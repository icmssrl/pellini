jQuery.sap.declare("utils.Formatter");

utils.Formatter = {

    formatName: function (value) {
        console.log(value);
        return value;
    },

    formatCustomer: function (value) {
        return (!value) ? "Warning" : "None";
    },

    formatCustomerText: function (value) {
        if (value) {
            return "";
        } else {
            return "is not a customer! call him!";
        };
        return "";
    },

    canCreate: function (value) {
        if (value.toUpperCase().indexOf("agent".toUpperCase()) >= 0)
            return true;
        else {
            return false;
        };
    },
    canEdit: function (value) {
        if (value.toUpperCase().indexOf("agent".toUpperCase()) >= 0)
            return true;
        else {
            return false;
        };
    },
    isSelected: function (obj) {
        if (obj !== "")
            return true;
        return false;

    },
    adaptCheckBoxValue: function (val) {
        if (_.isEmpty(val))
            return false;
        return val;
    },

    formatColorCreditWorthiness: function (payedOrder, totalOrder) {
        if (totalOrder > 0) {
            var p = (payedOrder / totalOrder) * 100;
            if (p >= 60) {
                return "Success";
            } else if (p >= 30 && p < 60) {
                return "Warning";
            } else {
                return "Error";
            };
        } else {
            return "None";
        };
    },

    formatPercentage: function (actual, total) {
        if (total > 0) {
            var p = (actual / total) * 100;
            return parseInt(p);
        } else {
            return 0;
        };
    },

    formatDecimalValue: function (val) {
        return val.toFixed(2);
    },

    formatVisibleIcon: function (val) {
        if (typeof val === "undefined") {
            return false;
        } else {
            return true;
        };
    },

    formatAvailable: function (value) {
        if (value === true) {
            return "./custom/img/tick.png";
        } else {
            return "./custom/img/warning.png";
        };
    },

    returnRowStatus: function (value) {
        if (!value) return;
        switch (value) {
            case "aperto":
                return "None";
                break;
            case "chiuso":
                return "None";
                break;
            case "bloccato":
                return "#ff4d4d";
                break;
        };
    },
    
    returnRowStatusByExtract: function (value) {
        if (!value) return;
        switch (value) {
            case "":
                return "None";
                break;
            case "Parziale":
                return "#FAD014";
                break;
            case "Totale":
                return "#ff4d4d";
                break;
        };
    },
    
    returnRowStatusByShippmentDate: function (requestedDate, shippingDate, stralcio) {
        if (typeof requestedDate === 'undefined' || typeof shippingDate === 'undefined' || typeof stralcio === 'undefined') return;
        var toNewDate = function (dateStr) {
            var parts = dateStr.split("/");
            return new Date(parts[2], parts[1] - 1, parts[0]);
        }
        if(shippingDate !== "")
            var newShippingDate = toNewDate(shippingDate);
        if(requestedDate !== "")
            var newRequestedDate = toNewDate(requestedDate);
        if(shippingDate === ""){
            switch (stralcio) {
                case "":
                    return "None";
                    break;
                case "Parziale":
                    return "#ffffb3";
                    break;
                case "Totale":
                    return "#ff4d4d";
                    break;
            };
        }else{
            if(newRequestedDate > newShippingDate){
                return "#99ff99";
            }else{
                return "None";
            }
        }

    },
    
    statoEventi: function (value) {
        var color = "None";
        if (value === "Pianificato") {
            color = "Success";
        } else {
            color = "Warning";
        }
        return color;
    },

    formatValueState: function (wantedDate, futureDate) {
        if (typeof wantedDate === "undefined" || typeof futureDate === "undefined") {
            return "None";
        } else if ((new Date(wantedDate)) >= (new Date(futureDate))) {
            return "Success";
        } else return "None";
    },

    formatDateState: function (requestedDate, shippmentDate) {
        var fromR = requestedDate.split("/");
        var req = new Date(fromR[2], fromR[1] - 1, fromR[0]);
        var fromS = shippmentDate.split("/");
        var shipp = new Date(fromS[2], fromS[1] - 1, fromS[0]);
        if (typeof requestedDate === "undefined" || typeof shippmentDate === "undefined") {
            return "None";
        } else if (shipp > req) {
            return "Warning";
        } else return "None";
    },

    formatColorCreditDebt: function (usedDebt, totalDebt) {
        var d = (usedDebt / totalDebt) * 100;

        if (d >= 80) {
            return "Error";
        } else if (d >= 30 && d < 80) {
            return "Warning";
        } else {
            return "Success";
        };

    },

    formatDateValue: function (value) {
        var res = "";
        if (value) {
                value = new Date(value);
//            if(typeof value === "object"){
                var day = value.getDate();
                var month = (value.getMonth()+1).toString();
                var year = value.getFullYear();
                var dashValue = day + '-' + month+ '-' + year
//            }else{
//                
//            }
            var d = dashValue.split('-');
            res = d[0] + "/" + d[1] + "/" + d[2];
        };
        return res;
    },

    formatDecimal: function (value) {
        if (value !== undefined) {
            var r = parseFloat(value);
            return r.toFixed(2);
        };
        return false;
    },

    returnInverter: function (val) {
        if (val === true) {
            return false
        } else {
            return true;
        };
    },

    allDayEvent: function (val) {
        if (val) {
            val = "X";
        } else {
            val = "";
        };
        return val;
    },

    editOrarioInizio: function (val) {
        if (val) {
            if (val !== "00:01" && val !== "23:59") {
                var ore = val.split(":")[0];
                if (ore.length < 2) {
                    ore = "0" + ore;
                };
                var minuti = val.split(":")[1];
                return (ore + ":" + minuti);
            } else {
                // se è un appuntamento che inizia per 00:01 o termina per 23:59 allora non voglio che compaiano le ore ma ci sarà la X che indica il full day
                return "";
            };
        };
    }
};
