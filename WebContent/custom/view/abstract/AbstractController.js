jQuery.sap.require("utils.Validator");
jQuery.sap.require("utils.ObjectUtils")
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.filters.Filter");

jQuery.sap.declare("view.abstract.AbstractController");


sap.ui.core.mvc.Controller.extend("view.abstract.AbstractController", {
    onInit: function () {
        this.router = sap.ui.core.UIComponent.getRouterFor(this);
        this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
        this.uiModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.uiModel, "ui");
        var user = model.persistence.Storage.session.get("user");
        var userModel = new sap.ui.model.json.JSONModel(user);
        this.getView().setModel(userModel, "user");
    },
    
    handleRouteMatched: function (evt) {
        this.clearValueState();
        this.cssReload();
    },

    onHomePress: function (evt) {
        if (model.Current.getCustomer()) {
            model.Current.removeOrder();
        };
        if(model.persistence.Storage.session.get("currentChannel"))
            model.persistence.Storage.session.remove("currentChannel");
        this.router.navTo("launchpad");
    },

    onAfterRendering: function () {
        this.setElementsToValidate();
        this.clearValueState();
    },

    _getLocaleText: function (key) {
        return this.getView().getModel("i18n").getProperty(key);
    },


    //---------------------Functions to check input field--------------------------

    _checkingValues: [],

    setElementsToValidate: function () {
        var inputsId = _.chain($('input'))
            .map(function (item) {
                var innerIndex = item.id.indexOf("-inner");
                if (innerIndex)
                    return item.id.substring(0, innerIndex);
                return item.id;
            })
            .value();

        var controls = _.map(inputsId, function (item) {
            return sap.ui.getCore().byId(item);
        });
        // console.log("controls" +controls);

        this._checkingValues = _.compact(controls);
        for (var i = 0; i < this._checkingValues.length; i++) {
            this._checkingValues[i].attachChange(null, this.checkInputField, this);
        };
    },
    clearValueState: function () {
        if (this._checkingValues) {
            for (var i = 0; i < this._checkingValues.length; i++) {
                this._checkingValues[i].setValueState("None");
            };
        };
    },

    validateCheck: function () {
        var result = true;
        if (!this._checkingValues || this._checkingValues.length === 0)
            return true;

        for (var i = 0; i < this._checkingValues.length; i++) {
            if (!this.checkInputField(this._checkingValues[i])) {
                if (this._checkingValues[i].data("req") === "true") {
                    result = false;
                };
            };
        };
        return result;
    },

    checkInputField: function (evt) {

        var control = evt.getSource ? evt.getSource() : evt;
        var infoControl = control.data("req");
        var typeControl = control.data("input");
        var correct = true;
        if (infoControl === "true" || control.getValue() !== "") {
            switch (typeControl) {

                default: correct = utils.Validator.required(control);
                break;
            };
        };

        // switch(infoControl)
        // {
        //   "true":
        //     correct = utils.Validator.required(control);
        //     break;
        //   "edit":
        //     var type= control.data("")
        //
        //   default:
        //     // correct = utils.Validator.required(control);
        //     break;
        //
        // }
        return correct;

    },
    getFailedControls: function () {
        var result = [];
        _.forEach(this._checkingValues, _.bind(function (item) {
            if (!this.checkInputField(item)) {
                result.push(item);
            }
        }, this));
        return result;
    },


    //--------------------------------------------------------------------------------------
    onSearch: function (evt) {
        var src = evt.getSource();
        this.searchValue = src.getValue();
        var searchProperty = this.uiModel.getProperty("/searchProperty");

        this.applyFilter(this.searchValue, searchProperty);

    },
    applyFilter: function (value, params) {

        var list = this.getView().byId("list");
        if (!list.getBinding("items").oList || list.getBinding("items").oList.length === 0)
            return;


        var temp = list.getBinding("items").oList[0]; // a template just to recover the data types
        var filtersArr = [];
        // var props = utils.ObjectUtils.getKeys(temp);

        if (!_.isEmpty(params)) {
            if (!_.isArray(params)) {
                params = [params];
            }
            for (var i = 0; i < params.length; i++) {


                switch (typeof (utils.ObjectUtils.getValues(temp, params[i]))) {
                    case "undefined":
                        break;
                    case "string":
                        filtersArr.push(new sap.ui.model.Filter(params[i], sap.ui.model.FilterOperator.Contains, value));
                        break;
                    case "number":
                        filtersArr.push(new sap.ui.model.Filter(params[i], sap.ui.model.FilterOperator.EQ, value));
                        break;
                }

            }
            var filter = new sap.ui.model.Filter({
                filters: filtersArr,
                and: false
            });
            list.getBinding("items").filter(filter);
            return;
        }
        list.getBinding("items").filter();
    },

    onLogoutPress: function (oEvent) {

        if (model.Current && model.Current.getCustomer()) {
            model.Current.removeOrder();
        }
        this.doLogout();
    },

    doLogout: function () {
        //resetto dati
        sessionStorage.clear();
        this.router.navTo("login");
    },

    //funzione back da rivedere
    onBackPress: function (evt) {
        var route = location.hash;
        if (route.indexOf("edit") > 0) {
            this.router.navTo("customerDetail", {
                id: this.customer.getId()
            });
        } else {
            this.router.navTo("launchpad");
        }
    },

    cssReload: function () {
        //** risetto il css
        jQuery.sap.includeStyleSheet("custom/css/customRI.css", "custom_style");
        var division = model.persistence.Storage.session.get("division");
        if (division) {
            //var division = workingUserFromSession.organizationData.results[0].division;
            //      jQuery.sap.includeStyleSheet("custom/css/customRI.css", "custom_style");
        }
        //**
    },




});
