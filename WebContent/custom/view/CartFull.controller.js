jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.Customers");
jQuery.sap.require("model.collections.Positions");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("model.Order");
jQuery.sap.require("model.Current");
jQuery.sap.require("model.Position");
jQuery.sap.require("model.Cart");

view.abstract.AbstractController.extend("view.CartFull", {

  onExit: function () {

  },


  onInit: function () {

    // this.router = sap.ui.core.UIComponent.getRouterFor(this);
    // this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
    view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

    this.valueStateModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.valueStateModel, "vs");
    this.valueStateModel.setProperty("valueState", "");

  },


  handleRouteMatched: function (evt) {

    view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
    var routeName = evt.getParameter("name");

    if (routeName !== "cartFullView") {
      return;
    }

    this.order = model.Current.getOrder();
    if (!this.order) {
      var orderData = model.persistence.Storage.session.get("currentOrder");
      this.order = new model.Order(orderData);
    };
    this.customer = model.Current.getCustomer();
    if (!this.customer) {
      var customerData = model.persistence.Storage.session.get("currentCustomer");
      this.customer = new model.Customer(customerData);
    }
    this.getView().setModel(this.customer.getModel(), "customer");


    this.cartModel = this.order.getModel();
    this.getView().setModel(this.cartModel, "c");


    this.calculateFunction();
  },

  onSave: function () {

        console.log(this.order);
        var positions = this.order.positions;
        for(var pos = 0; pos<positions.length; pos++){
            if(positions[pos].quantity===0 || positions[pos].quantity===undefined){
                sap.m.MessageBox.alert(model.i18n._getLocaleText("0_QUANTITY"), {
                    title: model.i18n._getLocaleText("WARNING")                                      
                });
                return;
            }
        }
        var that = this;
        var arr = [];
        
      
      ///////****************************************************/////////////
                    
                    sap.m.MessageBox.show(
                        model.i18n._getLocaleText("CONFIRM_CART_SAVE_TEXT"), {
                          icon: sap.m.MessageBox.Icon.QUESTION,
                          title: model.i18n._getLocaleText("CONFIRM_CART_SAVE_TITLE"),
                          actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
                          onClose: function(oAction) 
                            { / * test save * / 
                                console.log(oAction);
                                if(oAction==="YES"){
                                    
                                    //TODO invio dati del ordine a SAP
                                    
                                    
                                    sap.m.MessageToast.show(model.i18n._getLocaleText("ORDER_SAVED"));
                                    
                                    
                                    ///////////********************************//////////////////////
                                    
                                    sap.m.MessageBox.show(
                                        model.i18n._getLocaleText("ASK_TO_STORIFY_CART"), {
                                          icon: sap.m.MessageBox.Icon.QUESTION,
                                          title: model.i18n._getLocaleText("STORIFY_CART_TITLE"),
                                          actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
                                          onClose: function(oAction) 
                                            { / * test save * / 
                                                console.log(oAction);
                                                if(oAction==="YES"){
                                                    that.order.flagHistoryCart = true;
                                                    if(!!model.persistence.Storage.local.get("cartsHistory")){
                                                        var local = model.persistence.Storage.local.get("cartsHistory");
                                                        var maxid = 0;
                                                        local.carts.map(function(obj){     
                                                            if (obj.orderId > maxid) maxid = obj.orderId;    
                                                        });

                                                        var cnt = maxid+1;
                                                        that.order.orderId = cnt;
                                                        local.carts.push(that.order);

                                                        model.persistence.Storage.local.save("cartsHistory", {"carts": local.carts});

                                                    }else{
                                                    that.order.orderId = 1;
                                                    arr.push(that.order);
                                                    model.persistence.Storage.local.save("cartsHistory", {"carts": arr});
                                                    }
                                                    sap.m.MessageToast.show(model.i18n._getLocaleText("CART_LOCALLY_SAVED"));
                                                    
                                                    if(!!model.persistence.Storage.session.get("currentOrder")){
                                                        model.Current.removeOrder();
                                                    }
                                                    setTimeout(that.router.navTo("newOrder"), 4000);
                                                    



                                                }else{
                                                    if(!!model.persistence.Storage.session.get("currentOrder")){
                                                        model.Current.removeOrder();
                                                    }
                                                    setTimeout(that.router.navTo("newOrder"), 4000);
                                                    //that.router.navTo("newOrder");
                                                }
                                            }
                                      }
                                    );
                                    
                                    
                                    
                                    
                                    ///////////*******************************/////////////////////
                                    
                                    
                                    
                                }else{
                                    return;
                                }
                            }
                      }
                    );
                    

  },
  onShowProductPress: function (evt) {
//    var product = this.getView().getModel("appStatus").getProperty("/currentProduct");
//    if (product)
//      this.router.navTo("productDetail", {
//        id: product.getId()
//      });
//    else {
//      this.router.navTo("empty");
//    }
      this.router.navTo("empty");
      
  },
  onOrderHeaderPress: function (evt) {
    this.router.navTo("newOrder");
  },

  /********
  Primo metodo delete Rows che elimina dinamicamente dal model bindato alla view
  la posizione dall'array positions
  *************************************/ ///////
  onCancelRowPress: function (evt) {
    var position = evt.getParameters().listItem;
    var oContext = position.getBindingContext("c");
    var oModel = oContext.getModel();
    var row = oContext.getObject();
    var positionId = row.getId();
    this.order.removePosition(positionId);


    //        var oData = oModel.getData();
    //        var currentRowIndex = parseInt(oContext.getPath().split('/')[oContext.getPath().split('/').length - 1]);
    //        oData.positions.splice(currentRowIndex,1);
    oModel.updateBindings();
    this.getView().getModel("c").refresh(true);


  },

  onChangeQuantity: function (evt) {
    //var compareDate=this.order;
    //var quantityState = sap.ui.getCore().byId(this.getView().getId()).byId("inputQuantity");
    var inputID = evt.getParameters().id;
    var inputField = sap.ui.getCore().byId(inputID);
    var compareDate = new Date();
    var obj = evt.getSource().getBindingContext("c").getObject();
    obj.calculateTotalListPrice();
    var requestedDate = new Date(this.order.requestedDate); //.toLocaleDateString();
    //var cModel = this.getView().getModel("c");

    model.collections.Positions.getFutureDateByProductId(obj.productId, obj.quantity, requestedDate) //forceReload
      .then(_.bind(function (res) {
        console.log(res);
        var temp = {};
        temp.availableDate = res.availableDate.toLocaleDateString();
        temp.available = res.success;
        obj.update(temp);
        if (res.success === true) {
          inputField.setValueState("Success");
        } else {
          inputField.setValueState("None");
        }
        if (!this.order.fullEvasionAvailableDate || new Date(this.order.fullEvasionAvailableDate) < new Date(res.availableDate)) {
          this.order.fullEvasionAvailableDate = new Date(res.availableDate).toLocaleDateString();
        }
        this.cartModel.refresh(true);
        temp = {};
        //cModel.setProperty("/available", res.success);
        //cModel.setProperty("/availableDate", res.availableDate.toLocaleDateString());
        //cModel.updateBindings();
      }, this));

    //        var position = model.getData().positions;
    //
    //        for(var i=0; i<positions.length; i++){
    //            position[i].calculateTotalListPrice();
    //            //positions[i].totalListPrice = parseInt(positions[i].quantity) * parseFloat(positions[i].unitListPrice);
    //        }
    //model.updateBindings();
  },

  onSetDiscountPress: function (evt) {
    var src = evt.getSource();
    var position = src.getBindingContext("c").getObject();
    var discount = position.getDiscount();
    //var discount = position.discount;
    var discountModel = discount.getModel();
    //discountModel.setData(discount);
//    for (var prop in discount) {
//        if (_.isObject(this[prop])) {
//          _.merge(discountModel.getData()[prop], discount[prop]);
//        } else {
//          discountModel.getData()[prop] = discount[prop];
//        }
//      }
    
    
    var page = this.getView().byId("cartPage");
    this.discountDialog = sap.ui.xmlfragment("view.dialog.discountDialog", this);
    this.discountDialog.setModel(discountModel, "d");
    page.addDependent(this.discountDialog);

    this.discountDialog.open();
  },
  onDiscountDialogClose: function (evt) {
    this.discountDialog.close();

  },
  onDiscountDialogOK: function (evt) {

    console.log(this.order);
    this.discountDialog.close();

  },
  refreshDiscount: function (evt) {
    var discount = this.discountDialog.getModel("d").getData().ref;
    this.discountDialog.setModel(discount.refreshModel(), "d");
  },


  availableDateFunction: function (obj) {

    var compareDate = new Date();

    var requestedDate = new Date(obj.wantedDate); //.toLocaleDateString();


    model.collections.Positions.getFutureDateByProductId(obj.productId, obj.quantity, requestedDate) //forceReload
      .then(_.bind(function (res) {
        console.log(res);
        var temp = {};
        temp.availableDate = res.availableDate.toLocaleDateString();
        temp.available = res.success;
        obj.update(temp);
        //            if(res.success===true){
        //            inputField.setValueState("Success");
        //            }else{
        //                inputField.setValueState("None");
        //            }
        //**
        var d = undefined;
        if (this.order.fullEvasionAvailableDate) {
          var f = this.order.fullEvasionAvailableDate;
          if(f.indexOf('/')>0){
          d = this.order.fullEvasionAvailableDate.split('/');
            d = d[0] + "-" + d[1] + "-" + d[2];
          }
          else if(f.indexOf('-')>0)
          {
           d = this.order.fullEvasionAvailableDate.split('-'); 
            d = d[0] + "-" + d[1] + "-" + d[2];
          }
          
          
        }
        //**
        if (d) {
          if ( /*!this.order.fullEvasionAvailableDate ||*/ new Date(d) < new Date(res.availableDate)) {
            var da = new Date(res.availableDate).toLocaleDateString();
            if(da.indexOf("/")>0)
            {
              da=da.split("/");
            }
            else if(da.indexOf("-"))
            {
              da=da.split("-");
            }
            this.order.fullEvasionAvailableDate = da[2] + "-" + da[1] + "-" + da[0];
          } else {
            var da = new Date(d).toLocaleDateString();
            if(da.indexOf("/")>0)
            {
              da=da.split("/");
            }
            else if(da.indexOf("-"))
            {
              da=da.split("-");
            }
            this.order.fullEvasionAvailableDate = da[2] + "-" + da[1] + "-" + da[0];
          }
        } else {
          this.order.fullEvasionAvailableDate = this.order.requestedDate;
        }
        //            var modelObj = obj.getModel();
        //            modelObj.updateBindings();
        this.cartModel.refresh(true);
        temp = {};

      }, this));
  },





  calculateFunction: function () {
    if (this.order && this.order.positions.length > 0) {
      for (var i = 0; i < this.order.positions.length; i++) {
        this.availableDateFunction(this.order.positions[i]);
      }
    }

  }

});