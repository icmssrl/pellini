jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("model.collections.Orders");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.Order");
jQuery.sap.require("model.Current");
jQuery.sap.require("model.Order");
jQuery.sap.require("view.abstract.AbstractController");


view.abstract.AbstractController.extend("view.EmptyPlannings", {
	onInit: function () {
		view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
	},

	handleRouteMatched: function (evt) {
		view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
		var name = evt.getParameter("name");

		if (name !== "emptyPlannings"){
			return;
		}
	}
});
