jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.VisitsTour");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("model.filters.Filter");
jQuery.sap.require("jquery.sap.storage");
jQuery.sap.require("model.Current");


view.abstract.AbstractMasterController.extend("view.PlanningMaster", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
        this.uiModel.setProperty("/searchProperty", ["description", "id"]);

        this.userModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.userModel, "userModel");

        this.planningModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.planningModel, "planningModel");
    },


    handleRouteMatched: function (evt) {
        var name = evt.getParameter("name");

        if (name !== "planningMaster" && name !== "planningDetail" && name != "emptyPlanningDetail") {
            return;
        }

        this.user = model.persistence.Storage.session.get("user");
        this.userModel.setData(this.user);

        var plannings = [
            {
                'id': "1",
                'eventoPianificato': "Expo",
                'stato': "Pianificato",
                'scadenza': "27-07-2016"
          },
            {
                'id': "2",
                'eventoPianificato': "Mostra nazionale",
                'stato': "In esecuzione",
                'scadenza': "22-07-2016"
          },
            {
                'id': "3",
                'eventoPianificato': "Vinitaly",
                'stato': "Pianificato",
                'scadenza': "28-07-2016"
          }
      ];
        this.planningModel.setProperty("/plannings", plannings);
    },

    onEventPress: function (evt) {
        var path = evt.getSource().getBindingContext("planningModel").getPath();
        var index = parseInt(path.split("/")[2]);
        var planning = this.planningModel.getProperty("/plannings")[index];
        sessionStorage.setItem("planning", JSON.stringify(planning));
        this.router.navTo("planningDetail", {
            id: planning.id
        });
    },
    
    onBackPress: function () {
        this.router.navTo("calendar");
    }
});
