jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.Shippments");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.collections.praticheAnomale.PraticheAperte");
jQuery.sap.require("model.ui.CustomRow");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("model.Current");
jQuery.sap.require("model.i18n");

view.abstract.AbstractController.extend("view.shippment.ShippmentDetail", {

	onExit: function () {

	},


	onInit: function () {

		// this.router = sap.ui.core.UIComponent.getRouterFor(this);
		// this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
		view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
        
        this.shippmentDetailModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.shippmentDetailModel, "shipDetail");

	},


	handleRouteMatched: function (evt) {

		view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
		var routeName = evt.getParameter("name");

		if ( routeName !== "shippment.shippmentDetail"){
			return;
		}

		this.id = evt.getParameters().arguments.id;
        
        this.user = model.persistence.Storage.session.get("user");
        this.userModel = new sap.ui.model.json.JSONModel(this.user);
        this.getView().setModel(this.userModel, "userModel");


		model.collections.Shippments.getByShippmentId(this.id)
		.then(

			_.bind(function(result)
			{
				this.shippmentDetailModel.setData(result);
				this.shippmentDetailModel.refresh(true);
//				this.refreshView(result);
			}, this)
		);
        
        this.orderListModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.orderListModel, "orderList");
        
    
        model.collections.praticheAnomale.PraticheAperte.loadOrdersByShippmentId(this.id) //forceReload
        .then(_.bind(function(res){
            console.log(res);

            this.orderListModel.setData(res);
            this.orderListModel.refresh(true);

        }, this));
             
          

	},
    
	
	refreshView : function(data)//maybe to abstract
	{
//			var enable = {"editable": false};
//			var page = this.getView().byId("detailPage");
//			this.getView().setModel(data.getModel(), "c");
//			enable.editable=false;
//			var toolbar = sap.ui.xmlfragment("view.fragment.detailToolbar", this);
//			page.setFooter(toolbar);
//			var enableModel = new sap.ui.model.json.JSONModel(enable);
//			this.getView().setModel(enableModel, "en");

	},
    
    openCreatePacticeActionSheet : function (oEvent) {
			var oButton = oEvent.getSource();

			// create action sheet only once
			if (!this._actionSheet) {
				this._actionSheet = sap.ui.xmlfragment(
					"view.fragment.createPracticeActionSheet",
					this
				);
				this.getView().addDependent(this._actionSheet);
			}

			this._actionSheet.openBy(oButton);
    },
	
    
    onCreatePracticePress: function(evt){
        var detail = this.shippmentDetailModel.getData();
        model.persistence.Storage.session.save("selectedShippment", detail);
        var idButton = evt.getParameter("id");
        switch(idButton){
            case "resoConNotaCredito":
                this.router.navTo("praticheAnomale.ResoConNC",  {shippmentId : this.id});
                break;
            case "resoSenzaNotaCredito":
                this.router.navTo("praticheAnomale.ResoSenzaNC",  {shippmentId : this.id});
                break;
            case "richiestaRicambi":
                this.router.navTo("praticheAnomale.RichiestaRicambi",  {shippmentId : this.id});
                break;
            case "mancanzaMerce":
                this.router.navTo("praticheAnomale.MancanzaMerce",  {shippmentId : this.id});
                break;
            
        }
    }, 
    
    showPracticeDetails: function(){
        this.createRenderDialog = sap.ui.xmlfragment("view.fragment.renderConfirmPage", this);
        var page = this.getView().byId("shippmentDetailPage");
        page.addDependent(this.createRenderDialog);
        this.createRenderDialog.open();
    },
    
    onCreatePracticeDialogConfirm: function(evt){
        
        /* TODO: Send data to SAP*/
        
//        sap.m.MessageToast.show(model.i18n._getLocaleText("AP_PRACTICE_CREATED"));
        
        if(this.createRenderDialog){
            this.createRenderDialog.close();
            this.createRenderDialog.destroy();
        }
       
    },
	
    
    onLinkToUserInfoPress: function(evt){
      this.router.navTo("changePassword");  
    }


});
