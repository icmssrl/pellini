jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.Customers");
jQuery.sap.require("model.collections.Positions");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("model.Order");
jQuery.sap.require("model.Current");
jQuery.sap.require("model.Position");
jQuery.sap.require("model.Cart");
jQuery.sap.require("model.i18n");
jQuery.sap.require("model.Discount");

view.abstract.AbstractController.extend("view.ProductRODetail", {

	onExit: function () {

	},


	onInit: function () {

		// this.router = sap.ui.core.UIComponent.getRouterFor(this);
		// this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
		view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

	},


	handleRouteMatched: function (evt) {

		view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
		var routeName = evt.getParameter("name");

		if ( routeName !== "productRODetail"){
			return;
		}


		var id = evt.getParameters().arguments.id;

		this.order = model.Current.getOrder();

		//Now it's here, maybe the next code needs to be moved
		//As we are in orderCreqtion, maybe we need another property to verify the application status
		if(!this.order)
		{
			var orderData = model.persistence.Storage.session.get("currentOrder");
			this.order  = new model.Order(orderData);

		};

		this.customer = model.Current.getCustomer();

		//Now it's here, maybe the next code needs to be moved
		//As we are in orderCreqtion, maybe we need another property to verify the application status
		if(!this.customer)
		{
			var customerData = model.persistence.Storage.session.get("currentCustomer");
			this.customer  = new model.Customer(customerData);

		};

		this.reqModel = new sap.ui.model.json.JSONModel({"reqDate":"", "reqQty":""});
		this.getView().setModel(this.reqModel, "req");
    //**
    this.reqModel.setProperty("/reqDate",this.order.requestedDate);
    //**
		this.resModel = new sap.ui.model.json.JSONModel({"availableDate":""});
		this.getView().setModel(this.resModel, "res");

    //**
		model.collections.Products.getById(id) //this must be producId? why not use this variable?
    .then(

			_.bind(function(result)
			{
				this.product = result;
				// this.refreshView(result, routeName)
				this.refreshView(result);
			}, this)
		);

	},
	
	refreshView : function(data)//maybe to abstract
	{
			var enable = {"editable": false};
			var page = this.getView().byId("detailROPage");
			this.getView().setModel(data.getModel(), "p");
			enable.editable=false;
			var toolbar = sap.ui.xmlfragment("view.fragment.catalogueToolbar", this);
			page.setFooter(toolbar);
			this.getView().setModel(this.customer.getModel(), "customer");
			// var enableModel = new sap.ui.model.json.JSONModel(enable);
			// this.getView().setModel(enableModel, "en");

	},

	onAddPress : function(evt)
	{

				if(!this.cart)
				{
					this.cart = new model.Cart();

					this.cart.create(this.order.getId(), this.customer.getId());
				}

			//------------------------------------------------
			var position = new model.Position();
            var checkProductId = this.product.productId;
            if(this.order.positions){
            var positionsArr = this.order.positions;
            for(var i=0; i<positionsArr.length; i++){
                if(checkProductId===positionsArr[i].productId){
                    sap.m.MessageToast.show(model.i18n.getText("Warning!! Product already added before"));
                    return;
                }
            }
            }

	    position.create(this.order, this.product);
			position.setQuantity(this.reqModel.getData().reqQty);
			position.setWantedDate(this.reqModel.getData().reqDate);

	    this.order.addPosition(position);
	    model.Current.setOrder(this.order);
	    this.cart.setPositions(this.order.getPositions());
	    console.log(this.order);

			var discount = new model.Discount();
			discount.initialize(this.order.getId(), position)
			.then(_.bind(function()
			{
				position.setDiscount(discount);
				console.log(position);
	      sap.m.MessageToast.show(model.i18n.getText("Product added: "+this.product.getId()));
			}, this))






	},
	onProductRequestPress:function(evt)
	{
		var src = evt.getSource();
		var panel = src.getParent().getParent();
		if(panel.getExpanded())
			panel.setExpanded(false);
		else {
			panel.setExpanded(true);
		}

	},

	onConfirmCheckPress:function(evt)
	{
		var reqDate = this.getView().getModel("req").getData().reqDate;
		var reqQty = this.getView().getModel("req").getData().reqQty;

		//Maybe to Abstract
		model.collections.Positions.getFutureDateByProductId(this.product.getId(), reqQty, reqDate)//forceReload
		.then(_.bind(function(res){
				console.log(res);
				var temp={};
				temp.availableDate=res.availableDate.toLocaleDateString();
				temp.available=res.success;
				this.getView().getModel("res").setData({"availableDate": temp.availableDate});
				//cModel.setProperty("/available", res.success);
				//cModel.setProperty("/availableDate", res.availableDate.toLocaleDateString());
				//cModel.updateBindings();
		}, this));


	}




});
