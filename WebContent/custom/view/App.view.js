sap.ui.jsview("view.App", {

	getControllerName: function() {
		return "view.App";
	},

	createContent: function(oController) {
		this.setDisplayBlock(true);
		this.app = new sap.m.App("app");
		return this.app;
	}
});
