jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.Products");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("model.filters.Filter");
jQuery.sap.require("jquery.sap.storage");
jQuery.sap.require("model.Cart");
jQuery.sap.require("model.Current");
jQuery.sap.require("model.Catalogue");

view.abstract.AbstractMasterController.extend("view.ProductsList", {

  onExit: function () {

  },


  onInit: function () {

    // this.router = sap.ui.core.UIComponent.getRouterFor(this);
    // this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
    view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
    this.uiModel.setProperty("/searchProperty", ["description", "productId"]);
      
     
    
      
  },


  handleRouteMatched: function (evt) {

    var name = evt.getParameter("name");

    if (name !== "productsList" && name !== "productDetail" && name != "productEdit" && name != "empty") {
      return;
    }
      
    //this.getView().getModel("appStatus").setProperty("/enableFilterButton", false);
      
    this.catalogue = new model.Catalogue(); 
    this.catalogue.getHierarchy()
    .then(_.bind(this.refreshList, this));
      
    


//    model.collections.Products.loadProducts(true) //forceReload
//      .then(_.bind(this.refreshList, this));




  },

  onItemPress: function (evt) {
    var src = evt.getSource();
      
    var selectedItem = src.getBindingContext("h").getObject();
    var selectedLevel  = selectedItem.level;
    
    if(selectedLevel)
    {
    this.setPath(selectedItem);
       
    
    switch (selectedLevel){
        case 1:
            var res = selectedItem.items;
            this.hierarchyModel.setData({"items":res});
            this.getView().setModel(this.hierarchyModel, "h");
            break;
        case 2:
            //this.catalogue.getProducts(selectedItem.productId)
            model.collections.Products.loadProducts(selectedItem.productId)
            .then(_.bind(function(products){
              
              this.hierarchyModel.setData({"items": products});
              this.getView().setModel(this.hierarchyModel, "h");

            }, this));
            break;
        
    }
      

    }else{
    this.navToProduct(selectedItem.productId);
    this.getView().getModel("appStatus").setProperty("/currentProduct", selectedItem);
    
    }
    

  },
    
  setPath : function(item)
    {
       var itemId = item.productId;
       var level = item.level;
       var crumbToolbar = this.getView().byId("crumbToolbar");
       var content = crumbToolbar.getContent();
       switch(level){
           case 1 :
               crumbToolbar.addContent(new sap.m.Link({text:itemId, id:"lv1Node", class:"hierachyLink", press : [this.handleLinkPress, this]}));
               //sessionStorage.setItem("lv1Node",{"id": itemId});
               break;
           case 2 : 
               crumbToolbar.addContent(new sap.m.Link({text:itemId, id:"lv2Node", class:"hierachyLink", press : [this.handleLinkPress, this]}));
               //sessionStorage.setItem("lv2Node",{"id": itemId});
               break;
           default:
               return;
       }

    },
    
//    getPath: function(itemId, level){
//        
//    },
    
    
  handleLinkPress : function(evt)
  {
    var id = evt.getParameters().id;
    var itemId = evt.getSource().getText();
    var crumbToolbar = this.getView().byId("crumbToolbar");
    var content = crumbToolbar.getContent();
    var lv2 = sap.ui.getCore().byId("lv2Node");
    var lv1 = sap.ui.getCore().byId("lv1Node");
    switch(id){
        case "lv1Node":
        this.catalogue.getHierarchy()
        .then(_.bind(function(result){
          this.hierarchyModel.setData(result);
          this.getView().setModel(this.hierarchyModel, "h");
            
        }, this));
        if(content.length>=1){
            
            if(content[2]){
                if(!!lv2)
                lv2.destroy();
                crumbToolbar.removeContent(content[2]);  
            }
            if(!!lv1)
            lv1.destroy();
            crumbToolbar.removeContent(content[1]);
        }
        break;
        case "lv2Node":
        var result = this.catalogue.getChildNodeItems(itemId);
        this.hierarchyModel.setData(result);
        this.getView().setModel(this.hierarchyModel, "h");
        lv2.destroy();
        crumbToolbar.removeContent(content[2]);
        break;
    }

  },

  refreshList: function (evt) {
    var filters = this.getFiltersOnList();

    if (filters)
    this.getView().byId("list").getBinding("items").filter(filters);
      
    if(!this.hierarchyModel){
    this.hierarchyModel = this.catalogue.getModel();
    this.getView().setModel(this.hierarchyModel, "h");
    }

  },
  onFilterPress: function () {
    this.filterModel = model.filters.Filter.getModel(this.hierarchyModel.getData().items, "hierarchyItems");
    this.getView().setModel(this.filterModel, "filter");
    var page = this.getView().byId("productsListPageId");
    this.filterDialog = sap.ui.xmlfragment("view.dialog.filterDialog", this);
    page.addDependent(this.filterDialog);
    this.filterDialog.open();

  },
  onFilterDialogClose: function () {
    this.filterDialog.close();
  },

  onFilterPropertyPress: function (evt) {

    var parentPage = sap.ui.getCore().byId("parent");
    var elementPage = sap.ui.getCore().byId("children");
    console.log(this.getView().getModel("filter").getData().toString());
    var navCon = sap.ui.getCore().byId("navCon");
    var selectedProp = evt.getSource().getBindingContext("filter").getObject();
    this.getView().getModel("filter").setProperty("/selected", selectedProp);
    this.elementListFragment = sap.ui.xmlfragment("view.fragment.FilterList", this);
    elementPage.addContent(this.elementListFragment);

    navCon.to(elementPage, "slide");
    this.getView().getModel("filter").refresh();
  },

  onBackFilterPress: function (evt) {
    // this.addSelectedFilterItem();
    this.navConBack();
    this.getView().getModel("filter").setProperty("/selected", "");
    this.elementListFragment.destroy();
  },
  navConBack: function () {
    var navCon = sap.ui.getCore().byId("navCon");
    navCon.to(sap.ui.getCore().byId("parent"), "slide");
    this.elementListFragment.destroy();
  },
  afterOpenFilter: function (evt) {
    var navCon = sap.ui.getCore().byId("navCon");
    if (navCon.getCurrentPage().getId() == "children")
      navCon.to(sap.ui.getCore().byId("parent"), "slide");
    this.getView().getModel("filter").setProperty("/selected", "");
  },

  onSearchFilter: function (oEvt) {
    var aFilters = [];
    var sQuery = oEvt.getSource().getValue();

    if (sQuery && sQuery.length > 0) {

      // var filter = new sap.ui.model.Filter("value", sap.ui.model.FilterOperator.Contains, sQuery);

      // 	var filter = new sap.ui.model.Filter({path:"value", test:function(val)
      // {
      // 	var property= val.toString().toUpperCase();
      // 	return (property.indexOf(sQuery.toString().toUpperCase())>=0)
      // }});

      aFilters.push(this.createFilter(sQuery, "value"));
    }

    // update list binding
    var list = sap.ui.getCore().byId("filterList");
    var binding = list.getBinding("items");
    binding.filter(aFilters);
  },
  createFilter: function (query, property) {
    var filter = new sap.ui.model.Filter({
      path: property,
      test: function (val) {
        var prop = val.toString().toUpperCase();
        return (prop.indexOf(query.toString().toUpperCase()) >= 0)
      }
    });
    return filter;
  },
  onFilterDialogClose: function (evt) {
    if (this.elementListFragment) {
      this.elementListFragment.destroy();
    }
    if (this.filterDialog) {
      this.filterDialog.close();
      this.filterDialog.destroy();
    }
  },
  onFilterDialogOK: function (evt) {
    var filterItems = model.filters.Filter.getSelectedItems("hierarchyItems");
    if (this.elementListFragment)
      this.elementListFragment.destroy();
    this.filterDialog.close();
    this.getView().getModel("filter").setProperty("/selected", "");
    this.handleFilterConfirm(filterItems);
    this.filterDialog.destroy();
    delete(this.filterDialog);
  },
  handleFilterConfirm: function (selectedItems) {
    var filters = [];
    _.forEach(selectedItems, _.bind(function (item) {
        filters.push(this.createFilter(item.value, item.property));
      },
      this));
    var list = this.getView().byId("list");
    var binding = list.getBinding("items");
    binding.filter(filters);
  },
  onResetFilterPress: function () {
    model.filters.Filter.resetFilter("hierarchyItems");
    // console.log(model.filters.Filter.getSelectedItems("customers"));
    var list = this.getView().byId("list");
    var binding = list.getBinding("items");
    binding.filter();
    if (this.elementListFragment) {
      this.elementListFragment.destroy();
    }
    if (this.filterDialog) {
      this.filterDialog.close();
      this.filterDialog.destroy();
    }
  },

  onCartPress: function (evt) {
    var crumbToolbar = this.getView().byId("crumbToolbar");
    var content = crumbToolbar.getContent();
    var lv2 = sap.ui.getCore().byId("lv2Node");
    var lv1 = sap.ui.getCore().byId("lv1Node");
    
    if(content.length>=1){

        if(content[2]){
            if(!!lv2)
            lv2.destroy();
            crumbToolbar.removeContent(content[2]);  
        }
        if(!!lv1)
        lv1.destroy();
        crumbToolbar.removeContent(content[1]);
    }  
    this.hierarchyModel = undefined;  
    this.router.navTo("cartFullView");
   
    
    // var src = evt.getSource();
    // src.setIcon("sap-icon://add-product");
    // src.detachPress(this.onCartPress, this);
    // src.attachPress(null, this.onShowProductPress, this);
  },
  // onShowProductPress:function(evt)
  // {
  // 	if(this.getView().getModel("appStatus").getProperty("/currentProduct"))
  // 	{
  // 		var lastProductId = this.getView().getModel("appStatus").getProperty("/currentProduct").productId;
  // 		this.navToProduct(lastProductId, "flip");
  // 		return;
  // 	}
  //
  // 	this.router.navTo("empty");



  // },
  navToProduct: function (id) {
      
    this.router.navTo("productDetail", {
      id: id
    });
    // var src = this.getView().byId("navId");
    // src.setIcon("sap-icon://cart");
    // src.detachPress(this.onShowProductPress, this);
    // src.attachPress(null, this.onCartPress, this);
  },



  onOrderHeaderPress: function (evt) {
    var crumbToolbar = this.getView().byId("crumbToolbar");
    var content = crumbToolbar.getContent();
    var lv2 = sap.ui.getCore().byId("lv2Node");
    var lv1 = sap.ui.getCore().byId("lv1Node");
    
    if(content.length>=1){

        if(content[2]){
            if(!!lv2)
            lv2.destroy();
            crumbToolbar.removeContent(content[2]);  
        }
        if(!!lv1)
        lv1.destroy();
        crumbToolbar.removeContent(content[1]);
    }  
    this.hierarchyModel = undefined;  
    this.router.navTo("newOrder");
    
  }








});