jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.DateFormat");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.Customers");
jQuery.sap.require("model.persistence.Storage");

sap.ui.controller("view.Appointments", {

    onInit: function () {

        this.router = sap.ui.core.UIComponent.getRouterFor(this);
        this.router.attachRoutePatternMatched(this.handleRouteMatched, this);

        this.editModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.editModel, "editModel");

        this.enableModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.enableModel, "enableModel");
    },

    handleRouteMatched: function (evt) {

        var name = evt.getParameter("name");
        if (name !== "appointments") {
            return;
        };

        var d = evt.getParameters().arguments.data;
        if (d) {
            this.dataEvento = d.substr(0, 2) + "/" + d.substr(2, 2) + "/" + d.substr(4);
            this.dataScadenza = d.substr(0, 2) + "/" + d.substr(2, 2) + "/" + d.substr(4);
        } else {
            this.dataEvento = "";
            this.dataScadenza = "";
        };

        this.state = evt.getParameters().arguments.state;

        this.user = model.persistence.Storage.session.get("user");
        this.username = this.user.username;
        this.division = model.persistence.Storage.session.get("division");
        if (!this.division) {
            this.division = _.chain(this.user.organizationData).pluck('division').uniq().value()[0];
        };
        model.collections.Customers.loadCustomers(this.user.username, this.division)
            .then(_.bind(this.refreshList, this));

        // carico l'array di appuntamenti che ci sono nel local storage
        this.appointments = localStorage.getItem(this.username + "-appointments");
        if (this.appointments) {
            this.appointments = JSON.parse(this.appointments);
            // calcolo qual è l'id più grande cosicché se salvo un nuovo appuntamento aggiungo 1 all'id maggiore appena calcolato (NON SO SE SERVIRA' LA GESTIONE DI UN ID)
            this.appointments = _.sortBy(this.appointments, function (m) {
                return parseInt(m.counterFiori);
            });
            this.maxCounterFiori = this.appointments[this.appointments.length - 1].counterFiori;
        } else {
            this.appointments = [];
            this.maxCounterFiori = 0;
        };

        if (this.state === "edit") {
            this.appointment = JSON.parse(sessionStorage.getItem("selectedAppointment"));
            this.nomeEvento = this.appointment.nomeEvento;
            this.oraEventoInizio = this.appointment.oraEventoInizio;
            this.oraEventoFine = this.appointment.oraEventoFine;
            this.allDayEvento = this.appointment.allDayEvento;
            this.clienteEvento = this.appointment.clienteEvento;
            this.luogoEvento = this.appointment.luogoEvento;
            this.noteEvento = this.appointment.noteEvento;
            this.colore = this.appointment.colore;
            // setto l'immagine con il checkBox selezionato sul colore corretto
            var checkBoxSelected = this.getView().byId("appointmentsId--" + this.colore.substr(1,6));
            checkBoxSelected.setSrc("./custom/img/pellini/CheckboxTick/" + this.colore.substr(1,6) + ".svg");
        } else {
            this.nomeEvento = "";
            this.oraEventoInizio = "";
            this.oraEventoFine = "";
            this.allDayEvento = false;
            this.clienteEvento = "";
            this.luogoEvento = "";
            this.noteEvento = "";
            this.colore = "#33FF33";
            // la prima immagina sarà quella con il checkBox selezionato
            var checkBoxSelected = this.getView().byId("appointmentsId--" + this.colore.substr(1,6));
            checkBoxSelected.setSrc("./custom/img/pellini/CheckboxTick/" + this.colore.substr(1,6) + ".svg");
        };
        this.editModel.setProperty("/nomeEvento", this.nomeEvento);
        this.editModel.setProperty("/dataEvento", this.dataEvento);
        this.editModel.setProperty("/oraEventoInizio", this.oraEventoInizio);
        this.editModel.setProperty("/oraEventoFine", this.oraEventoFine);
        this.editModel.setProperty("/dataScadenza", this.dataScadenza);
        this.enableModel.setProperty("/allDay", this.allDayEvento);
        this.editModel.setProperty("/clienteEvento", this.clienteEvento);
        this.editModel.setProperty("/luogoEvento", this.luogoEvento);
        this.editModel.setProperty("/noteEvento", this.noteEvento);
    },

    _getLocaleText: function (key) {
        return this.getView().getModel("i18n").getProperty(key);
    },

    refreshList: function () {
        this.customersModel = model.collections.Customers.getModel();
        this.getView().setModel(this.customersModel, "c");
    },


    changeNomeEvento: function (evt) {
        var nomeEvento = evt.getSource().getValue();
        this.nomeEvento = nomeEvento;
        if (nomeEvento !== "") {
            this.getView().byId("idNomeEvento").setValueState("None");
        };
    },

    changeDataEvento: function (evt) {
        var dataEvento = evt.getSource().getDateValue();
        dataEvento = utils.DateFormat.formatDate(dataEvento);
        this.dataEvento = dataEvento;
        if (dataEvento !== "") {
            this.getView().byId("idDataEvento").setValueState("None");
        };
    },

    changeScadenzaEvento: function (evt) {
        var dataEvento = evt.getSource().getDateValue();
        dataEvento = utils.DateFormat.formatDate(dataEvento);
        this.dataEvento = dataEvento;
        if (dataEvento !== "") {
            this.getView().byId("idScadenza").setValueState("None");
        };
    },

    changeOraInizio: function (evt) {
        var oraInizio = evt.getSource().getValue();
        this.oraEventoInizio = oraInizio;
        if (this.oraEventoInizio === "") {
            this.oraEventoFine = "";
            this.editModel.setProperty("/oraEventoFine", this.oraEventoFine);
        } else {
            this.getView().byId("idOraEventoInizio").setValueState("None");
            if (this.oraEventoFine === "") {
                this.oraEventoFine = (parseInt(oraInizio.split(":")[0]) + 1).toString() + ":" + oraInizio.split(":")[1];
                this.editModel.setProperty("/oraEventoFine", this.oraEventoFine);
            } else {
                if (!this.checkOrari(this.oraEventoInizio, this.oraEventoFine)) {
                    //error
                } else {
                    //OK
                }
            }
        }
    },

    changeOraFine: function (evt) {
        var oraFine = evt.getSource().getValue();
        this.oraEventoFine = oraFine;
        if (this.oraEventoFine === "") {
            this.oraEventoInizio = "";
            this.editModel.setProperty("/oraEventoInizio", this.oraEventoInizio);
        } else {
            this.getView().byId("idOraEventoFine").setValueState("None");
            if (this.oraEventoInizio === "") {
                this.oraEventoInizio = (parseInt(oraFine.split(":")[0]) - 1).toString() + ":" + oraFine.split(":")[1];
                this.editModel.setProperty("/oraEventoInizio", this.oraEventoInizio);
            } else {
                if (!this.checkOrari(this.oraEventoInizio, this.oraEventoFine)) {
                    //error
                } else {
                    //OK
                }
            }
        }
    },

    changeAllDayEvento: function (evt) {
        var isSelect = evt.getSource().getSelected();
        if (isSelect) {
            this.allDayEvento = true;
            this.oraEventoInizio = "";
            this.oraEventoFine = "";
        } else {
            this.allDayEvento = false;
            this.oraEventoInizio = this.editModel.getProperty("/oraEventoInizio");
            this.oraEventoFine = this.editModel.getProperty("/oraEventoFine");
        };
        this.enableModel.setProperty("/allDay", this.allDayEvento);
    },

    changeClienteEvento: function (evt) {
        this.clienteEvento = evt.getSource().getValue();
    },

    changeLuogoEvento: function (evt) {
        this.luogoEvento = evt.getSource().getValue();
    },

    changeNoteEvento: function (evt) {
        this.noteEvento = evt.getSource().getValue();
    },

    colorPress: function (evt) {
        var checkBoxCiclato = this.getView().byId("appointmentsId--" + this.colore.substr(1,6));
        var srcCiclato = checkBoxCiclato.getSrc();
        var srcDaSostituire = srcCiclato.replace("boxTick", "boxNoTick");
        checkBoxCiclato.setSrc(srcDaSostituire);
        // carico l'immagine corretta e dò il valore giusto al parametro this.colore
        var coloreCliccato = evt.getSource().getProperty("alt");
        var idCliccato = evt.getParameters().id;
        var checkBoxSelected = sap.ui.getCore().getElementById(idCliccato);
        checkBoxSelected.setSrc("./custom/img/pellini/CheckboxTick/" + coloreCliccato + ".svg");
        this.colore = "#" + coloreCliccato;
    },

    checkOrari: function (oraEventoInizio, oraEventoFine) {
        var oraInizio = utils.DateFormat.orarioDaStringa(oraEventoInizio);
        var oraFine = utils.DateFormat.orarioDaStringa(oraEventoFine);
        if (oraInizio < oraFine) {
            return true;
        } else {
            return false;
        };
    },

    onSaveOnLocal: function () {
        var campiMancanti = false;
        if (this.nomeEvento === "") {
            this.getView().byId("idNomeEvento").setValueState("Error");
            campiMancanti = true;
        };
        if (this.dataEvento === "") {
            this.getView().byId("idDataEvento").setValueState("Error");
            campiMancanti = true;
        };
        if (this.allDayEvento === false) {
            if (this.oraEventoInizio === "") {
                campiMancanti = true;
            };
            if (this.oraEventoFine === "") {
                campiMancanti = true;
            };
        };
        if (this.idClienteEvento === "") {
            this.getView().byId("idDataEvento").setValueState("Error");
            campiMancanti = true;
        };
        if (campiMancanti) {
            sap.m.MessageBox.error(this._getLocaleText("campiMancanti"), {
                title: this._getLocaleText("WARNING")
            });
        } else {
            if (this.allDayEvento) {
                this.oraEventoInizio = "00:01";
                this.oraEventoFine = "23:59";
            };
            if (this.state === "new") {
                var counterFiori = this.maxCounterFiori + 1;
            } else {
                var counterFiori = this.appointment.counterFiori;
                _.remove(this.appointments, {
                    'counterFiori': this.appointment.counterFiori
                });
            };
            var appointment = {
                'nomeEvento': this.nomeEvento,
                'dataEvento': this.dataEvento,
                'oraEventoInizio': this.oraEventoInizio,
                'oraEventoFine': this.oraEventoFine,
                'allDayEvento': this.allDayEvento,
                'clienteEvento': this.clienteEvento,
                'luogoEvento': this.luogoEvento,
                'noteEvento': this.noteEvento,
                'colore': this.colore,
                'counterFiori': counterFiori
            };
            this.appointments.push(appointment);
            localStorage.setItem(this.username + "-appointments", JSON.stringify(this.appointments));
            this.resetFields();
            sessionStorage.removeItem("selectedAppointment");
            this.router.navTo("calendar");
        };
        var checkBoxCiclato = this.getView().byId("appointmentsId--" + this.colore.substr(1,6));
        var srcCiclato = checkBoxCiclato.getSrc();
        var srcDaSostituire = srcCiclato.replace("boxTick", "boxNoTick");
        checkBoxCiclato.setSrc(srcDaSostituire);
    },

    onCancelPress: function (evt) {
        var checkBoxCiclato = this.getView().byId("appointmentsId--" + this.colore.substr(1,6));
        var srcCiclato = checkBoxCiclato.getSrc();
        var srcDaSostituire = srcCiclato.replace("boxTick", "boxNoTick");
        checkBoxCiclato.setSrc(srcDaSostituire);
        this.resetFields();
        sessionStorage.removeItem("selectedAppointment");
        this.router.navTo("calendar");
    },

    resetFields: function () {
        this.getView().byId("idNomeEvento").setValue("");
        this.getView().byId("idDataEvento").setValue("");
        this.getView().byId("idOraEventoInizio").setValue("");
        this.getView().byId("idOraEventoFine").setValue("");
        this.getView().byId("idAllDay").setSelected(false);
        this.getView().byId("idClienteEvento").setValue("");
        this.getView().byId("idLuogoEvento").setValue("");
        this.getView().byId("idNoteEvento").setValue("");
    },


    //        handleRouteMatched: function (evt) {

    //        if (!sessionStorage.isLogged || sessionStorage.isLogged === "false") {
    //            this.router.navTo("");
    //        } else {
    //            var userInfo = utils.LocalStorage.getItem("cidSet");
    //            if (userInfo) {
    //                this.nomeCognomeModel.setProperty("/nomeCognome", userInfo[0].nome + " " + userInfo[0].cognome);
    //            };
    //
    //            var rapportiniModel = new sap.ui.model.json.JSONModel();
    //            this.getView().setModel(rapportiniModel, "rapportini");
    //
    //            this.resetStates();
    //            this.enableModel.setProperty("/dataModificabile", true);
    //            var data = new Date();
    //            this.cid = sessionStorage.cid;
    //            this.entity = evt.getParameters().arguments.entity;
    //            var d = location.hash.split("/")[3];
    //            var counter = location.hash.split("/")[4];
    //            var data = "";
    //
    //            if (d) {
    //                data = d.substr(0, 2) + "/" + d.substr(2, 2) + "/" + d.substr(4);
    //            };
    //
    //            this.data = _.clone(data);
    //
    //            this.getView().byId("datePickerEdit").setEnabled(true);
    //
    //            var that = this;
    //            setTimeout(_.bind(that.enableOEPTabNavigation, this, "edit"), 100);
    //
    //            document.onkeypress = _.bind(function (e) {
    //                if (e.keyCode === 13) {
    //                    return null;
    //                };
    //            }, this);
    //
    //            ////////////setto il counter SAP a undefined e poi lo trovo eventualmente in caso di entity = edit /////////////////////////
    //            this.counter = undefined;
    //
    //            this.attivitaSet = utils.LocalStorage.getItem("attivitaSet");
    //            this.projectSet = utils.LocalStorage.getItem("projectSet");
    //            this.commessaSet = utils.LocalStorage.getItem("commessaSet");
    //            for (var i = 0; i < this.projectSet.length; i++) {
    //                //            this.projectSet[i].descrizioneCommessa = this.projectSet[i].descrizione;
    //                this.projectSet[i].descrizioneCommessa = this.projectSet[i].descrizioneProgetto + " - " + this.projectSet[i].descrizione;
    //            };
    //            this.getView().byId("location").setSelectedKey();
    //            this.luogoLavoro = utils.LocalStorage.getItem("luogoLavoroSet");
    //            this.getView().getModel("appModel").setProperty("/location", this.luogoLavoro);
    //            this.tipoOrario = utils.LocalStorage.getItem("tipoOrarioSet");
    //            this.getView().getModel("appModel").setProperty("/tipoOrario", this.tipoOrario);
    //            var commesse = [];
    //            for (var i = 0; i < this.projectSet.length; i++) {
    //                var commessa = {
    //                    'key': this.projectSet[i].commessa,
    //                    'name': this.projectSet[i].descrizioneCommessa,
    //                    'disabilitato': this.projectSet[i].disabilitato
    //                };
    //                commesse.push(commessa);
    //            };
    //            var commesseUnificate = [];
    //            var array = [];
    //            for (var i = 0; i < commesse.length; i++) {
    //                array.push(commesse[i].key)
    //            };
    //            array = _.uniq(array);
    //            for (var i = 0; i < array.length; i++) {
    //                var obj = _.find(commesse, {
    //                    'key': array[i]
    //                });
    //                commesseUnificate.push(obj);
    //            };
    //            _.remove(commesseUnificate, {
    //                'disabilitato': "X"
    //            });
    //            this.commesseModel.setProperty("/commessa", commesseUnificate);
    //            this.commesseModel.refresh();
    //
    //            rapportiniModel.setData(this.attivitaSet);
    //
    //            var maxTime = "09:00:00";
    //            this.time = new Date();
    //
    //            this.editModel.setProperty("/nonFatturabile", false);
    //
    //            if (this.entity === "edit") {
    //
    //                this.getView().byId("attivita").setEditable(true);
    //                this.getView().byId("attivita").setEnabled(true);
    //
    //                var myObj = _.find(this.attivitaSet, {
    //                    'dataCommessa': data,
    //                    'counterFiori': counter,
    //                });
    //
    //                if (!myObj) {
    //                    var myObj = _.find(this.attivitaSet, {
    //                        'dataCommessa': data,
    //                        'counter': counter
    //                    });
    //                };
    //                this.counter = myObj.counter;
    //                this.wbsIniziale = myObj.attivita;
    //                this.dataIniziale = myObj.dataCommessa;
    //                var commessaDisabilitata = _.find(this.projectSet, {
    //                    'commessa': myObj.commessa
    //                });
    //
    //                var day = myObj.dataCommessa.split("/")[0];
    //                var month = myObj.dataCommessa.split("/")[1];
    //                var year = myObj.dataCommessa.split("/")[2];
    //                myObj.date = new Date(month + "/" + day + "/" + year);
    //                myObj.oraInizio = myObj.orarioInizio.split(":")[0];
    //                myObj.minutiInizio = myObj.orarioInizio.split(":")[1];
    //                this.checkOreEdit = myObj.durata;
    //
    //                var commessaScelta = myObj.commessa;
    //                var attivitaList = [];
    //                for (var i = 0; i < this.commessaSet.dati.length; i++) {
    //                    var attivita = {
    //                        'commessaKey': this.commessaSet.dati[i].commessa,
    //                        'attivitaKey': this.commessaSet.dati[i].elementoWBSID,
    //                        'name': this.commessaSet.dati[i].elementoWBS.substring(11) + " - " + this.commessaSet.dati[i].descrizione,
    //                        'disabilitato': this.commessaSet.dati[i].disabilitato
    //                    };
    //                    attivitaList.push(attivita);
    //                };
    //                var attivitaListUnificate = [];
    //                var attivitaListUnificate = _.where(attivitaList, {
    //                    'commessaKey': commessaScelta
    //                });
    //                _.remove(attivitaListUnificate, {
    //                    'disabilitato': "X"
    //                });
    //                this.attivitaModel.setProperty("/attivita", []);
    //                this.attivitaModel.setProperty("/attivita", attivitaListUnificate);
    //                this.getView().byId("location").setSelectedKey(myObj.luogo);
    //                this.prevModel.setData(_.cloneDeep(myObj));
    //                var attivitaTemporanea = this.prevModel.getData();
    //                this.editModel.setData(attivitaTemporanea);
    //
    //                this.enableModel.setProperty("/dataModificabile", false);
    //                this.getView().byId("datePickerEdit").setDateValue(objData);
    //                this.enableModel.refresh();
    //                this.editModel.refresh();
    //                this.getView().getModel("editModel").refresh();
    //                this.getView().getModel("appModel").refresh();
    //                //********************************
    //
    //            } else if (this.entity === "duplicate") {
    //
    //                this.getView().byId("attivita").setEditable(true);
    //                this.getView().byId("attivita").setEnabled(true);
    //
    //                var myObj = _.find(this.getView().getModel("rapportini").getData(), {
    //                    'dataCommessa': data,
    //                    'counterFiori': counter
    //                });
    //
    //                if (!myObj) {
    //                    var myObj = _.find(this.getView().getModel("rapportini").getData(), {
    //                        'dataCommessa': data,
    //                        'counter': counter
    //                    });
    //                };
    //
    //                var attivitaGiornaliere = _.where(this.attivitaSet, {
    //                    'dataCommessa': this.data,
    //                    'delete': ""
    //                });
    //
    //                this.totOrarioFine = [];
    //                if (attivitaGiornaliere.length > 0) {
    //                    for (var i = 0; i < attivitaGiornaliere.length; i++) {
    //                        if (attivitaGiornaliere[i].delete !== "X") {
    //                            var orarioProvvisorio = attivitaGiornaliere[i].orarioFine;
    //                            var oraProvvisoria = orarioProvvisorio.split(":")[0];
    //                            var minutiProvvisori = orarioProvvisorio.split(":")[1];
    //                            orarioProvvisorio = parseFloat(oraProvvisoria + "." + minutiProvvisori);
    //                            this.totOrarioFine.push(orarioProvvisorio);
    //                        };
    //                    };
    //                } else {
    //                    this.orarioMassimo = "9.0";
    //                };
    //                var orarioMezzanotte = false;
    //                if (this.totOrarioFine.length === 0) {
    //                    this.time.setHours(maxTime.split(":")[0], maxTime.split(":")[1]);
    //                } else if (this.totOrarioFine.length > 0) {
    //                    this.orarioMassimo = this.totOrarioFine[0];
    //                    for (var i = 0; i < this.totOrarioFine.length; i++) {
    //                        if (this.totOrarioFine[i] === 00 || this.totOrarioFine[i] === 0) {
    //                            orarioMezzanotte = true;
    //                        };
    //                        if (this.totOrarioFine[i] > this.orarioMassimo) {
    //                            this.orarioMassimo = this.totOrarioFine[i];
    //                        };
    //                    };
    //                };
    //                this.orarioMassimo = this.orarioMassimo.toString();
    //                if (orarioMezzanotte === true) {
    //                    this.orarioMassimo = "00:00";
    //                    var oraMax = this.orarioMassimo.split(":")[0];
    //                    var minutiMax = this.orarioMassimo.split(".")[1];
    //                    if (!minutiMax) {
    //                        minutiMax = "00";
    //                    };
    //                } else {
    //                    var oraMax = this.orarioMassimo.split(".")[0];
    //                    var minutiMax = this.orarioMassimo.split(".")[1];
    //                    if (!minutiMax) {
    //                        minutiMax = "00";
    //                    } else if (minutiMax === "5" || minutiMax === "50") {
    //                        minutiMax = "30";
    //                    };
    //                    this.orarioMassimo = oraMax + ":" + minutiMax;
    //                };
    //                this.time.setHours(oraMax, minutiMax);
    //                var oraInizio = this.time.getHours();
    //                oraInizio = oraInizio.toString();
    //                if (oraInizio.length === 1) {
    //                    oraInizio = "0" + oraInizio;
    //                };
    //                var minutiInizio = this.time.getMinutes();
    //                minutiInizio = minutiInizio.toString();
    //                if (minutiInizio.length === 1) {
    //                    minutiInizio = minutiInizio + "0";
    //                };
    //
    //                var commessaScelta = myObj.commessa;
    //                var attivitaList = [];
    //                for (var i = 0; i < this.commessaSet.dati.length; i++) {
    //                    var attivita = {
    //                        'commessaKey': this.commessaSet.dati[i].commessa,
    //                        'attivitaKey': this.commessaSet.dati[i].elementoWBSID,
    //                        'name': this.commessaSet.dati[i].elementoWBS.substring(11) + " - " + this.commessaSet.dati[i].descrizione,
    //                        'disabilitato': this.commessaSet.dati[i].disabilitato
    //                    };
    //                    attivitaList.push(attivita);
    //                };
    //                var attivitaListUnificate = [];
    //                var attivitaListUnificate = _.where(attivitaList, {
    //                    'commessaKey': commessaScelta
    //                });
    //                _.remove(attivitaListUnificate, {
    //                    'disabilitato': "X"
    //                });
    //                this.attivitaModel.setProperty("/attivita", attivitaListUnificate);
    //                this.prevModel.setData(_.cloneDeep(myObj));
    //                var attivitaTemporanea = this.prevModel.getData();
    //                this.editModel.setData(attivitaTemporanea);
    //
    //                //setto la data selezionata precedentemente al date picker
    //                var objData = new Date();
    //                objData.setFullYear(d.substr(4));
    //                var month = parseInt(d.substr(2, 2));
    //                month = (month - 1).toString();
    //                objData.setDate(1);
    //                objData.setMonth(month);
    //                objData.setDate(d.substr(0, 2));
    //
    //                this.editModel.getData().oraInizio = oraInizio;
    //                this.editModel.getData().minutiInizio = minutiInizio;
    //                this.getView().byId("datePickerEdit").setDateValue(objData);
    //                this.enableModel.refresh();
    //                this.getView().getModel("editModel").refresh();
    //                this.getView().getModel("appModel").refresh();
    //                this.rememberCheckStateDuplicate = this.enableModel.getProperty("/straordinario");
    //                //********************************
    //
    //            } else if (this.entity === "new") {
    //                //resetto tutti gli stati degli elementi
    //
    //                this.getView().byId("attivita").setEnabled(false);
    //                this.getView().byId("attivita").setEditable(false);
    //                this.getView().byId("datePickerEdit").setEnabled(false);
    //
    //                var attivitaGiornaliere = _.where(this.attivitaSet, {
    //                    'dataCommessa': this.data,
    //                    'delete': ""
    //                });
    //
    //                this.totOrarioFine = [];
    //                if (attivitaGiornaliere.length > 0) {
    //                    for (var i = 0; i < attivitaGiornaliere.length; i++) {
    //                        if (attivitaGiornaliere[i].delete !== "X") {
    //                            var orarioProvvisorio = attivitaGiornaliere[i].orarioFine;
    //                            var oraProvvisoria = orarioProvvisorio.split(":")[0];
    //                            var minutiProvvisori = orarioProvvisorio.split(":")[1];
    //                            orarioProvvisorio = parseFloat(oraProvvisoria + "." + minutiProvvisori);
    //                            this.totOrarioFine.push(orarioProvvisorio);
    //                        };
    //                    };
    //                } else {
    //                    this.orarioMassimo = "9.0";
    //                };
    //                var orarioMezzanotte = false;
    //                if (this.totOrarioFine.length === 0) {
    //                    this.time.setHours(maxTime.split(":")[0], maxTime.split(":")[1]);
    //                } else if (this.totOrarioFine.length > 0) {
    //                    this.orarioMassimo = this.totOrarioFine[0];
    //                    for (var i = 0; i < this.totOrarioFine.length; i++) {
    //                        if (this.totOrarioFine[i] === 00 || this.totOrarioFine[i] === 0) {
    //                            orarioMezzanotte = true;
    //                        };
    //                        if (this.totOrarioFine[i] > this.orarioMassimo) {
    //                            this.orarioMassimo = this.totOrarioFine[i];
    //                        };
    //                    };
    //                };
    //                this.orarioMassimo = this.orarioMassimo.toString();
    //                if (orarioMezzanotte === true) {
    //                    this.orarioMassimo = "00:00";
    //                    var oraMax = this.orarioMassimo.split(":")[0];
    //                    var minutiMax = this.orarioMassimo.split(".")[1];
    //                    if (!minutiMax) {
    //                        minutiMax = "00";
    //                    };
    //                } else {
    //                    var oraMax = this.orarioMassimo.split(".")[0];
    //                    var minutiMax = this.orarioMassimo.split(".")[1];
    //                    if (!minutiMax) {
    //                        minutiMax = "00";
    //                    } else if (minutiMax === "5" || minutiMax === "50") {
    //                        minutiMax = "30";
    //                    };
    //                    this.orarioMassimo = oraMax + ":" + minutiMax;
    //                };
    //                this.time.setHours(oraMax, minutiMax);
    //                var oraInizio = this.time.getHours();
    //                oraInizio = oraInizio.toString();
    //                if (oraInizio.length === 1) {
    //                    oraInizio = "0" + oraInizio;
    //                };
    //                var minutiInizio = this.time.getMinutes();
    //                minutiInizio = minutiInizio.toString();
    //                if (minutiInizio.length === 1) {
    //                    minutiInizio = minutiInizio + "0";
    //                };
    //
    //                this.editModel.getData().commessa = "";
    //                this.editModel.getData().attivita = "";
    //                this.editModel.getData().descrizione = "";
    //                this.editModel.getData().durata = "";
    //                this.editModel.getData().note = "";
    //                this.editModel.getData().oraInizio = oraInizio;
    //                this.editModel.getData().minutiInizio = minutiInizio;
    //                this.editModel.getData().tipoOrario = ""; // il tipoOrario = "" è il tipoOrario = "Ordinario"
    //                var objData = new Date();
    //                if (data) {
    //                    objData.setFullYear(data.substr(6));
    //                    var month = parseInt(data.substr(3, 2));
    //                    objData.setDate(1);
    //                    objData.setMonth(month - 1);
    //                    objData.setDate(data.substr(0, 2));
    //                };
    //                this.enableModel.setProperty("/dataModificabile", false);
    //                this.getView().byId("datePickerEdit").setDateValue(objData);
    //                this.getView().setModel(this.editModel, "editModel");
    //                this.getView().byId("fatturabile").setSelected(false);
    //            };
    //
    //            this.getView().setModel(this.enableModel, "enable");
    //            this.getView().getModel("editModel").refresh();
    //            this.getView().getModel("appModel").refresh();
    //        };
    //    },
    //    enableOEPTabNavigation: function (id) {
    //        var that = this;
    //        this.qtyInputs = $.find('input[id*=' + id + '][disabled!=disabled]:visible');
    //        jQuery(this.qtyInputs[0]).focus();
    //        jQuery(this.qtyInputs[0]).select();
    //        $.each(this.qtyInputs, _.bind(function (i, el) {
    //
    //            $(el).on('keydown', function (oEvent) {
    //
    //                if (oEvent.keyCode === 13) {
    //                    return null;
    //                    oEvent.preventDefault();
    //                };
    //            });
    //        }, that));
    //    },
    //
    //    onSelectCommessaChange: function (evt) {
    //        this.getView().byId("attivita").setEditable(true);
    //        this.getView().byId("attivita").setEnabled(true);
    //        var commessaScelta = this.getView().byId("commessa").getSelectedKey();
    //        var attivitaList = [];
    //        for (var i = 0; i < this.commessaSet.dati.length; i++) {
    //            var attivita = {
    //                'commessaKey': this.commessaSet.dati[i].commessa,
    //                'attivitaKey': this.commessaSet.dati[i].elementoWBSID,
    //                'name': this.commessaSet.dati[i].elementoWBS.substring(11) + " - " + this.commessaSet.dati[i].descrizione,
    //                'disabilitato': this.commessaSet.dati[i].disabilitato
    //            };
    //            attivitaList.push(attivita);
    //        };
    //        var attivitaListUnificate = [];
    //        var attivitaListUnificate = _.where(attivitaList, {
    //            'commessaKey': commessaScelta
    //        });
    //        _.remove(attivitaListUnificate, {
    //            'disabilitato': "X"
    //        });
    //        if (this.getView().byId("attivita").getSelectedItem()) {
    //            var attivitaSelect = this.getView().byId("attivita");
    //            var item = attivitaSelect.getSelectedItemId();
    //            this.attivitaModel.setProperty("/attivita", attivitaListUnificate);
    //            attivitaSelect.setSelectedIndex(0);
    //            var attivitaKey = attivitaSelect.getSelectedItem().mProperties.key;
    //            attivitaSelect.setSelectedKey(attivitaKey);
    //        } else {
    //            this.attivitaModel.setProperty("/attivita", attivitaListUnificate);
    //        };
    //        this.attivitaModel.refresh();
    //    },
    //
    //    onSelectChange: function (evt) {
    //        var items = evt.oSource.mBindingInfos.items.binding.oList;
    //        var verified = false;
    //        for (i = 0; i < items.length; i++) {
    //            if (evt.mParameters.value === items[i].name && evt.mParameters.value !== "" && evt.mParameters.value !== undefined || evt.mParameters.value === items[i].descrizione) {
    //                verified = true;
    //                break;
    //            };
    //        };
    //        if (verified) {
    //            evt.oSource.setValueState(sap.ui.core.ValueState.None);
    //        } else {
    //            evt.oSource.setValueState(sap.ui.core.ValueState.Error);
    //        };
    //    },
    //
    //    selectnonFatturabile: function (evt) {
    //        var o = evt.getSource();
    //        if (o.getSelected() == false) {
    //            this.editModel.setProperty("/nonFatturabile", 1);
    //        } else {
    //            this.editModel.setProperty("/nonFatturabile", 0);
    //        };
    //        this.getView().getModel("appModel").refresh();
    //    },
    //
    //    onSaveOnLocal: function (evt) {
    //
    //        this.ottoOreSuperate = false;
    //        this.oreSuperateGiorno = false;
    //        this.statoOraInizioErrato = false;
    //        this.statoAttivitaErrato = false;
    //        this.inserimentoAttivitaErrore = false;
    //        this.statoCommessaErrato = false;
    //        this.statoOreErrato = false;
    //        this.statoTipoOrario = false;
    //        this.statoLuogoLavoroErrato = false;
    //        this.oreSuperateGiornoDopoMezzanotte = false;
    //        this.statoTipoOrarioErrato = false;
    //        this.statoDescrizioneErrore = false;
    //        this.counterDaCancellare = false;
    //        this.dataUltimaModifica = undefined;
    //        this.oraUltimaModifica = undefined;
    //        this.dataToSave = undefined;
    //        this.noteSpeseDaCancellare = false;
    //        this.noteSpeseDaCancellare = false;
    //
    //        var dataToSync = utils.LocalStorage.getItem("attivitaSet");
    //        var dataToSave = this.getView().getModel("editModel").getData();
    //        var checkDateA = new Date(this.editModel.getProperty("/date"));
    //        var checkDate = utils.Util.formatDate(checkDateA);
    //        this.dataFormattata = checkDate;
    //        var dataValue = this.getView().byId("datePickerEdit").getDateValue();
    //        var day = dataValue.getDate().toString();
    //        if (day.length < 2) {
    //            day = "0" + day;
    //        };
    //        var month = (dataValue.getMonth() + 1).toString();
    //        if (month.length < 2) {
    //            month = "0" + month;
    //        };
    //        var year = dataValue.getFullYear().toString();
    //        this.data = day + "/" + month + "/" + year;
    //
    //        if (!checkDate) {
    //            var d = this.editModel.getProperty("/dataCommessa");
    //            var dateToSync = new Date(d);
    //            dateToSync = d.toLocaleDateString();
    //            var giorno = d.getDate();
    //            if (giorno < 10) {
    //                giorno = "0" + giorno;
    //            };
    //            var mese = d.getMonth() + 1;
    //            if (mese < 10) {
    //                mese = "0" + mese
    //            };
    //            dateToSync = giorno + "/" + mese + "/" + d.getFullYear();
    //            this.editModel.setProperty("/dataCommessa", dateToSync.toString());
    //        };
    //        this.editModel.setProperty("/dataCommessa", checkDate);
    //
    //        ////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //        if (this.getView().byId("commessa").getSelectedItem()) {
    //            var commessa = this.getView().byId("commessa").getSelectedKey();
    //            var descrizioneCommessa = this.getView().byId("commessa").getSelectedItem().mProperties.text.split("- ")[1];
    //        } else {
    //            this.statoCommessaErrato = true;
    //        };
    //        var luogoLavoro = dataToSave.luogo;
    //        if (luogoLavoro === "" || luogoLavoro === undefined) {
    //            this.statoLuogoLavoroErrato = true;
    //        };
    //        if (this.getView().byId("attivita").getSelectedItem()) {
    //            var attivita = this.getView().byId("attivita").getSelectedKey();
    //            var lunghezzaDescrizione = this.getView().byId("attivita").getSelectedItem().mProperties.text.split("- ").length;
    //            var descrizioneAttivita = "";
    //            for (var c = 1; c < lunghezzaDescrizione; c++) {
    //                if (c === 1) {
    //                    descrizioneAttivita = this.getView().byId("attivita").getSelectedItem().mProperties.text.split("- ")[c];
    //                } else {
    //                    descrizioneAttivita = descrizioneAttivita + "- " + this.getView().byId("attivita").getSelectedItem().mProperties.text.split("- ")[c];
    //                };
    //            };
    //        } else {
    //            this.statoAttivitaErrato = true;
    //        };
    //        var nonFatturabile = this.getView().byId("fatturabile").getSelected();
    //        if (nonFatturabile === false) {
    //            nonFatturabile = "";
    //        } else {
    //            nonFatturabile = "X";
    //        };
    //        if (this.getView().byId("idTipoOrario").getSelectedItem()) {
    //            var tipoOrario = this.getView().byId("idTipoOrario").getSelectedKey();
    //            var descrizioneTipoOrario = this.getView().byId("idTipoOrario").getSelectedItem().mProperties.text;
    //        } else {
    //            this.statoTipoOrarioErrato = true;
    //        };
    //        var durata = dataToSave.durata;
    //        var descrizione = dataToSave.descrizione;
    //        var note = dataToSave.note;
    //
    //        var meseInserimento = parseInt((checkDate.split("/"))[1]);
    //        var dataOggi = new Date();
    //        var meseOggi = dataOggi.getMonth() + 1;
    //        if (meseInserimento === meseOggi) {
    //            var stato = "10";
    //        } else {
    //            var stato = "20";
    //        };
    //
    //        var dataUltimaModifica = new Date();
    //        dataUltimaModifica = dataUltimaModifica.getDate() + "/" + parseInt(dataUltimaModifica.getMonth() + 1) + "/" + +dataUltimaModifica.getFullYear();
    //        var oraUltimaModifica = new Date();
    //        if (oraUltimaModifica.getHours().toString().length < 2) {
    //            var oraModifica = "0" + oraUltimaModifica.getHours();
    //        } else {
    //            var oraModifica = oraUltimaModifica.getHours()
    //        };
    //        if (oraUltimaModifica.getMinutes().toString().length < 2) {
    //            var minutiModifica = "0" + oraUltimaModifica.getMinutes();
    //        } else {
    //            var minutiModifica = oraUltimaModifica.getMinutes()
    //        };
    //        if (oraUltimaModifica.getSeconds().toString().length < 2) {
    //            var secondiModifica = "0" + oraUltimaModifica.getSeconds();
    //        } else {
    //            var secondiModifica = oraUltimaModifica.getSeconds()
    //        };
    //        oraUltimaModifica = oraModifica + ":" + minutiModifica + ":" + secondiModifica;
    //
    //        /////////////////////////// in caso di modifica rapportino tolgo dal locl storage il precedente e poi modifico le eventuali
    //        /////////////////////////// note spese corrispondenti con la nuova data e la nuova wbs
    //        if (this.entity === "edit") {
    //            if (dataToSave.counterFiori) {
    //                var editEntityRemoved = _.remove(dataToSync, _.find(dataToSync, {
    //                    'dataCommessa': this.dataIniziale,
    //                    'counterFiori': dataToSave.counterFiori
    //                }));
    //            } else {
    //                var editEntityRemoved = _.remove(dataToSync, _.find(dataToSync, {
    //                    'dataCommessa': this.dataIniziale,
    //                    'counter': dataToSave.counter
    //                }));
    //            };
    //            if (this.dataIniziale !== this.data) {
    //                editEntityRemoved[0].delete = "X";
    //                this.max = 0;
    //                for (var i = 0; i < dataToSync.length; i++) {
    //                    if (dataToSync[i].counterFiori) {
    //                        if (parseInt(dataToSync[i].counterFiori) > this.max) {
    //                            this.max = parseInt(dataToSync[i].counterFiori);
    //                        };
    //                    };
    //                };
    //                this.counterDaCancellare = true;
    //                editEntityRemoved[0].counterFiori = (this.max + 1).toString();
    //                editEntityRemoved[0].dataUltimaModifica = dataUltimaModifica;
    //                editEntityRemoved[0].oraUltimaModifica = oraUltimaModifica;
    //                dataToSync.push(editEntityRemoved[0]);
    //            };
    //        };
    //
    //        this.max = 0;
    //        for (var i = 0; i < dataToSync.length; i++) {
    //            if (dataToSync[i].counterFiori) {
    //                if (parseInt(dataToSync[i].counterFiori) > this.max) {
    //                    this.max = parseInt(dataToSync[i].counterFiori);
    //                };
    //            };
    //        };
    //        var counterFiori = (this.max + 1).toString();
    //
    //        var attivitaGiornaliere = _.where(dataToSync, {
    //            'dataCommessa': this.data,
    //            'delete': ""
    //        });
    //
    //        if (tipoOrario === "") { //tipoOrario = "" è il tipoOrario = Ordinario
    //            this.totOre = 0;
    //            for (var i = 0; i < attivitaGiornaliere.length; i++) {
    //                if (attivitaGiornaliere[i].tipoOrario === "" && attivitaGiornaliere[i].delete !== "X") {
    //                    this.totOre = this.totOre + parseInt(attivitaGiornaliere[i].durata);
    //                    if (this.totOre + parseFloat(durata) > 8) {
    //                        this.ottoOreSuperate = true;
    //                        break;
    //                    };
    //                };
    //            };
    //        };
    //
    //        ////////// CONTROLLO SE SUPERO LE 24 ORE GIORNALIERE //////////
    //        this.oraInizio = 0;
    //        this.oraFine = 0;
    //
    //        if (attivitaGiornaliere.length > 0) {
    //            for (var i = 0; i < attivitaGiornaliere.length; i++) {
    //                if (attivitaGiornaliere[i].delete !== "X") {
    //                    this.oraFine = this.oraFine + parseFloat(attivitaGiornaliere[i].durata);
    //                };
    //            };
    //        };
    //
    //        if (oraFine > 24) {
    //            this.oreSuperateGiorno = true;
    //        };
    //
    //        ////////// ORA GUARDO L'ORA DI INIZIO E DI FINE E VEDO SE SI PUO' EFFETTIVAMENTE INSERIRE //////////
    //
    //        var oraInizio = parseInt(this.getView().byId("idOraInizio").getValue());
    //        if ((!oraInizio && oraInizio !== 0) || oraInizio.toString().indexOf(".") >= 0 || oraInizio.toString().indexOf(",") >= 0) {
    //            this.oraInizioNonValida = true;
    //        } else {
    //            this.oraInizioNonValida = false;
    //        };
    //        var minutiInizio = parseInt(this.getView().byId("idMinutiInizio").getSelectedKey());
    //        if (minutiInizio === 30) {
    //            minutiInizio = 50;
    //        };
    //        var orarioInizio = parseFloat(oraInizio + "." + minutiInizio);
    //        var orarioFine = orarioInizio + parseFloat(durata);
    //        if (orarioFine > 24) {
    //            this.oreSuperateGiornoDopoMezzanotte = true; // ho verificato che non venga inserita un'attività per esempio dalle 17 alle 01
    //        };
    //
    //        var oraFine = orarioFine.toString().split(".")[0];
    //        var minutiFine = orarioFine.toString().split(".")[1];
    //        if (minutiFine === "50" || minutiFine === "5") {
    //            minutiFine = "30";
    //        };
    //
    //        if (!minutiFine) {
    //            var minutiFine = 0;
    //        };
    //
    //        for (var i = 0; i < attivitaGiornaliere.length; i++) {
    //            if (attivitaGiornaliere[i].delete !== "X") {
    //                var oraInizioAttivitaIesima = parseInt(attivitaGiornaliere[i].orarioInizio.split(":")[0]);
    //                var oraFineAttivitaIesima = parseInt(attivitaGiornaliere[i].orarioFine.split(":")[0]);
    //                var minutiInizioAttivitaIesima = parseInt(attivitaGiornaliere[i].orarioInizio.split(":")[1]);
    //                if (minutiInizioAttivitaIesima === 30) {
    //                    minutiInizioAttivitaIesima = 50;
    //                };
    //                var minutiFineAttivitaIesima = parseInt(attivitaGiornaliere[i].orarioFine.split(":")[1]);
    //                if (minutiFineAttivitaIesima === 30) {
    //                    minutiFineAttivitaIesima = 50;
    //                };
    //                var orarioInizioAttivitaIesima = parseFloat(oraInizioAttivitaIesima + "." + minutiInizioAttivitaIesima);
    //                var orarioFineAttivitaIesima = parseFloat(oraFineAttivitaIesima + "." + minutiFineAttivitaIesima);
    //                if (orarioInizio >= orarioFineAttivitaIesima || orarioFine <= orarioInizioAttivitaIesima) {
    //                    this.inserimentoAttivitaErrore = false;
    //                } else {
    //                    this.inserimentoAttivitaErrore = true;
    //                    break;
    //                };
    //            };
    //        };
    //
    //        if (oraInizio.toString().length === 1) {
    //            oraInizio = "0" + oraInizio;
    //        };
    //        if (oraFine.toString().length === 1) {
    //            oraFine = "0" + oraFine;
    //        };
    //        if (minutiInizio.toString().length === 1) {
    //            minutiInizio = minutiInizio + "0";
    //        };
    //        if (minutiFine.toString().length === 1) {
    //            minutiFine = minutiFine + "0";
    //        };
    //
    //        if (minutiInizio === 50 || minutiInizio === "50") {
    //            minutiInizio = 30;
    //        };
    //        orarioInizio = oraInizio + ":" + minutiInizio;
    //        if (minutiFine === 50 || minutiFine === "50") {
    //            minutiFine = 30;
    //        };
    //        orarioFine = oraFine + ":" + minutiFine;
    //
    //        durata = durata.toString();
    //        if (durata.length === 1) {
    //            durata = durata + ".00";
    //        } else if (durata.length === 3) {
    //            durata = durata + "0";
    //        };
    //
    //        var statoCommessa = this.getView().byId("commessa").getValueState();
    //        if (statoCommessa === "Error") {
    //            this.statoCommessaErrato = true;
    //        };
    //
    //        var statoAttivita = this.getView().byId("attivita").getValueState();
    //        if (statoAttivita === "Error") {
    //            this.statoAttivitaErrato = true;
    //        };
    //
    //        var statoOraInizio = this.getView().byId("idOraInizio").getValueState();
    //        if (statoOraInizio === "Error") {
    //            this.statoOraInizioErrato = true;
    //        };
    //
    //        var statoOre = this.getView().byId("oreLavoro").getValueState();
    //        if (statoOre === "Error" || !durata) {
    //            this.statoOreErrato = true;
    //        };
    //
    //        var statoLuogoLavoro = this.getView().byId("location").getValueState();
    //        if (statoLuogoLavoro === "Error") {
    //            this.statoLuogoLavoroErrato = true;
    //        };
    //
    //        if (this.getView().byId("idDescrizione").getValue() === "") {
    //            this.statoDescrizioneErrore = true;
    //        };
    //
    //        //////// APPLICO I CONTROLLI FACENDO COMPARIRE IL MESSAGE BOX CORRISPONDENTE /////////////////////
    //        if (this.ottoOreSuperate === true) {
    //            var that = this;
    //            sap.m.MessageBox.error(this._getLocaleText("OreSuperateWbs"), {
    //                title: this._getLocaleText("WARNING"),
    //                action: that.fareFocus()
    //            });
    //        } else if (this.oraInizioNonValida === true) {
    //            var that = this;
    //            sap.m.MessageBox.error(this._getLocaleText("oraInizioNonValida"), {
    //                title: this._getLocaleText("WARNING"),
    //                action: that.fareFocus()
    //            });
    //        } else if (this.oreSuperateGiorno === true) {
    //            var that = this;
    //            sap.m.MessageBox.error(this._getLocaleText("OreSuperateGiorno"), {
    //                title: this._getLocaleText("WARNING"),
    //                action: that.fareFocus()
    //            });
    //        } else if (this.statoOraInizioErrato === true) {
    //            var that = this;
    //            sap.m.MessageBox.error(this._getLocaleText("InserisciOraValida"), {
    //                title: this._getLocaleText("WARNING"),
    //                action: that.fareFocus()
    //            });
    //        } else if (this.oreSuperateGiornoDopoMezzanotte === true) {
    //            var that = this;
    //            sap.m.MessageBox.error(this._getLocaleText("oreSuperateGiornoDopoMezzanotte"), {
    //                title: this._getLocaleText("WARNING"),
    //                action: that.fareFocus()
    //            });
    //        } else if (this.statoAttivitaErrato === true) {
    //            var that = this;
    //            sap.m.MessageBox.error(this._getLocaleText("InserisciAttivitàCorretta"), {
    //                title: this._getLocaleText("WARNING"),
    //                action: that.fareFocus()
    //            });
    //        } else if (this.inserimentoAttivitaErrore === true) {
    //            var that = this;
    //            sap.m.MessageBox.error(this._getLocaleText("PeriodoAttività") + " " + orarioInizio + " - " + orarioFine, {
    //                title: this._getLocaleText("WARNING"),
    //                action: that.fareFocus()
    //            });
    //        } else if (this.statoCommessaErrato === true) {
    //            var that = this;
    //            sap.m.MessageBox.error(this._getLocaleText("InserisciCommessa"), {
    //                title: this._getLocaleText("WARNING"),
    //                action: that.fareFocus()
    //            });
    //        } else if (this.statoOreErrato === true) {
    //            var that = this;
    //            sap.m.MessageBox.error(this._getLocaleText("InserisciDurata"), {
    //                title: this._getLocaleText("WARNING"),
    //                action: that.fareFocus()
    //            });
    //        } else if (this.statoLuogoLavoroErrato === true) {
    //            var that = this;
    //            sap.m.MessageBox.error(this._getLocaleText("InserisciLuogoLavoro"), {
    //                title: this._getLocaleText("WARNING"),
    //                action: that.fareFocus()
    //            });
    //        } else if (this.statoDescrizioneErrore === true) {
    //            var that = this;
    //            sap.m.MessageBox.error(this._getLocaleText("InserisciDecrizione"), {
    //                title: this._getLocaleText("WARNING"),
    //                action: that.fareFocus()
    //            });
    //        } else if (this.statoTipoOrarioErrato === true) {
    //            var that = this;
    //            sap.m.MessageBox.error(this._getLocaleText("InserisciTipoOrario"), {
    //                title: this._getLocaleText("WARNING"),
    //                action: that.fareFocus()
    //            });
    //        }
    //        //////// CONTROLLI TERMINATI E PROCEDO ALL'AGGIUNTA DELL'ATTIVITA AL LOCALE STORAGE /////////////////////
    //        else {
    //            //////// verifico se salvo un'attività con la commessa non più abilitata ////////////////
    //            var keyCommessaScelta = this.getView().byId("commessa").getSelectedItem().mProperties.key;
    //            var obj = _.find(this.getView().getModel("appModel"), {
    //                'key': keyCommessaScelta
    //            });
    //            if (obj) {
    //                if (obj.disabilitato === "X") {
    //                    var that = this;
    //                    sap.m.MessageBox.error(this._getLocaleText("CommessaCheNonSalvera"), {
    //                        title: this._getLocaleText("WARNING"),
    //                        action: that.fareFocus()
    //                    });
    //                };
    //            };
    //            var coloriSet = utils.LocalStorage.getItem("commessaColorSet");
    //            var colore = _.find(coloriSet, {
    //                'commessa': commessa
    //            }).colore;
    //
    //            if (!this.counter || this.counterDaCancellare === true) {
    //                this.counter = "";
    //            };
    //
    //            var cid = utils.LocalStorage.getItem("cidSet")[0];
    //            cid = cid.userID;
    //
    //            var dataToSave = {
    //                'commessa': commessa,
    //                'attivita': attivita,
    //                'descrizioneAttivita': descrizioneAttivita,
    //                'descrizioneCommessa': descrizioneCommessa,
    //                'luogo': luogoLavoro,
    //                'dataCommessa': this.data,
    //                'nonFatturabile': nonFatturabile,
    //                'tipoOrario': tipoOrario,
    //                'descrizioneTipoOrario': descrizioneTipoOrario,
    //                'durata': durata,
    //                'orarioInizio': orarioInizio + ":00", // quelli aggiunti alla fine sono i secondi
    //                'orarioFine': orarioFine + ":00", // quelli aggiunti alla fine sono i secondi
    //                'counter': this.counter,
    //                'counterFiori': counterFiori,
    //                'color': colore,
    //                'stato': stato,
    //                'descrizione': descrizione,
    //                'note': note,
    //                'dataUltimaModifica': dataUltimaModifica,
    //                'oraUltimaModifica': oraUltimaModifica,
    //                'cid': cid,
    //                'counter': this.counter,
    //                'delete': ""
    //            };
    //            dataToSync.push(dataToSave);
    //            utils.LocalStorage.setItem("attivitaSet", dataToSync);
    //            var data = this.data;
    //            this.descrizioneAttivita = descrizioneAttivita;
    //
    //            if (this.entity === "edit") {
    //                var notaSpese = utils.LocalStorage.getItem("notaSpeseSet");
    //
    //                /// verifico se ci sono noteSpese corrispondenti
    //                var noteSpeseCorrispondenti = _.find(notaSpese, {
    //                    'dataNotaSpese': this.dataIniziale,
    //                    'attivita': this.wbsIniziale
    //                });
    //                if (noteSpeseCorrispondenti) {
    //
    //
    //                    ///verifico se c'è almeno un altro rapportino con la stessa data e con la stessa wbs
    //                    var rapportiniSet = utils.LocalStorage.getItem("attivitaSet");
    //                    var altroRapportino = _.where(rapportiniSet, {
    //                        'dataCommessa': this.dataIniziale,
    //                        'attivita': this.wbsIniziale,
    //                        'delete': ""
    //                    });
    //
    //                    ///// se non c'è un altro rapportino con stesse data e wbs, voglio che le note spese vengano cancellate, altrimenti no!
    //                    this.noteSpeseCancellate = undefined;
    //                    if (this.wbsIniziale === attivita && this.dataIniziale === this.data) {
    //                        if (altroRapportino.length < 2) {
    //                            this.noteSpeseCancellate = _.remove(notaSpese, {
    //                                'dataNotaSpese': this.dataIniziale,
    //                                'attivita': this.wbsIniziale
    //                            });
    //                            utils.LocalStorage.setItem("notaSpeseSet", notaSpese);
    //                            this.noteSpeseDaCancellare = true;
    //                        } else {
    //                            this.noteSpeseCancellate = _.where(notaSpese, {
    //                                'dataNotaSpese': this.dataIniziale,
    //                                'attivita': this.wbsIniziale
    //                            });
    //                        };
    //                    } else {
    //                        if (altroRapportino.length < 1) {
    //                            this.noteSpeseCancellate = _.remove(notaSpese, {
    //                                'dataNotaSpese': this.dataIniziale,
    //                                'attivita': this.wbsIniziale
    //                            });
    //                            utils.LocalStorage.setItem("notaSpeseSet", notaSpese);
    //                            this.noteSpeseDaCancellare = true;
    //                        } else {
    //                            this.noteSpeseCancellate = _.where(notaSpese, {
    //                                'dataNotaSpese': this.dataIniziale,
    //                                'attivita': this.wbsIniziale
    //                            });
    //                        };
    //                    };
    //
    //                    this.dataUltimaModifica = dataUltimaModifica;
    //                    this.oraUltimaModifica = oraUltimaModifica;
    //                    this.dataToSave = dataToSave;
    //                    this.descrizioneAttivita = descrizioneAttivita;
    //                    if (this.wbsIniziale === dataToSave.attivita) {
    //                        this.modificaNoteSpese();
    //                    } else {
    //                        sap.m.MessageBox.show(this._getLocaleText("confirmDuplicateNoteSpese"),
    //                            sap.m.MessageBox.Icon.NONE,
    //                            this._getLocaleText("confirmDuplicateNoteSpeseTitle"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
    //                            _.bind(this.modificaNoteSpese, this)
    //                        );
    //                    };
    //                } else {
    //                    if (parseInt(dataToSave.dataCommessa.split("/")[1]) !== (new Date().getMonth() + 1)) {
    //                        sap.m.MessageBox.show(this._getLocaleText("confirmExitToHome"),
    //                            sap.m.MessageBox.Icon.NONE,
    //                            this._getLocaleText("confirmExitToHomeTitle"), [sap.m.MessageBox.Action.YES],
    //                            _.bind(this.exitToHome, this)
    //                        );
    //                    } else {
    //                        this.getView().getModel("appModel").setProperty("/arrivatoDaAltrePagine", true);
    //                        this.router.navTo("home");
    //                        sap.m.MessageToast.show(this._getLocaleText("attivita") + ": " + descrizioneAttivita + " " + this._getLocaleText("aggiuntaInData") + ": " + data, {
    //                            duration: 1000
    //                        });
    //                    };
    //                };
    //            } else {
    //                if (parseInt(dataToSave.dataCommessa.split("/")[1]) !== (new Date().getMonth() + 1)) {
    //                    sap.m.MessageBox.show(this._getLocaleText("confirmExitToHome"),
    //                        sap.m.MessageBox.Icon.NONE,
    //                        this._getLocaleText("confirmExitToHomeTitle"), [sap.m.MessageBox.Action.YES],
    //                        _.bind(this.exitToHome, this)
    //                    );
    //                } else {
    //                    this.getView().getModel("appModel").setProperty("/arrivatoDaAltrePagine", true);
    //                    this.router.navTo("home");
    //                    sap.m.MessageToast.show(this._getLocaleText("attivita") + ": " + descrizioneAttivita + " " + this._getLocaleText("aggiuntaInData") + ": " + data, {
    //                        duration: 1000
    //                    });
    //                };
    //            };
    //        };
    //    },
    //
    //    exitToHome: function (evt) {
    //        this.dataRapportino = this.data;
    //        if (evt === sap.m.MessageBox.Action.YES) {
    //            this.getView().getModel("appModel").setProperty("/arrivatoDaAltrePagine", true);
    //            this.router.navTo("home");
    //            sap.m.MessageToast.show(this._getLocaleText("attivita") + ": " + this.descrizioneAttivita + " " + this._getLocaleText("aggiuntaInData") + ": " + this.dataRapportino, {
    //                duration: 1000
    //            });
    //        };
    //    },
    //
    //    modificaNoteSpese: function (evt) {
    //
    //        var notaSpese = utils.LocalStorage.getItem("notaSpeseSet");
    //
    //        if (evt === sap.m.MessageBox.Action.NO) {
    //            for (var n = 0; n < this.noteSpeseCancellate.length; n++) {
    //                this.noteSpeseCancellate[n].dataUltimaModifica = this.dataUltimaModifica;
    //                this.noteSpeseCancellate[n].oraUltimaModifica = this.oraUltimaModifica;
    //                this.noteSpeseCancellate[n].dataNotaSpese = this.dataIniziale;
    //                this.noteSpeseCancellate[n].attivita = this.wbsIniziale;
    //                this.noteSpeseCancellate[n].delete = "X";
    //                this.max = 0;
    //                for (var j = 0; j < notaSpese.length; j++) {
    //                    if (parseInt(notaSpese[j].counterFiori) > this.max) {
    //                        this.max = parseInt(notaSpese[j].counterFiori);
    //                    };
    //                };
    //                if (!this.noteSpeseCancellate[n].counterFiori || this.noteSpeseCancellate[n].counterFiori !== "") {
    //                    this.noteSpeseCancellate[n].counterFiori = (this.max + 1).toString();
    //                };
    //                notaSpese.push(this.noteSpeseCancellate[n]);
    //            };
    //        } else {
    //            this.noteSpeseCancellateClone = _.cloneDeep(this.noteSpeseCancellate);
    //            if (this.noteSpeseDaCancellare === true) {
    //                for (var n = 0; n < this.noteSpeseCancellate.length; n++) {
    //                    this.noteSpeseCancellate[n].dataUltimaModifica = this.dataUltimaModifica;
    //                    this.noteSpeseCancellate[n].oraUltimaModifica = this.oraUltimaModifica;
    //                    this.noteSpeseCancellate[n].dataNotaSpese = this.dataIniziale;
    //                    this.noteSpeseCancellate[n].attivita = this.wbsIniziale;
    //                    this.noteSpeseCancellate[n].delete = "X";
    //                    this.max = 0;
    //                    for (var j = 0; j < notaSpese.length; j++) {
    //                        if (parseInt(notaSpese[j].counterFiori) > this.max) {
    //                            this.max = parseInt(notaSpese[j].counterFiori);
    //                        };
    //                    };
    //                    if (!this.noteSpeseCancellate[n].counterFiori || this.noteSpeseCancellate[n].counterFiori !== "") {
    //                        this.noteSpeseCancellate[n].counterFiori = (this.max + 1).toString();
    //                    };
    //                    notaSpese.push(this.noteSpeseCancellate[n]);
    //                };
    //            };
    //            for (var b = 0; b < this.noteSpeseCancellateClone.length; b++) {
    //                this.max = 0;
    //                for (var j = 0; j < notaSpese.length; j++) {
    //                    if (parseInt(notaSpese[j].counterFiori) > this.max) {
    //                        this.max = parseInt(notaSpese[j].counterFiori);
    //                    };
    //                };
    //                if (!this.noteSpeseCancellateClone[b].counterFiori || this.noteSpeseCancellateClone[b].counterFiori !== "") {
    //                    this.noteSpeseCancellateClone[b].counterFiori = (this.max + 1).toString();
    //                };
    //                this.noteSpeseCancellateClone[b].dataUltimaModifica = this.dataUltimaModifica;
    //                this.noteSpeseCancellateClone[b].oraUltimaModifica = this.oraUltimaModifica;
    //                this.noteSpeseCancellateClone[b].dataNotaSpese = this.data;
    //                this.noteSpeseCancellateClone[b].attivita = this.dataToSave.attivita;
    //                this.noteSpeseCancellateClone[b].numeroTrasferta = "";
    //                this.noteSpeseCancellateClone[b].numeroPeriodoTrasferta = "";
    //                notaSpese.push(this.noteSpeseCancellateClone[b]);
    //            };
    //        };
    //        utils.LocalStorage.setItem("notaSpeseSet", notaSpese);
    //        var data = this.data;
    //        var descrizioneAttivita = this.descrizioneAttivita;
    //        if (parseInt(this.dataToSave.dataCommessa.split("/")[1]) !== (new Date().getMonth() + 1)) {
    //            sap.m.MessageBox.show(this._getLocaleText("confirmExitToHome"),
    //                sap.m.MessageBox.Icon.NONE,
    //                this._getLocaleText("confirmExitToHomeTitle"), [sap.m.MessageBox.Action.YES],
    //                _.bind(this.exitToHome, this)
    //            );
    //        } else {
    //            this.getView().getModel("appModel").setProperty("/arrivatoDaAltrePagine", true);
    //            this.router.navTo("home");
    //            sap.m.MessageToast.show(this._getLocaleText("attivita") + ": " + descrizioneAttivita + " " + this._getLocaleText("aggiuntaInData") + ": " + data, {
    //                duration: 1000
    //            });
    //        };
    //    },
    //
    //    fareFocus: function () {
    //        jQuery(this.qtyInputs[0]).focus();
    //    },
    //
    //    onCancelPress: function (evt) {
    //        //resetto campi e fields
    //        if (this.entity === "new") {
    //            this.resetFields();
    //        };
    //        this.resetStates();
    //
    //        if (this.getView().getModel("appStatus").getProperty("/fromAdd") === true) {
    //            this.editModelModel.refresh();
    //            this.getView().getModel("appModel").setProperty("/arrivatoDaAltrePagine", true);
    //            this.router.navTo("home");
    //        } else {
    //            this.getView().getModel("appStatus").setProperty("/fromAdd", false);
    //            this.editModel.setData(this.prevModel.getData());
    //            this.editModel.refresh();
    //            var dataPattern = this.getView().getModel("appModel").getProperty("/dataPattern");
    //            var commessa = this.getView().getModel("appModel").getProperty("/comm");
    //            this.getView().getModel("appModel").setProperty("/arrivatoDaAltrePagine", true);
    //            this.router.navTo("home");
    //        };
    //        this.getView().getModel("appModel").refresh();
    //    },
    //
    //    _getLocaleText: function (key) {
    //        return this.getView().getModel("i18n").getProperty(key);
    //    },
    //
    //    resetFields: function () {
    //        this.getView().byId("commessa").setValue("");
    //        this.getView().byId("oreLavoro").setValue("");
    //        this.getView().byId("attivita").setValue("");
    //    },
    //
    //    resetStates: function () {
    //        this.getView().byId("oreLavoro").setValueState("None");
    //        this.getView().byId("commessa").setValueState("None");
    //        this.getView().byId("attivita").setValueState("None");
    //    },
    //
    //    onSelectTipoOrarioChange: function (evt) {
    //        var val = this.getView().byId("oreLavoro").getValue();
    //        var obj = this.getView().byId("oreLavoro");
    //        if (val.split(".")[1]) {
    //            if (val.split(".")[1] !== "5" && val.split(".")[1] !== "50") {
    //                obj.setValueState("Error");
    //            };
    //        };
    //        if (val.split(",")[1]) {
    //            if (val.split(",")[1] !== "5" && val.split(",")[1] !== "50") {
    //                obj.setValueState("Error");
    //            };
    //        };
    //        val = parseFloat(val);
    //        if (!val || val < 0.5 || val > 24 || val[val.length - 1] === "." || val[val.length - 1] === ",") {
    //            obj.setValueState("Error");
    //        } else if (evt.getSource().mProperties.selectedKey === "") {
    //            if (val > 8) {
    //                obj.setValueState("Error");
    //            };
    //        } else {
    //            obj.setValueState("None");
    //        };
    //    },
    //
    //    onOreLavoroChange: function (evt) {
    //        var noEntratoInIf = true;
    //        var obj = evt.getSource();
    //        var val = obj.getValue();
    //        if (val.split(".")[1]) {
    //            if (val.split(".")[1] !== "5" && val.split(".")[1] !== "50" && val.split(".")[1] !== "0" && val.split(".")[1] !== "00") {
    //                obj.setValueState("Error");
    //                noEntratoInIf = false;
    //            };
    //        } else if (val[val.length - 1] === ".") {
    //            obj.setValueState("Error");
    //            noEntratoInIf = false;
    //        };
    //        if (obj.getValueState() !== "Error" || noEntratoInIf === true) {
    //            if (!parseFloat(val) || parseFloat(val) < 0.5 || parseFloat(val) > 24) {
    //                obj.setValueState("Error");
    //            } else if (val > 8 && this.getView().byId("idTipoOrario").getSelectedKey() === "") {
    //                obj.setValueState("Error");
    //            } else {
    //                obj.setValueState("None");
    //            };
    //        };
    //        if (val === ".5" || val === ",5") {
    //            this.getView().byId("oreLavoro").setValue(0.5);
    //        };
    //    },
    //
    //    oraInizioChange: function (evt) {
    //        var obj = evt.getSource();
    //        var val = obj.getValue();
    //        val = parseInt(val);
    //        if (val < 0 || val > 23 || val === "e" || val === "E") {
    //            obj.setValueState("Error");
    //        } else {
    //            obj.setValueState("None");
    //        };
    //    },
    //
    //    onLuogoLavoroChange: function (evt) {
    //        var obj = evt.getSource();
    //        var val = obj.getValue();
    //        if (val !== "Cliente" || val !== "Sede" || val !== "Casa") {
    //            obj.setValueState("Error");
    //        } else {
    //            obj.setValueState("None");
    //        };
    //    },
    //
    //    onChangeDate: function (evt) {
    //        var maxTime = "09:00:00";
    //        this.time = new Date();
    //        var dataScelta = evt.getParameters().value.substring(0, 6) + evt.getParameters().dateValue.getFullYear();
    //        var attivitaGiornaliere = _.where(this.attivitaSet, {
    //            'dataCommessa': dataScelta,
    //            'delete': ""
    //        });
    //        this.totOrarioFine = [];
    //        if (attivitaGiornaliere.length > 0) {
    //            for (var i = 0; i < attivitaGiornaliere.length; i++) {
    //                if (attivitaGiornaliere[i].delete !== "X") {
    //                    var orarioProvvisorio = attivitaGiornaliere[i].orarioFine;
    //                    var oraProvvisoria = orarioProvvisorio.split(":")[0];
    //                    var minutiProvvisori = orarioProvvisorio.split(":")[1];
    //                    orarioProvvisorio = parseFloat(oraProvvisoria + "." + minutiProvvisori);
    //                    this.totOrarioFine.push(orarioProvvisorio);
    //                };
    //            };
    //        } else {
    //            this.orarioMassimo = "9.0";
    //        };
    //        var orarioMezzanotte = false;
    //        if (this.totOrarioFine.length === 0) {
    //            this.time.setHours(maxTime.split(":")[0], maxTime.split(":")[1]);
    //        } else if (this.totOrarioFine.length > 0) {
    //            this.orarioMassimo = this.totOrarioFine[0];
    //            for (var i = 0; i < this.totOrarioFine.length; i++) {
    //                if (this.totOrarioFine[i] === 00 || this.totOrarioFine[i] === 0) {
    //                    orarioMezzanotte = true;
    //                };
    //                if (this.totOrarioFine[i] > this.orarioMassimo) {
    //                    this.orarioMassimo = this.totOrarioFine[i];
    //                };
    //            };
    //        };
    //        this.orarioMassimo = this.orarioMassimo.toString();
    //        if (orarioMezzanotte === true) {
    //            this.orarioMassimo = "00:00";
    //            var oraMax = this.orarioMassimo.split(":")[0];
    //            var minutiMax = this.orarioMassimo.split(".")[1];
    //            if (!minutiMax) {
    //                minutiMax = "00";
    //            };
    //        } else {
    //            var oraMax = this.orarioMassimo.split(".")[0];
    //            var minutiMax = this.orarioMassimo.split(".")[1];
    //            if (!minutiMax) {
    //                minutiMax = "00";
    //            } else if (minutiMax === "5" || minutiMax === "50") {
    //                minutiMax = "30";
    //            };
    //            this.orarioMassimo = oraMax + ":" + minutiMax;
    //        };
    //        this.time.setHours(oraMax, minutiMax);
    //        var oraInizio = this.time.getHours();
    //        oraInizio = oraInizio.toString();
    //        if (oraInizio.length === 1) {
    //            oraInizio = "0" + oraInizio;
    //        };
    //        var minutiInizio = this.time.getMinutes();
    //        minutiInizio = minutiInizio.toString();
    //        if (minutiInizio.length === 1) {
    //            minutiInizio = minutiInizio + "0";
    //        };
    //        this.editModel.getData().oraInizio = oraInizio;
    //        this.editModel.getData().minutiInizio = minutiInizio;
    //        this.editModel.refresh();
    //    }
});
