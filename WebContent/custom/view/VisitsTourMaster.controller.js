jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.VisitsTour");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("model.filters.Filter");
jQuery.sap.require("jquery.sap.storage");
jQuery.sap.require("model.Current");


view.abstract.AbstractMasterController.extend("view.VisitsTourMaster", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
        this.uiModel.setProperty("/searchProperty", ["description", "id"]);

        this.userModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.userModel, "userModel");

        this.sellingPointModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.sellingPointModel, "sellingPoints");
    },


    handleRouteMatched: function (evt) {
        var name = evt.getParameter("name");

        if (name !== "visitsTourMaster" && name !== "visitsTourDetail" && name != "emptyVisitsTour") {
            return;
        }

        this.user = model.persistence.Storage.session.get("user");
        this.userModel.setData(this.user);

        this.uiModel.setProperty("/changeOrganization", false);

        var channels = [{
            description: "Retail"
        }, {
            description: "PrivateLabel"
        }, {
            description: "Horeca"
        }];
        this.dataOrgModel = new sap.ui.model.json.JSONModel();
        this.dataOrgModel.setProperty("/orgData", channels);
        this.getView().setModel(this.dataOrgModel, "dataOrg");
        this.currentChannel = model.persistence.Storage.session.get("currentChannel");
        if (!!this.currentChannel) {
            this.uiModel.setProperty("/changeOrganization", true);
            model.collections.VisitsTour.loadSellingPointsByChannel(this.currentChannel) //forceReload
                .then(_.bind(function (res) {
                    console.log(res);
                    this.sellingPointModel.setData(res);
                    this.sellingPointModel.refresh(true);

                }, this));
        } else {
            this._innerDataOrgDialogOpen();
        }
    },

    _innerDataOrgDialogOpen: function () {
        var currentCustomer = model.persistence.Storage.session.get("currentCustomer");
        if (!this.dataOrgDialog)
            this.dataOrgDialog = sap.ui.xmlfragment("view.fragment.selectChannel", this);

        var page = this.getView().byId("visitsTourMasterPage");
        page.addDependent(this.dataOrgDialog);
        this.dataOrgDialog.open();

        this.dataOrgSelectList = sap.ui.getCore().byId("dataOrgSelectListId");
        if (!this.dataOrgSelectList.getSelectedItem()) {
            this.dataOrgSelectList.setSelectedItem(this.dataOrgSelectList.getItems()[0]);
        }
    },

    onDataOrgDialogOpen: function () {
        this._innerDataOrgDialogOpen();
    },

    afterDialogChannelOpen: function () {
        var channelList = sap.ui.getCore().getElementById("dataOrgSelectListId");
        var channels = channelList.getItems();
        var hash = location.hash;
        var selectedChannel = "";
        if (hash.indexOf("detail") >= 0) {
            selectedChannel = hash.split("/")[3];

        } else if (this.selectedChannel) {
            selectedChannel = this.selectedChannel.description;
        }
        if (selectedChannel) {
            for (var i = 0; i < channels.length; i++) {
                if (channels[i].mProperties.title === selectedChannel) {
                    channelList.setSelectedItem(channels[i]);
                    break;
                }
            }
        }
    },

    onDataOrgDialogConfirm: function (evt) {
        this.router.navTo("emptyVisitsTour");
        var itemSelected = evt.getSource().getParent().getParent().getContent()[0].getSelectedItem().getBindingContext("dataOrg").getObject();
        model.persistence.Storage.session.save("currentChannel", itemSelected.description);
        this.getView().setBusy(true);
        model.collections.VisitsTour.loadSellingPointsByChannel(itemSelected.description).then(function (res) {
            this.getView().setBusy(false);
            console.log(res);
            this.sellingPointModel.setData(res);
            this.sellingPointModel.refresh(true);
            this.uiModel.setProperty("/changeOrganization", true);
            if (!!this.dataOrgDialog) {
                this.dataOrgDialog.close();
            }
            this.selectedChannel = itemSelected;
        }.bind(this), function (error) {
            this.getView().setBusy(false);
            sap.m.MessageBox.error(error, {
                title: this._getLocaleText("errorLoadingCustomerTitle")
            });
            if (!!this.dataOrgDialog) {
                this.dataOrgDialog.close();
            }
        }.bind(this));
    },

    onDataOrgDialogBeforeClose: function (evt) {
        var itemSelected = evt.getSource().getContent()[0].getSelectedItem().getBindingContext("dataOrg").getObject();
        if (!!itemSelected) {
            model.persistence.Storage.session.save("currentChannel", itemSelected.description);
            this.getView().setBusy(true);
            model.collections.VisitsTour.loadSellingPointsByChannel(itemSelected.description).then(function (res) {
                this.getView().setBusy(false);
                console.log(res);
                this.sellingPointModel.setData(res);
                this.sellingPointModel.refresh(true);
                this.uiModel.setProperty("/changeOrganization", true);
            }.bind(this), function (error) {
                this.getView().setBusy(false);
                sap.m.MessageBox.error(error, {
                    title: this._getLocaleText("errorLoadingCustomerTitle")
                });
            }.bind(this));
        }
    },

    onItemPress: function (evt) {
        var src = evt.getSource();
        var selectedItem = src.getBindingContext("sellingPoints").getObject();
        var currentChannel = model.persistence.Storage.session.get("currentChannel");
        model.persistence.Storage.session.save("currentSellingPoint", selectedItem);
        this.router.navTo("visitsTourDetail", {
            channel: currentChannel,
            sellingPoint: selectedItem.id
        });
    },

    onFilterPress: function () {
        this.filterModel = model.filters.Filter.getModel(this.hierarchyModel.getData().items, "sellingPoints");
        this.getView().setModel(this.filterModel, "filter");
        var page = this.getView().byId("productsListPageId");
        this.filterDialog = sap.ui.xmlfragment("view.dialog.filterDialog", this);
        page.addDependent(this.filterDialog);
        this.filterDialog.open();
    },

    onFilterDialogClose: function () {
        this.filterDialog.close();
    },

    onFilterPropertyPress: function (evt) {
        var parentPage = sap.ui.getCore().byId("parent");
        var elementPage = sap.ui.getCore().byId("children");
        console.log(this.getView().getModel("filter").getData().toString());
        var navCon = sap.ui.getCore().byId("navCon");
        var selectedProp = evt.getSource().getBindingContext("filter").getObject();
        this.getView().getModel("filter").setProperty("/selected", selectedProp);
        this.elementListFragment = sap.ui.xmlfragment("view.fragment.FilterList", this);
        elementPage.addContent(this.elementListFragment);
        navCon.to(elementPage, "slide");
        this.getView().getModel("filter").refresh();
    },

    onBackFilterPress: function (evt) {
        this.navConBack();
        this.getView().getModel("filter").setProperty("/selected", "");
        this.elementListFragment.destroy();
    },

    navConBack: function () {
        var navCon = sap.ui.getCore().byId("navCon");
        navCon.to(sap.ui.getCore().byId("parent"), "slide");
        this.elementListFragment.destroy();
    },

    afterOpenFilter: function (evt) {
        var navCon = sap.ui.getCore().byId("navCon");
        if (navCon.getCurrentPage().getId() == "children")
            navCon.to(sap.ui.getCore().byId("parent"), "slide");
        this.getView().getModel("filter").setProperty("/selected", "");
    },

    onSearchFilter: function (oEvt) {
        var aFilters = [];
        var sQuery = oEvt.getSource().getValue();

        if (sQuery && sQuery.length > 0) {
            aFilters.push(this.createFilter(sQuery, "value"));
        }

        // update list binding
        var list = sap.ui.getCore().byId("filterList");
        var binding = list.getBinding("items");
        binding.filter(aFilters);
    },

    createFilter: function (query, property) {
        var filter = new sap.ui.model.Filter({
            path: property,
            test: function (val) {
                var prop = val.toString().toUpperCase();
                return (prop.indexOf(query.toString().toUpperCase()) >= 0)
            }
        });
        return filter;
    },

    onFilterDialogClose: function (evt) {
        if (this.elementListFragment) {
            this.elementListFragment.destroy();
        }
        if (this.filterDialog) {
            this.filterDialog.close();
            this.filterDialog.destroy();
        }
    },

    onFilterDialogOK: function (evt) {
        var filterItems = model.filters.Filter.getSelectedItems("hierarchyItems");
        if (this.elementListFragment)
            this.elementListFragment.destroy();
        this.filterDialog.close();
        this.getView().getModel("filter").setProperty("/selected", "");
        this.handleFilterConfirm(filterItems);
        this.filterDialog.destroy();
        delete(this.filterDialog);
    },

    handleFilterConfirm: function (selectedItems) {
        var filters = [];
        _.forEach(selectedItems, _.bind(function (item) {
                filters.push(this.createFilter(item.value, item.property));
            },
            this));
        var list = this.getView().byId("list");
        var binding = list.getBinding("items");
        binding.filter(filters);
    },

    onResetFilterPress: function () {
        model.filters.Filter.resetFilter("hierarchyItems");
        var list = this.getView().byId("list");
        var binding = list.getBinding("items");
        binding.filter();
        if (this.elementListFragment) {
            this.elementListFragment.destroy();
        }
        if (this.filterDialog) {
            this.filterDialog.close();
            this.filterDialog.destroy();
        }
    },

    navToProduct: function (id) {
        this.router.navTo("productDetail", {
            id: id
        });
    }
});
