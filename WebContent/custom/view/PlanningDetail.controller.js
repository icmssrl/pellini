jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.Customers");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("model.Current");
jQuery.sap.require("model.i18n");


view.abstract.AbstractController.extend("view.PlanningDetail", {
    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.planningDetailModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.planningDetailModel, "planningDetailModel");
    },

    handleRouteMatched: function (evt) {

        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var routeName = evt.getParameter("name");

        if (routeName !== "planningDetail") {
            return;
        }
        
        var nomeEvento = JSON.parse(sessionStorage.getItem("planning")).eventoPianificato;
        this.planningDetailModel.setProperty("/nomeEvento", nomeEvento);
    }
});
