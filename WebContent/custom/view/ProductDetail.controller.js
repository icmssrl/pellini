jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.Customers");
jQuery.sap.require("model.collections.Positions");
jQuery.sap.require("model.collections.Accessories");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("model.Order");
jQuery.sap.require("model.Current");
jQuery.sap.require("model.Position");
jQuery.sap.require("model.Cart");
jQuery.sap.require("model.i18n");
jQuery.sap.require("model.Discount");

view.abstract.AbstractController.extend("view.ProductDetail", {

	onExit: function () {

	},


	onInit: function () {

		// this.router = sap.ui.core.UIComponent.getRouterFor(this);
		// this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
		view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

	},


	handleRouteMatched: function (evt) {

		view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
		var routeName = evt.getParameter("name");

		if ( routeName !== "productDetail"){
			return;
		}


		var id = evt.getParameters().arguments.id;

		this.order = model.Current.getOrder();

		//Now it's here, maybe the next code needs to be moved
		//As we are in orderCreqtion, maybe we need another property to verify the application status
		if(!this.order)
		{
			var orderData = model.persistence.Storage.session.get("currentOrder");
			this.order  = new model.Order(orderData);

		};

		this.customer = model.Current.getCustomer();

		//Now it's here, maybe the next code needs to be moved
		//As we are in orderCreqtion, maybe we need another property to verify the application status
		if(!this.customer)
		{
			var customerData = model.persistence.Storage.session.get("currentCustomer");
			this.customer  = new model.Customer(customerData);

		};

		this.reqModel = new sap.ui.model.json.JSONModel({"reqDate":new Date(), "reqQty":""});
		this.getView().setModel(this.reqModel, "req");
    //**
    this.reqModel.setProperty("/reqDate",this.order.requestedDate);
    //**
		this.resModel = new sap.ui.model.json.JSONModel({"availableDate":""});
		this.getView().setModel(this.resModel, "res");

    //**
		model.collections.Products.getById(id) //this must be producId? why not use this variable?
    .then(

			_.bind(function(result)
			{
				this.product = result;
				// this.refreshView(result, routeName)
				this.refreshView(result);
			}, this)
		);
        
        model.collections.Accessories.loadAccessoriesByProductId(id) //this must be producId? why not use this variable?
    .then(

			_.bind(function(result)
			{
				this.accessories = result;
				// this.refreshView(result, routeName)
				this.accessoriesModel = new sap.ui.model.json.JSONModel();
                this.accessoriesModel.setData({"items":result});
                this.getView().setModel(this.accessoriesModel, "accessories");
			}, this)
		);
        
        var requestAvailabilityPanel = this.getView().byId("productRequestPanelId");
        if(!!requestAvailabilityPanel && requestAvailabilityPanel.getExpanded())
			requestAvailabilityPanel.setExpanded(false);

	},
	// refreshView : function(data, route)
	// {
	// 		var enable = {"editable": false};
	// 		var page = this.getView().byId("detailPage");
	// 		if(route && route.toUpperCase().indexOf("edit".toUpperCase())>=0)
	// 		{
	// 			var clone = _.cloneDeep(data.getModel().getData());
	// 			var clonedModel = new sap.ui.model.json.JSONModel(clone);
	// 			this.getView().setModel(clonedModel, "c");
	// 			enable.editable = true;
	// 			var toolbar = sap.ui.xmlfragment("view.fragment.editToolbar", this);
	// 			page.setFooter(toolbar);
	// 			//Maybe to parameterize
	// 			this.populateSelect();
	// 			//------------------------
	// 		}
	// 		else
	// 		{
	// 			this.getView().setModel(data.getModel(), "c");
	// 			enable.editable=false;
	// 			var toolbar = sap.ui.xmlfragment("view.fragment.detailToolbar", this);
	// 			page.setFooter(toolbar);
	// 		}
	// 		var enableModel = new sap.ui.model.json.JSONModel(enable);
	// 		this.getView().setModel(enableModel, "en");
	//
	// },
	refreshView : function(data)//maybe to abstract
	{
			var enable = {"editable": false};
			var page = this.getView().byId("detailPage");
			this.getView().setModel(data.getModel(), "p");
			enable.editable=false;
			var toolbar = sap.ui.xmlfragment("view.fragment.catalogueToolbar", this);
			page.setFooter(toolbar);
			this.getView().setModel(this.customer.getModel(), "customer");
			// var enableModel = new sap.ui.model.json.JSONModel(enable);
			// this.getView().setModel(enableModel, "en");

	},

	onAddPress : function(evt)
	{
            
        var accessoriesList = this.getView().byId("accessoriesList");
        var selectedAccessories = [];
        if(accessoriesList && this.accessories.length>0 ){
            var accessoriesArr = accessoriesList.getModel("accessories").getData().items;
        }

        if(!this.cart)
        {
            this.cart = new model.Cart();

            this.cart.create(this.order.getId(), this.customer.getId());
        }

        //------------------------------------------------
        var position = new model.Position();
        var checkProductId = this.product.productId;
            if(this.order.positions){
            var positionsArr = this.order.positions;
            for(var i=0; i<positionsArr.length; i++){
                if(checkProductId===positionsArr[i].productId){
                    sap.m.MessageToast.show(model.i18n._getLocaleText("PRODUCT_ALREADY_ADDED"));
                    return;
                }
            }
        }

	    position.create(this.order, this.product);
        position.setQuantity(this.reqModel.getData().reqQty);
        position.setWantedDate(this.reqModel.getData().reqDate);
        this.order.addPosition(position);
        var discount = new model.Discount();
        discount.initialize(this.order.getId(), position);
//        .then(_.bind(function()
//        {
            position.setDiscount(discount);
            console.log(position);

//        }, this));
        
        if(accessoriesArr && accessoriesList.getSelectedItems().length>0){
            var selectedItems = accessoriesList.getSelectedItems();
            for (var a = 0; a < selectedItems.length; a++){
                var oContext = selectedItems[a].getBindingContext("accessories");
                var path = oContext.getPath();
                var currentIndex = parseInt(path.split('/')[path.split('/').length - 1]);
                selectedAccessories.push(accessoriesArr[currentIndex]);
            }
        }

	    
        if(selectedAccessories.length>0){
            for(var aa = 0; aa<selectedAccessories.length; aa++){
                var newProduct = new model.Product();
                newProduct.update(selectedAccessories[aa]);
                var newPosition = new model.Position();
                newPosition.create(this.order, newProduct);
                //newPosition.setQuantity(this.reqModel.getData().reqQty);
                newPosition.setWantedDate(this.reqModel.getData().reqDate);
                this.order.addPosition(newPosition);
                var newDiscount = new model.Discount();
                newDiscount.initialize(this.order.getId(), newPosition);
//                .then(_.bind(function()
//                {
                    newPosition.setDiscount(newDiscount);
                    console.log(newPosition);

//                }, this));
            }
            
        }
	    model.Current.setOrder(this.order);
	    this.cart.setPositions(this.order.getPositions());
	    console.log(this.order);

        

        sap.m.MessageToast.show(model.i18n._getLocaleText("PRODUCT_ADDED")+" : "+this.product.getId());






	},
	onProductRequestPress:function(evt)
	{
		var src = evt.getSource();
		var panel = src.getParent().getParent();
		if(panel.getExpanded())
			panel.setExpanded(false);
		else {
			panel.setExpanded(true);
		}

	},
    
//    onShowAccessoriesPress:function(evt)
//	{
//        var button = this.getView().byId("showAccessoriesButton");
//        var text = button.getText();
//		var src = evt.getSource();
//		var panel = src.getParent().getParent();
//		if(!!panel)
//            if(text===model.i18n._getLocaleText("SHOW_ACCESSORIES")){
//			    panel.setExpanded(true);
//                button.setText(model.i18n._getLocaleText("HIDE_ACCESSORIES"));
//            }else {
//                panel.setExpanded(false);
//                button.setText(model.i18n._getLocaleText("SHOW_ACCESSORIES"));
//            }
//
//	},

	onConfirmCheckPress:function(evt)
	{
		var reqDate = this.getView().getModel("req").getData().reqDate;
		var reqQty = this.getView().getModel("req").getData().reqQty;

		//Maybe to Abstract
		model.collections.Positions.getFutureDateByProductId(this.product.getId(), reqQty, reqDate)//forceReload
		.then(_.bind(function(res){
				console.log(res);
				var temp={};
				temp.availableDate=res.availableDate.toLocaleDateString();
				temp.available=res.success;
				this.getView().getModel("res").setData({"availableDate": temp.availableDate});
				//cModel.setProperty("/available", res.success);
				//cModel.setProperty("/availableDate", res.availableDate.toLocaleDateString());
				//cModel.updateBindings();
		}, this));


	}




	// onStartSessionPress : function()
	// {
	// 	model.persistence.Storage.session.save("customerSession", true);
	// 	model.persistence.Storage.session.save("currentCustomer", this.customer);
	// 	//Maybe is better correcting save class
	// 	model.Current.setCustomer(this.customer);
	// 	this.router.navTo("launchpad");
	// },
	// onEditPress : function()
	// {
	// 	this.router.navTo("customerEdit", {id:this.customer.getId()});
	// 	// this.refreshView(this.customer, "edit");
	// },

	// onCreatePress:function(evt)
	// {
	// 	this.router.navTo("newCustomer");
	// }
	// onSavePress :function()
	// {
	// 	var editedCustomer = this.getView().getModel("c").getData();
	// 	this.customer.update(editedCustomer);
	// 	this.router.navTo("customerDetail", {id:this.customer.getId()});
	// 	// this.refreshView(this.customer);
	// },
	// onResetPress : function()
	// {
	// 	this.getView().getModel("c").setData(_.cloneDeep(this.customer.getModel().getData()));
	// },

	// populateSelect :  function()
	// {
	// 	//HP1 : Maybe it's possible creating a mapping file on which every collection has its params to select
	// 	//HP2 : Every collections contains an array of params where a select is needed
	// 	var pToSelArr = [
	// 		{"type":"registryTypes", "namespace":"rt"},
	// 		{"type":"billTypes", "namespace":"bt"},
	// 		{"type":"contactTypes", "namespace":"ct"},
	// 		{"type":"paymentConditions", "namespace": "pc"},
	// 		{"type": "places", "namespace":"p"}
	// 		];
	//
	// 	_.map(pToSelArr, _.bind(function(item)
	// {
	// 	utils.Collections.getModel(item.type)
	// 	.then(_.bind
	// 		(function(result)
	// 	{
	// 		this.getView().setModel(result, item.namespace);
	//
	// 	}, this))
	// }, this));
	//
	//
	// }


});
