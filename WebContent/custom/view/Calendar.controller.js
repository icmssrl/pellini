jQuery.sap.require("sap.m.MessageBox");
//jQuery.sap.require("sap.m.MessagePopover");
//jQuery.sap.require("sap.m.MessagePopoverItem");
//jQuery.sap.require("jquery.sap.storage");
jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.User");
//jQuery.sap.require("utils.LocalStorage");
//jQuery.sap.require("utils.SessionStorage");
jQuery.sap.require("utils.Util");
jQuery.sap.require("utils.DateFormat");
//jQuery.sap.require("model.attivitaOdata");
//jQuery.sap.require("model.noteSpeseOdata");
//jQuery.sap.require("model.perioOdata");
//jQuery.sap.require("utils.calcoloPasqua");

view.abstract.AbstractController.extend("view.Calendar", {


    /* UTILI
    var cal = this._oListFragment.getAggregation("content")[0]; // 04/02/2016
var d = new Date(parseInt(annoCalendarioHome), intMeseCalendarioHome, 0);
cal._oFocusedDate._oInnerDate = d;
            this._oListFragment.rerender();
    
    */

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.tableModel = new sap.ui.model.json.JSONModel();
        var table = {
            dati: []
        };
        this.tableModel.setData(table);
        this.getView().setModel(this.tableModel, "tableModel");

        this.userModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.userModel, "userModel");

        this.datiModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.datiModel, "datiModel");

        this.enableModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.enableModel, "enableModel");

        var arrayColori = ["#3366FF", "#FFCC33", "#CC33CC", "#FFFF33", "#33FF33", "#FF99FF", "#FF6600", "#009966", "#336699", "#99FFCC", "#FF0066", "#C0C0C0"];

        this.colorModel = new sap.ui.model.json.JSONModel();
        this.colorModel.setData(arrayColori);
        this.getView().setModel(this.colorModel, "color");
    },

    handleRouteMatched: function (evt) {
        var name = evt.getParameter("name");

        if (name !== "calendar") {
            return;
        };

        this.user = model.persistence.Storage.session.get("user");
        this.username = this.user.username;
        this.userModel.setData(this.user);
        this.appointments = JSON.parse(localStorage.getItem(this.username + "-appointments"));
        this.datiModel.setProperty("/dati", this.appointments);
        if (this.calendarioCaricato) {
            this.updateTable();
            this._setDayFull();
        };
        this.enableModel.setProperty("/deleteEnable", false);

        var appModel = this.getView().getModel("appStatus");
        appModel.setProperty("/navBackVisible", true);
    },

    handleCalendarSelect: function (oEvent) {
        var oCalendar = oEvent.oSource;
        var giornoSelezionato = utils.Util.dateToString(new Date(oCalendar.getSelectedDates()[0].getStartDate()));
        this.updateTable(giornoSelezionato);
    },

    onPlanningPress: function () {
        this.router.navTo("emptyPlanningDetail");
    },

    onAddPress: function (evt) {
        var c = this.getView().byId("calendar");
        var d = c.getSelectedDates()[0];
        if (d) {
            var data = new Date(d.getStartDate());
            data = utils.DateFormat.formatDate(data);
            data = data.split("/").join("");
        } else {
            var data = new Date();
            data = utils.DateFormat.formatDate(data);
            data = data.split("/").join("");
        };

        this.router.navTo("appointments", {
            state: "new",
            data: data
        });
    },

    _setDayFull: function (idCalendar, activityToRelease) {
        /*funzione che aggiunge righe sul giorno del mese in cui sono presenti attività*/
        if (!idCalendar) {
            var oCal1 = sap.ui.getCore().getElementById("calendarId--calendar");
            var annoAttuale = document.getElementById("calendarId--calendar--Head-B2").textContent;
            var meseAttuale = document.getElementById("calendarId--calendar--Head-B1").textContent;
        } else {
            var oCal1 = sap.ui.getCore().getElementById(idCalendar);
            var annoAttuale = document.getElementById(idCalendar + "--Head-B2").textContent;
            var meseAttuale = document.getElementById(idCalendar + "--Head-B1").textContent;
        };
        var dataX = "inDate";
        var oRefDate = new Date();

        meseAttuale = utils.Util._getMeseAttuale(meseAttuale, annoAttuale).mese;

        var firstDay = new Date(utils.Util.getFirstDayFromMonth(parseInt(annoAttuale), parseInt(meseAttuale) - 1)).getDate();
        var lastDay = new Date(utils.Util.getLastDayFromMonth(parseInt(annoAttuale), parseInt(meseAttuale))).getDate();
        for (var i = firstDay; i < lastDay + 1; i++) {
            if (i < 10) {
                i = "0" + i;
            } else {
                i = i.toString();
            };
            if (parseInt(meseAttuale) < 10) {
                meseAttuale = "0" + parseInt(meseAttuale);
            } else {
                meseAttuale = meseAttuale.toString();
            };
            var data = annoAttuale + "" + meseAttuale + "" + i;
            if (!idCalendar) {
                document.getElementById("calendarId--calendar--Month0-" + data).style.boxShadow = "none";
            } else {
                document.getElementById(idCalendar + "--Month0-" + data).style.boxShadow = "none";
            };
        };

        if (this.datiModel.getData().dati) {

            if (activityToRelease) {
                var anno = _.groupBy(activityToRelease, function (n) {
                    return n.dataEvento.split("/")[2];
                });
            } else {
                var anno = _.groupBy(this.datiModel.getData().dati, function (n) {
                    return n.dataEvento.split("/")[2];
                });
            };

            for (var d in anno) {
                if (annoAttuale === d) {
                    var ciccio = _.remove(anno[d], {
                        'delete': "X"
                    });
                    var annoOK = anno[d];
                    var mese = _.groupBy(annoOK, function (n) {
                        return n.dataEvento.split("/")[1];
                    });
                    break;
                } else {
                    var mese = undefined;
                };
            };

            if (mese) {
                for (var j in mese) {
                    if (meseAttuale === j) {
                        var meseOK = mese[j];
                        meseOK = _.sortBy(meseOK, 'dataEvento');
                    };
                };
            };
            if (meseOK) {
                var activityByDate = _.groupBy(meseOK, function (n) {
                    return n.dataEvento.split("/")[0];
                });
                for (var l in activityByDate) {
                    var f = activityByDate[l][0].dataEvento.split("/");
                    var datamia = f[2] + "" + f[1] + "" + f[0];
                    if (!idCalendar) {
                        var daySelected = document.getElementById("calendarId--calendar--Month0-" + datamia);
                    } else {
                        var daySelected = document.getElementById(idCalendar + "--Month0-" + datamia);
                    };

                    var spessore = 4;

                    var orderTemp = _.sortBy(activityByDate[l], function (m) {
                        var orarioInizio = parseFloat(m.oraEventoInizio.split(":")[0] + "." + m.oraEventoInizio.split(":")[1]);
                        return orarioInizio;
                    });
                    //inverto l'ordine dell'array cosicché le linee compaiano in maniera corretta
                    orderTemp.reverse();
                    var arrayStringa = [];
                    var stringa = "";
                    var supplemento = [];
                    for (var y = orderTemp.length; y > 0; y--) {
                        if (orderTemp[y - 1].colore === undefined) {
                            orderTemp[y - 1].colore = "#000000";
                        };
                        if (orderTemp.length > 1) {
                            if (y !== orderTemp.length) {
                                arrayStringa.push("inset " + (spessore * (orderTemp.length - y) + 2 + (supplemento.length * 2)) + "px 0rem 0 #FFFFFF")
                                supplemento.push(y);
                            };
                        };
                        arrayStringa.push("inset " + (spessore * (orderTemp.length - y) + spessore + (supplemento.length * 2)) + "px 0rem 0 " + orderTemp[y - 1].colore)
                    };
                    for (var i = 0; i < arrayStringa.length; i++) {
                        stringa = stringa + ", " + arrayStringa[i];
                    };
                    daySelected.style.boxShadow = stringa.substr(2, stringa.length);
                };
            };
        };
    },

    onDeletePress: function () {
        sap.m.MessageBox.show(this._getLocaleText("confirmDelete"),
            sap.m.MessageBox.Icon.NONE,
            this._getLocaleText("confirmDeleteTitle"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this.deleteAppointment, this)
        );
    },

    deleteAppointment: function (evt) {
        var appointmentsTable = this.getView().byId("idTable");
        var selectedIndexAppointments = appointmentsTable.getSelectedContextPaths();
        for (var i = 0; i < selectedIndexAppointments.length; i++) {
            var index = parseInt(selectedIndexAppointments[i].split("/")[2]);
            var appointmentToDelete = this.tableModel.getData().dati[index];
            _.remove(this.appointments, {
                'counterFiori': appointmentToDelete.counterFiori
            });
            var data = appointmentToDelete.dataEvento;
        };
        model.persistence.Storage.local.save(this.username + "-appointments", this.appointments);
        //        localStorage.setItem(this.username + "-appointments", this.appointments);
        this.updateTable(data);
        this._setDayFull();
        appointmentsTable.removeSelections();
    },





    onAfterRendering: function () {
        $("#calendarId--calendar--Head-B1").on("click", function () {
            preventDefault();
        });
        $("#calendarId--calendar--Head-B2").on("click", function () {
            preventDefault();
        });
        $("#calendarId--calendar--Head-B1").attr("disabled", "disabled");
        $("#calendarId--calendar--Head-B2").attr("disabled", "disabled");
        this._setDayFull();
        this.calendarioCaricato = true;






        //        if (sessionStorage.getItem("noRefresh") === "true") {
        //            this._setDayFull();
        //        };
        //
        //        var meseAtt = document.getElementById("calendarId--calendar--Head-B1").textContent;
        //        var annoAttuale = document.getElementById("calendarId--calendar--Head-B2").textContent;
        //        var meseAttuale = parseInt(utils.Util._getMeseAttuale(meseAtt, annoAttuale).mese);
        //        var meseReale = (new Date().getMonth()) + 1;
        //        var mese = meseAttuale;
        //        if (mese < 10) {
        //            mese = "0" + mese;
        //        } else {
        //            mese = mese.toString();
        //        };
        //        var dataInizio = utils.Util.getFirstDayFromMonth(parseInt(annoAttuale), parseInt(mese) - 1);
        //        var dataFine = utils.Util.getLastDayFromMonth(parseInt(annoAttuale), parseInt(mese));
        //        if ((new Date().getMonth() + 1) !== meseAttuale) {
        //            this._odataCallAttivitaSet(dataInizio, dataFine);
        //        };
        //        if (location.hostname === "war.icms.it" || location.pathname === "/fiori/ftrqlt/") {
        //            if (meseAttuale === 1 && annoAttuale === "2016") {
        //                $("#calendarId--calendar--Head-prev").addClass("visible");
        //            };
        //        };
        //        var test = function (evt) {
        //
        //            if (document.getElementById("calendarId--calendar--Head-B1")) {
        //                var meseAtt = document.getElementById("calendarId--calendar--Head-B1").textContent;
        //                var annoAttuale = document.getElementById("calendarId--calendar--Head-B2").textContent;
        //                var meseAttuale = parseInt(utils.Util._getMeseAttuale(meseAtt, annoAttuale).mese);
        //                var meseReale = (new Date().getMonth()) + 1;
        //                var buttonPress = (evt.currentTarget.id).split("-");
        //                buttonPress = buttonPress[buttonPress.length - 1];
        //                if (buttonPress === "prev") {
        //                    if (location.hostname === "war.icms.it" || location.pathname === "/fiori/ftrqlt/" || location.pathname === "/fiori/ftrQlt/") {
        //                        if (meseAttuale === 2 && annoAttuale === "2016") {
        //                            $("#calendarId--calendar--Head-prev").addClass("visible");
        //                        };
        //                    };
        //                    meseAttuale = meseAttuale - 1;
        //                    if (meseAttuale === 0) {
        //                        meseAttuale = 12;
        //                        annoAttuale = (parseInt(annoAttuale) - 1).toString();
        //                    };
        //                    if (meseAttuale)
        //                        var mese = meseAttuale;
        //                    if (mese < 10) {
        //                        mese = "0" + mese;
        //                    } else {
        //                        mese = mese.toString();
        //                    };
        //                    var primoGiorno = "01/" + mese + "/" + annoAttuale;
        //                    this.enableModel.setProperty("/giornoSelezionato", primoGiorno);
        //                    this.calcolaOreLavorate();
        //                    this.updateTable(primoGiorno);
        //                    var dataInizio = utils.Util.getFirstDayFromMonth(parseInt(annoAttuale), parseInt(mese) - 1);
        //                    var dataFine = utils.Util.getLastDayFromMonth(parseInt(annoAttuale), parseInt(mese));
        //                    if ((new Date().getMonth() + 1) !== meseAttuale || (new Date().getFullYear().toString() !== annoAttuale)) {
        //                        this._odataCallAttivitaSet(dataInizio, dataFine, "prev");
        //                    } else {
        //                        this.updateTable();
        //                        this._setDayFull();
        //                        this._odataCallPerioSet(dataInizio, dataFine, buttonPress);
        //                    };
        //                    this.deleteEnableMulti = false;
        //                    this.enableModel.setProperty("/releaseEnable", false);
        //                } else if (buttonPress === "next") {
        //                    if (location.hostname === "war.icms.it" || location.pathname === "/fiori/ftrqlt/" || location.pathname === "/fiori/ftrQlt/") {
        //                        $("#calendarId--calendar--Head-prev").removeClass("visible");
        //                    };
        //                    meseAttuale = meseAttuale + 1;
        //
        //                    if (meseAttuale === 13) {
        //                        meseAttuale = 1;
        //                        annoAttuale = (parseInt(annoAttuale) + 1).toString();
        //                    };
        //                    var meseReale = new Date().getMonth() + 1;
        //                    var annoReale = new Date().getFullYear();
        //                    if (meseReale !== meseAttuale || annoReale !== parseInt(annoAttuale)) {
        //                        this.enableModel.setProperty("/syncEnable", false);
        //                        this.enableModel.setProperty("/refreshEnable", false);
        //                        this.enableModel.refresh();
        //                    } else {
        //                        this.enableModel.setProperty("/refreshEnable", true);
        //                    };
        //                    var mese = meseAttuale;
        //                    if (mese < 10) {
        //                        mese = "0" + mese;
        //                    } else {
        //                        mese = mese.toString();
        //                    };
        //                    var primoGiorno = "01/" + mese + "/" + annoAttuale;
        //                    this.enableModel.setProperty("/giornoSelezionato", primoGiorno);
        //                    this.calcolaOreLavorate();
        //                    this.updateTable(primoGiorno);
        //                    var dataInizio = utils.Util.getFirstDayFromMonth(parseInt(annoAttuale), parseInt(mese) - 1);
        //                    var dataFine = utils.Util.getLastDayFromMonth(parseInt(annoAttuale), parseInt(mese));
        //                    if ((new Date().getMonth() + 1) !== meseAttuale || (new Date().getFullYear().toString() !== annoAttuale)) {
        //                        this._odataCallAttivitaSet(dataInizio, dataFine, "next");
        //                    } else {
        //                        this.updateTable();
        //                        this._setDayFull();
        //                        this._odataCallPerioSet(dataInizio, dataFine, buttonPress);
        //                    };
        //                    this.datiModel.setProperty("/dati", utils.LocalStorage.getItem("attivitaSet"));
        //                    this.enableModel.setProperty("/releaseEnable", false);
        //                };
        //                if (meseAttuale === meseReale) {
        //                    this.deleteEnableMulti = true;
        //                    var attivitaLS = utils.LocalStorage.getItem("attivitaSet");
        //                    this.datiModel.setProperty("/dati", attivitaLS);
        //                    var giornoOggi = new Date();
        //                    giornoOggi = utils.Util.dateToString(giornoOggi);
        //                    this.enableModel.setProperty("/giornoSelezionato", giornoOggi);
        //                    this.calcolaOreLavorate();
        //                    this.enableModel.setProperty("/deleteEnableMulti", true)
        //                    this.updateTable();
        //                    this._findActivityToRelease();
        //                } else {
        //                    $("#calendarId--calendar--Head-next").removeClass("visible");
        //                };
        //            };
        //            this.enableModel.setProperty("/duplicateEnable", false);
        //            this.enableModel.setProperty("/deleteEnable", false);
        //            this.enableModel.setProperty("/unReleaseEnable", false);
        //            this.enableModel.setProperty("/setAddButtonVisible", false);
        //            this.enableModel.setProperty("/releaseSingleEnable", false);
        //
        //            var LS = utils.LocalStorage.getItem("attivitaSet");
        //            var trovatoAlmenoUno = false;
        //            if (LS.length !== 0) {
        //                var contoStato = 0;
        //                var contoDelete = 0;
        //                for (var g = 0; g < LS.length; g++) {
        //                    if (LS[g].stato === "10") {
        //                        contoStato = contoStato + 1;
        //                    }
        //                    if (LS[g].delete === "X") {
        //                        contoDelete = contoDelete + 1;
        //                    };
        //                };
        //                if (contoStato > contoDelete) {
        //                    trovatoAlmenoUno = true;
        //                } else {
        //                    trovatoAlmenoUno = false;
        //                };
        //            };
        //            var meseAttOggi = document.getElementById("calendarId--calendar--Head-B1").textContent;
        //            var annoAttualeOggi = document.getElementById("calendarId--calendar--Head-B2").textContent;
        //            meseAttualeOggi = parseInt(utils.Util._getMeseAttuale(meseAttOggi, annoAttualeOggi).mese);
        //            var dataOggi = new Date();
        //            var meseOggi = dataOggi.getMonth() + 1;
        //            if (trovatoAlmenoUno === false || LS.length === 0 || Math.abs(meseOggi - parseInt(meseAttualeOggi)) === 0 || (Math.abs(meseOggi - parseInt(meseAttualeOggi)) > 1 && Math.abs(meseOggi - parseInt(meseAttualeOggi)) !== 11) || (meseOggi - parseInt(meseAttualeOggi) > 0 && buttonPress === "prev") || (meseOggi - parseInt(meseAttualeOggi) < 0 && buttonPress === "next")) {
        //                if (meseOggi - parseInt(meseAttualeOggi) === 11) {
        //                    this.enableModel.setProperty("/deleteEnableMulti", true);
        //                } else {
        //                    this.enableModel.setProperty("/deleteEnableMulti", false);
        //                    this.deleteEnableMulti = false;
        //                }
        //            } else {
        //                this.enableModel.setProperty("/deleteEnableMulti", true);
        //            };
        //            setTimeout(_.bind(this._setDayFull, this), 300);
        //        };
        //        test = _.bind(test, this);
        //
        //        $("#calendarId--calendar--Head-next").on("click", test);
        //        $("#calendarId--calendar--Head-prev").on("click", test);
        //        $("#calendarId--calendar--Head-B1").on("click", function () {
        //            preventDefault();
        //        });
        //        $("#calendarId--calendar--Head-B2").on("click", function () {
        //            preventDefault();
        //        });
        //        $("#calendarId--calendar--Head-B1").attr("disabled", "disabled");
        //        $("#calendarId--calendar--Head-B2").attr("disabled", "disabled");
        //
        //        var meseAttuale = $("#calendarId--calendar--Head-B1").text();
        //        var annoAttuale = $("#calendarId--calendar--Head-B2").text();
        //        var result = utils.Util._getMeseAttuale(meseAttuale, annoAttuale);
        //        var mese = parseInt(result.mese);
        //        var annoSucc = annoAttuale;
        //        var annoPrec = annoAttuale;
        //        var meseSucc = mese + 1;
        //        var mesePrec = mese - 1;
        //        if (meseSucc < 10) {
        //            meseSucc = "0" + meseSucc;
        //        } else {
        //            meseSucc = meseSucc.toString();
        //        };
        //        if (mesePrec < 10) {
        //            mesePrec = "0" + mesePrec;
        //        } else {
        //            mesePrec = mesePrec.toString();
        //        };
        //        if (mese === 12) {
        //            annoAttuale = (parseInt(annoAttuale) + 1).toString();
        //            meseSucc = "01";
        //        };
        //        if (mese === 01) {
        //            annoAttuale = (parseInt(annoAttuale) + 1).toString();
        //            mesePrec = "12";
        //        };
        //        for (var i = 1; i <= 6; i++) {
        //            var giorno = i;
        //            var giornoMeseVecchio = parseInt(annoAttuale) + "" + meseSucc + "0" + giorno;
        //            $("#calendarId--calendar--Month0-" + giornoMeseVecchio).addClass("visible");
        //        };
        //        for (var i = 23; i <= 31; i++) {
        //            var giorno = i;
        //            var giornoMeseVecchio = parseInt(annoAttuale) + "" + mesePrec + giorno;
        //            $("#calendarId--calendar--Month0-" + giornoMeseVecchio).addClass("visible");
        //        };
        //
        //        this.firstTime = false;
        //        this.passatoInOnAfterRendering = true;
        //        this.enableModel.refresh();
    },




    //    onAfterRendering: function () {
    //        if (sessionStorage.getItem("noRefresh") === "true") {
    //            this._setDayFull();
    //        };
    //
    //        var meseAtt = document.getElementById("calendarId--calendar--Head-B1").textContent;
    //        var annoAttuale = document.getElementById("calendarId--calendar--Head-B2").textContent;
    //        var meseAttuale = parseInt(utils.Util._getMeseAttuale(meseAtt, annoAttuale).mese);
    //        var meseReale = (new Date().getMonth()) + 1;
    //        var mese = meseAttuale;
    //        if (mese < 10) {
    //            mese = "0" + mese;
    //        } else {
    //            mese = mese.toString();
    //        };
    //        var dataInizio = utils.Util.getFirstDayFromMonth(parseInt(annoAttuale), parseInt(mese) - 1);
    //        var dataFine = utils.Util.getLastDayFromMonth(parseInt(annoAttuale), parseInt(mese));
    //        if ((new Date().getMonth() + 1) !== meseAttuale) {
    //            this._odataCallAttivitaSet(dataInizio, dataFine);
    //        };
    //        if (location.hostname === "war.icms.it" || location.pathname === "/fiori/ftrqlt/") {
    //            if (meseAttuale === 1 && annoAttuale === "2016") {
    //                $("#calendarId--calendar--Head-prev").addClass("visible");
    //            };
    //        };
    //        var test = function (evt) {
    //
    //            if (document.getElementById("calendarId--calendar--Head-B1")) {
    //                var meseAtt = document.getElementById("calendarId--calendar--Head-B1").textContent;
    //                var annoAttuale = document.getElementById("calendarId--calendar--Head-B2").textContent;
    //                var meseAttuale = parseInt(utils.Util._getMeseAttuale(meseAtt, annoAttuale).mese);
    //                var meseReale = (new Date().getMonth()) + 1;
    //                var buttonPress = (evt.currentTarget.id).split("-");
    //                buttonPress = buttonPress[buttonPress.length - 1];
    //                if (buttonPress === "prev") {
    //                    if (location.hostname === "war.icms.it" || location.pathname === "/fiori/ftrqlt/" || location.pathname === "/fiori/ftrQlt/") {
    //                        if (meseAttuale === 2 && annoAttuale === "2016") {
    //                            $("#calendarId--calendar--Head-prev").addClass("visible");
    //                        };
    //                    };
    //                    meseAttuale = meseAttuale - 1;
    //                    if (meseAttuale === 0) {
    //                        meseAttuale = 12;
    //                        annoAttuale = (parseInt(annoAttuale) - 1).toString();
    //                    };
    //                    if (meseAttuale)
    //                        var mese = meseAttuale;
    //                    if (mese < 10) {
    //                        mese = "0" + mese;
    //                    } else {
    //                        mese = mese.toString();
    //                    };
    //                    var primoGiorno = "01/" + mese + "/" + annoAttuale;
    //                    this.enableModel.setProperty("/giornoSelezionato", primoGiorno);
    //                    this.calcolaOreLavorate();
    //                    this.updateTable(primoGiorno);
    //                    var dataInizio = utils.Util.getFirstDayFromMonth(parseInt(annoAttuale), parseInt(mese) - 1);
    //                    var dataFine = utils.Util.getLastDayFromMonth(parseInt(annoAttuale), parseInt(mese));
    //                    if ((new Date().getMonth() + 1) !== meseAttuale || (new Date().getFullYear().toString() !== annoAttuale)) {
    //                        this._odataCallAttivitaSet(dataInizio, dataFine, "prev");
    //                    } else {
    //                        this.updateTable();
    //                        this._setDayFull();
    //                        this._odataCallPerioSet(dataInizio, dataFine, buttonPress);
    //                    };
    //                    this.deleteEnableMulti = false;
    //                    this.enableModel.setProperty("/releaseEnable", false);
    //                } else if (buttonPress === "next") {
    //                    if (location.hostname === "war.icms.it" || location.pathname === "/fiori/ftrqlt/" || location.pathname === "/fiori/ftrQlt/") {
    //                        $("#calendarId--calendar--Head-prev").removeClass("visible");
    //                    };
    //                    meseAttuale = meseAttuale + 1;
    //
    //                    if (meseAttuale === 13) {
    //                        meseAttuale = 1;
    //                        annoAttuale = (parseInt(annoAttuale) + 1).toString();
    //                    };
    //                    var meseReale = new Date().getMonth() + 1;
    //                    var annoReale = new Date().getFullYear();
    //                    if (meseReale !== meseAttuale || annoReale !== parseInt(annoAttuale)) {
    //                        this.enableModel.setProperty("/syncEnable", false);
    //                        this.enableModel.setProperty("/refreshEnable", false);
    //                        this.enableModel.refresh();
    //                    } else {
    //                        this.enableModel.setProperty("/refreshEnable", true);
    //                    };
    //                    var mese = meseAttuale;
    //                    if (mese < 10) {
    //                        mese = "0" + mese;
    //                    } else {
    //                        mese = mese.toString();
    //                    };
    //                    var primoGiorno = "01/" + mese + "/" + annoAttuale;
    //                    this.enableModel.setProperty("/giornoSelezionato", primoGiorno);
    //                    this.calcolaOreLavorate();
    //                    this.updateTable(primoGiorno);
    //                    var dataInizio = utils.Util.getFirstDayFromMonth(parseInt(annoAttuale), parseInt(mese) - 1);
    //                    var dataFine = utils.Util.getLastDayFromMonth(parseInt(annoAttuale), parseInt(mese));
    //                    if ((new Date().getMonth() + 1) !== meseAttuale || (new Date().getFullYear().toString() !== annoAttuale)) {
    //                        this._odataCallAttivitaSet(dataInizio, dataFine, "next");
    //                    } else {
    //                        this.updateTable();
    //                        this._setDayFull();
    //                        this._odataCallPerioSet(dataInizio, dataFine, buttonPress);
    //                    };
    //                    this.datiModel.setProperty("/dati", utils.LocalStorage.getItem("attivitaSet"));
    //                    this.enableModel.setProperty("/releaseEnable", false);
    //                };
    //                if (meseAttuale === meseReale) {
    //                    this.deleteEnableMulti = true;
    //                    var attivitaLS = utils.LocalStorage.getItem("attivitaSet");
    //                    this.datiModel.setProperty("/dati", attivitaLS);
    //                    var giornoOggi = new Date();
    //                    giornoOggi = utils.Util.dateToString(giornoOggi);
    //                    this.enableModel.setProperty("/giornoSelezionato", giornoOggi);
    //                    this.calcolaOreLavorate();
    //                    this.enableModel.setProperty("/deleteEnableMulti", true)
    //                    this.updateTable();
    //                    this._findActivityToRelease();
    //                } else {
    //                    $("#calendarId--calendar--Head-next").removeClass("visible");
    //                };
    //            };
    //            this.enableModel.setProperty("/duplicateEnable", false);
    //            this.enableModel.setProperty("/deleteEnable", false);
    //            this.enableModel.setProperty("/unReleaseEnable", false);
    //            this.enableModel.setProperty("/setAddButtonVisible", false);
    //            this.enableModel.setProperty("/releaseSingleEnable", false);
    //
    //            var LS = utils.LocalStorage.getItem("attivitaSet");
    //            var trovatoAlmenoUno = false;
    //            if (LS.length !== 0) {
    //                var contoStato = 0;
    //                var contoDelete = 0;
    //                for (var g = 0; g < LS.length; g++) {
    //                    if (LS[g].stato === "10") {
    //                        contoStato = contoStato + 1;
    //                    }
    //                    if (LS[g].delete === "X") {
    //                        contoDelete = contoDelete + 1;
    //                    };
    //                };
    //                if (contoStato > contoDelete) {
    //                    trovatoAlmenoUno = true;
    //                } else {
    //                    trovatoAlmenoUno = false;
    //                };
    //            };
    //            var meseAttOggi = document.getElementById("calendarId--calendar--Head-B1").textContent;
    //            var annoAttualeOggi = document.getElementById("calendarId--calendar--Head-B2").textContent;
    //            meseAttualeOggi = parseInt(utils.Util._getMeseAttuale(meseAttOggi, annoAttualeOggi).mese);
    //            var dataOggi = new Date();
    //            var meseOggi = dataOggi.getMonth() + 1;
    //            if (trovatoAlmenoUno === false || LS.length === 0 || Math.abs(meseOggi - parseInt(meseAttualeOggi)) === 0 || (Math.abs(meseOggi - parseInt(meseAttualeOggi)) > 1 && Math.abs(meseOggi - parseInt(meseAttualeOggi)) !== 11) || (meseOggi - parseInt(meseAttualeOggi) > 0 && buttonPress === "prev") || (meseOggi - parseInt(meseAttualeOggi) < 0 && buttonPress === "next")) {
    //                if (meseOggi - parseInt(meseAttualeOggi) === 11) {
    //                    this.enableModel.setProperty("/deleteEnableMulti", true);
    //                } else {
    //                    this.enableModel.setProperty("/deleteEnableMulti", false);
    //                    this.deleteEnableMulti = false;
    //                }
    //            } else {
    //                this.enableModel.setProperty("/deleteEnableMulti", true);
    //            };
    //            setTimeout(_.bind(this._setDayFull, this), 300);
    //        };
    //        test = _.bind(test, this);
    //
    //        $("#calendarId--calendar--Head-next").on("click", test);
    //        $("#calendarId--calendar--Head-prev").on("click", test);
    //        $("#calendarId--calendar--Head-B1").on("click", function () {
    //            preventDefault();
    //        });
    //        $("#calendarId--calendar--Head-B2").on("click", function () {
    //            preventDefault();
    //        });
    //        $("#calendarId--calendar--Head-B1").attr("disabled", "disabled");
    //        $("#calendarId--calendar--Head-B2").attr("disabled", "disabled");
    //
    //        var meseAttuale = $("#calendarId--calendar--Head-B1").text();
    //        var annoAttuale = $("#calendarId--calendar--Head-B2").text();
    //        var result = utils.Util._getMeseAttuale(meseAttuale, annoAttuale);
    //        var mese = parseInt(result.mese);
    //        var annoSucc = annoAttuale;
    //        var annoPrec = annoAttuale;
    //        var meseSucc = mese + 1;
    //        var mesePrec = mese - 1;
    //        if (meseSucc < 10) {
    //            meseSucc = "0" + meseSucc;
    //        } else {
    //            meseSucc = meseSucc.toString();
    //        };
    //        if (mesePrec < 10) {
    //            mesePrec = "0" + mesePrec;
    //        } else {
    //            mesePrec = mesePrec.toString();
    //        };
    //        if (mese === 12) {
    //            annoAttuale = (parseInt(annoAttuale) + 1).toString();
    //            meseSucc = "01";
    //        };
    //        if (mese === 01) {
    //            annoAttuale = (parseInt(annoAttuale) + 1).toString();
    //            mesePrec = "12";
    //        };
    //        for (var i = 1; i <= 6; i++) {
    //            var giorno = i;
    //            var giornoMeseVecchio = parseInt(annoAttuale) + "" + meseSucc + "0" + giorno;
    //            $("#calendarId--calendar--Month0-" + giornoMeseVecchio).addClass("visible");
    //        };
    //        for (var i = 23; i <= 31; i++) {
    //            var giorno = i;
    //            var giornoMeseVecchio = parseInt(annoAttuale) + "" + mesePrec + giorno;
    //            $("#calendarId--calendar--Month0-" + giornoMeseVecchio).addClass("visible");
    //        };
    //
    //        this.firstTime = false;
    //        this.passatoInOnAfterRendering = true;
    //        this.enableModel.refresh();
    //    },

    //    handleRouteMatched: function (evt) {
    ////        if (!sessionStorage.isLogged || sessionStorage.isLogged === "false") {
    ////            this.router.navTo("");
    ////        } else {
    ////            this.commessaPerNotaSpesa = undefined;
    ////            if (sessionStorage.getItem("cid")) {
    ////                sessionStorage.setItem("noRefresh", true);
    ////                var cid = sessionStorage.cid;
    ////                var user = cid.substring(3);
    ////                var userInfo = utils.LocalStorage.getItem("cidSet");
    ////                this.nomeCognomeModel.setProperty("/nomeCognome", userInfo[0].nome + " " + userInfo[0].cognome);
    ////            };
    ////
    ////            this.cid = sessionStorage.cid;
    ////            var that = this;
    ////            this.enableModel.setProperty("/setAddButtonVisible", false);
    ////            this.enableModel.refresh();
    //        var name = evt.getParameter("name");
    //
    //        if (name !== "calendar") {
    //            return;
    //        };
    //        
    //        this.user = model.persistence.Storage.session.get("user");
    //        this.userModel.setData(this.user);
    //        
    //        
    ////
    ////            if (document.getElementById("calendarId--calendar--Head-B1") && !this.getView().getModel("appModel").getProperty("/arrivatoDaAltrePagine") === true) {
    ////                var meseAtt = document.getElementById("calendarId--calendar--Head-B1").textContent;
    ////                var annoAttuale = document.getElementById("calendarId--calendar--Head-B2").textContent;
    ////                var meseAttuale = parseInt(utils.Util._getMeseAttuale(meseAtt, annoAttuale).mese);
    ////                var meseReale = (new Date().getMonth()) + 1;
    ////                var mese = meseAttuale;
    ////                if (mese < 10) {
    ////                    mese = "0" + mese;
    ////                } else {
    ////                    mese = mese.toString();
    ////                };
    ////                var dataInizio = utils.Util.getFirstDayFromMonth(parseInt(annoAttuale), parseInt(mese) - 1);
    ////                var dataFine = utils.Util.getLastDayFromMonth(parseInt(annoAttuale), parseInt(mese));
    ////                if ((new Date().getMonth() + 1) !== meseAttuale) {
    ////                    this._odataCallAttivitaSet(dataInizio, dataFine);
    ////                };
    ////            } else if (document.getElementById("calendarId--calendar--Head-B1") && this.getView().getModel("appModel").getProperty("/arrivatoDaAltrePagine") === true) {
    ////                var meseAtt = document.getElementById("calendarId--calendar--Head-B1").textContent;
    ////                var annoAttuale = document.getElementById("calendarId--calendar--Head-B2").textContent;
    ////                var meseAttuale = parseInt(utils.Util._getMeseAttuale(meseAtt, annoAttuale).mese);
    ////                var meseReale = (new Date().getMonth()) + 1;
    ////                var mese = meseAttuale;
    ////                if (mese < 10) {
    ////                    mese = "0" + mese;
    ////                } else {
    ////                    mese = mese.toString();
    ////                };
    ////                var dataInizio = utils.Util.getFirstDayFromMonth(parseInt(annoAttuale), parseInt(mese) - 1);
    ////                var dataFine = utils.Util.getLastDayFromMonth(parseInt(annoAttuale), parseInt(mese));
    ////                if ((new Date().getMonth() + 1) !== meseAttuale) {
    ////                    this._odataCallPerioSet(dataInizio, dataFine);
    ////                };
    ////            } else {
    ////                obj = {
    ////                    "AttivitaChiusa": "",
    ////                    "NotaSpeseChiusa": ""
    ////                };
    ////                utils.LocalStorage.setItem("perioSet", obj);
    ////            };
    ////            this.nAttDaRil;
    ////            this.nAttDaSinc;
    ////            this.nNoteSpeseDaSinc;
    ////            this.sincronizzare = false;
    ////            this.refresh = false;
    ////
    ////            var oModel = new sap.ui.model.json.JSONModel(this._data);
    ////            this.getView().setModel(oModel);
    ////
    ////            this.attivitaSet = utils.LocalStorage.getItem("attivitaSet");
    ////            this.notaSpeseSet = utils.LocalStorage.getItem("notaSpeseSet");
    ////
    ////            this.datiModel.setProperty("/dati", this.attivitaSet);
    ////
    ////            var periodoAttivita = utils.LocalStorage.getItem("perioSet").AttivitaChiusa;
    ////            var periodoNoteSpese = utils.LocalStorage.getItem("perioSet").NotaSpeseChiusa;
    ////            var meseReale = (new Date().getMonth()) + 1;
    ////            var annoReale = new Date().getFullYear().toString();
    ////            if (document.getElementById("calendarId--calendar--Head-B1")) {
    ////                var meseAtt = document.getElementById("calendarId--calendar--Head-B1").textContent;
    ////                var annoAttuale = document.getElementById("calendarId--calendar--Head-B2").textContent;
    ////                var meseAttuale = parseInt(utils.Util._getMeseAttuale(meseAtt, annoAttuale).mese);
    ////            } else {
    ////                var meseAttuale = meseReale;
    ////                var annoAttuale = annoReale;
    ////            };
    ////            var mese = meseAttuale;
    ////            if (periodoAttivita && periodoAttivita === "X" && periodoNoteSpese && periodoNoteSpese === "X" && meseAttuale === meseReale && annoAttuale === annoReale) {
    ////                this.enableModel.setProperty("/syncEnable", false);
    ////                this.enableModel.setProperty("/refreshEnable", true);
    ////                this.enableModel.setProperty("/releaseEnable", true); //04/02/2016
    ////                this.enableModel.setProperty("/deleteEnableMulti", false);
    ////            } else {
    ////                if (this.getView().getModel("appModel").getProperty("/arrivatoDaAltrePagine") === true) {
    ////                    if (meseAttuale !== meseReale || annoAttuale !== annoReale) {
    ////                        this.enableModel.setProperty("/syncEnable", false);
    ////                        this.enableModel.setProperty("/refreshEnable", false);
    ////                    } else {
    ////                        this.enableModel.setProperty("/syncEnable", true);
    ////                        this.enableModel.setProperty("/refreshEnable", true);
    ////                    };
    ////                    this.enableModel.setProperty("/releaseEnable", true);
    ////                } else {
    ////                    if (this.getView().getModel("appModel").getProperty("/nascondiSincroButton") === true) {
    ////                        this.enableModel.setProperty("/syncEnable", false);
    ////                    } else {
    ////                        this.enableModel.setProperty("/syncEnable", true);
    ////                        this.enableModel.setProperty("/refreshEnable", true);
    ////                    };
    ////                    this.enableModel.setProperty("/releaseEnable", true);
    ////                };
    ////            };
    ////
    ////            var oCalendar = this.getView().byId("calendar");
    ////            if (!oCalendar.getSelectedDates()) {
    ////                var dataInit = utils.Util.dateToString(new Date());
    ////                oCalendar.getSelectedDates()[0].mProperties.startDate = dataInit;
    ////            };
    ////            this.updateTable();
    ////
    ////            if (!this.firstTime) {
    ////                this._setDayFull();
    ////                this.updateTable(this.getView().getModel("appStatus").getProperty("/data"));
    ////            };
    ////            this.enableModel.setProperty("/duplicateEnable", false);
    ////            this.enableModel.setProperty("/deleteEnable", false);
    ////            this.enableModel.setProperty("/unReleaseEnable", false);
    ////
    ////            var LS = utils.LocalStorage.getItem("attivitaSet");
    ////            var trovatoAlmenoUno = false;
    ////            if (LS.length !== 0) {
    ////                var contoStato = 0;
    ////                var contoDelete = 0;
    ////                for (var g = 0; g < LS.length; g++) {
    ////                    if (LS[g].stato === "10") {
    ////                        contoStato = contoStato + 1;
    ////                    }
    ////                    if (LS[g].delete === "X") {
    ////                        contoDelete = contoDelete + 1;
    ////                    };
    ////                };
    ////                if (contoStato > contoDelete) {
    ////                    trovatoAlmenoUno = true;
    ////                } else {
    ////                    trovatoAlmenoUno = false;
    ////                };
    ////            };
    ////            if (trovatoAlmenoUno === false || LS.length === 0) {
    ////                this.enableModel.setProperty("/deleteEnableMulti", false);
    ////            } else {
    ////                this.enableModel.setProperty("/deleteEnableMulti", true);
    ////            };
    ////            this.enableModel.refresh();
    ////            var giornoSelezionato = new Date();
    ////            if (!this.enableModel.getProperty("/giornoSelezionato")) {
    ////                this.enableModel.setProperty("/giornoSelezionato", utils.Util.dateToString(giornoSelezionato));
    ////                this.calcolaOreLavorate();
    ////            } else {
    ////                this.updateTable(this.enableModel.getProperty("/giornoSelezionato"));
    ////                this.giornoSelezionatoBack = this.enableModel.getProperty("/giornoSelezionato");
    ////                this.handleCalendarSelect();
    ////            };
    ////            var that = this;
    ////            var funzioneOnline = function () {
    ////                if (location.hostname === "war.icms.it") {
    ////                    var url = 'http://war.icms.it/fiori/verificaOffline.txt';
    ////                } else if (location.hostname === "wardev.icms.it") {
    ////                    var url = 'http://wardev.icms.it/fiori/verificaOffline.txt';
    ////                } else {
    ////                    var url = 'http://www.google.it';
    ////                };
    ////                Offline.options = {
    ////                    checks: {
    ////                        xhr: {
    ////                            url: url
    ////                        }
    ////                    }
    ////                };
    ////                Offline.check();
    ////                if (Offline.state === "up") {
    ////                    that.onlineModel.setProperty("/visibileOnline", false);
    ////                } else if (Offline.state === "down") {
    ////                    that.onlineModel.setProperty("/visibileOnline", true);
    ////                };
    ////                that.onlineModel.refresh();
    ////            };
    ////
    ////            this.mySetInterval = setInterval(funzioneOnline, 15000);
    ////
    ////            Offline.check();
    ////            if (Offline.state === "up") {
    ////                this.onlineModel.setProperty("/visibileOnline", false);
    ////            } else if (Offline.state === "down") {
    ////                this.onlineModel.setProperty("/visibileOnline", true);
    ////            };
    ////            this.onlineModel.refresh();
    ////            this.oDialog = this.getView().byId("BusyDialog");
    ////        };
    //    },

    //    handleCalendarSelect: function (oEvent) {
    //        this.selectedDate = undefined;
    //        var t = this.getView().byId("idTable");
    //        if (!t.getSelectedItem()) {
    //            this.enableModel.setProperty("/duplicateEnable", false);
    //            this.enableModel.setProperty("/deleteEnable", false);
    //            this.enableModel.setProperty("/unReleaseEnable", false);
    //            this.enableModel.setProperty("/releaseSingleEnable", false);
    //        };
    //
    //        var periodoAttivita = utils.LocalStorage.getItem("perioSet").AttivitaChiusa;
    //        if (periodoAttivita && periodoAttivita === "X") {
    //            this.enableModel.setProperty("/setAddButtonVisible", false);
    //        } else {
    //            this.enableModel.setProperty("/setAddButtonVisible", true);
    //        };
    //
    //        if (this.giornoSelezionatoBack) {
    //            var giornoSelezionato = this.giornoSelezionatoBack;
    //            var selectedDate = this.giornoSelezionatoBack;
    //            var rapportiniLocalStorage = utils.LocalStorage.getItem("attivitaSet");
    //            var oreTotaliLavorate = 0;
    //            for (var b = 0; b < rapportiniLocalStorage.length; b++) {
    //                if (rapportiniLocalStorage[b].delete !== "X" && rapportiniLocalStorage[b].dataCommessa === selectedDate) {
    //                    rapportiniLocalStorage[b].durata.replace(",", ".");
    //                    oreTotaliLavorate = oreTotaliLavorate + parseFloat(rapportiniLocalStorage[b].durata);
    //                };
    //            };
    //            if (oreTotaliLavorate.toString().split(".")[1]) {
    //                oreTotaliLavorate = oreTotaliLavorate.toFixed(2);
    //            } else {
    //                oreTotaliLavorate = oreTotaliLavorate + ".00";
    //            };
    //            this.enableModel.setProperty("/oreLavorateGiornoSelezionato", oreTotaliLavorate);
    //            this.giornoSelezionatoBack = undefined;
    //        } else {
    //            var oCalendar = oEvent.oSource;
    //            var giornoSelezionato = oCalendar.getSelectedDates()[0].getStartDate();
    //            this.enableModel.setProperty("/giornoSelezionato", utils.Util.dateToString(giornoSelezionato));
    //            this.calcolaOreLavorate();
    //            var selectedDate = utils.Util.dateToString(new Date(oCalendar.getSelectedDates()[0].getStartDate()));
    //        };
    //        this.selectedDate = selectedDate;
    //
    //        this.enableModel.refresh();
    //
    //        this.getView().getModel("appStatus").setProperty("/dataCommessa", selectedDate);
    //        this.enableModel.setProperty("/duplicateEnable", false);
    //        this.enableModel.setProperty("/deleteEnable", false);
    //        this.enableModel.setProperty("/unReleaseEnable", false);
    //        this.enableModel.setProperty("/releaseSingleEnable", false);
    //
    //
    //        var meseAtt = document.getElementById("calendarId--calendar--Head-B1").textContent;
    //        var annoAttuale = document.getElementById("calendarId--calendar--Head-B2").textContent;
    //        var meseAttuale = parseInt(utils.Util._getMeseAttuale(meseAtt, annoAttuale).mese);
    //        var meseAttualeStringa = meseAttuale.toString();
    //        if (meseAttualeStringa.length === 1) {
    //            meseAttualeStringa = "0" + meseAttualeStringa;
    //        };
    //        var dataOggi = new Date();
    //        var meseOggi = dataOggi.getMonth() + 1;
    //
    //        var LS = utils.LocalStorage.getItem("attivitaSet");
    //        var trovatoAlmenoUno = false;
    //        if (LS.length !== 0) {
    //            var contoStato = 0;
    //            var contoDelete = 0;
    //            for (var g = 0; g < LS.length; g++) {
    //                if (LS[g].stato === "10" && LS[g].dataCommessa.split("/")[1] === meseAttualeStringa && LS[g].dataCommessa.split("/")[2] === annoAttuale) {
    //                    contoStato = contoStato + 1;
    //                };
    //                if (LS[g].delete === "X" && LS[g].dataCommessa.split("/")[1] === meseAttualeStringa && LS[g].dataCommessa.split("/")[2] === annoAttuale) {
    //                    contoDelete = contoDelete + 1;
    //                };
    //            };
    //            if (contoStato > contoDelete) {
    //                trovatoAlmenoUno = true;
    //            } else {
    //                trovatoAlmenoUno = false;
    //            };
    //        };
    //        var periodoAttivita = utils.LocalStorage.getItem("perioSet").AttivitaChiusa;
    //        if (trovatoAlmenoUno === false || periodoAttivita === "X") {
    //            this.enableModel.setProperty("/deleteEnableMulti", false);
    //        } else if (parseInt(meseAttuale) !== meseOggi) {
    //            this.enableModel.setProperty("/deleteEnableMulti", false);
    //        } else {
    //            this.enableModel.setProperty("/deleteEnableMulti", true);
    //        };
    //        this.updateTable(selectedDate);
    //        this._setDayFull();
    //    },

    updateTable: function (date) {
        if (!date) {
            var selectedDate = utils.Util.dateToString(new Date());
        } else {
            var selectedDate = utils.Util.dateToString(utils.Util.stringToDate(date));
        };
        this.tableModel.setProperty("/giornoSelezionato", selectedDate);
        var objFound = "";
        objFound = _.where(this.datiModel.getData().dati, {
            'dataEvento': selectedDate
        });

        this.tableModel.getData().dati = [];
        if (objFound) {
            for (var i = 0; i < objFound.length; i++) {
                this.tableModel.getData().dati.push(objFound[i]);
            };
        };
        this.tableModel.getData().dati = _.sortBy(this.tableModel.getData().dati, function (m) {
            var orarioInizio = parseFloat(m.oraEventoInizio.split(":")[0] + "." + m.oraEventoInizio.split(":")[1]);
            return orarioInizio;
        });
        this.tableModel.refresh();
    },

    //    _setDayFull: function (idCalendar, activityToRelease) {
    //        /*funzione che aggiunge righe sul giorno del mese in cui sono presenti attività*/
    //        if (!idCalendar) {
    //            var oCal1 = sap.ui.getCore().getElementById("calendarId--calendar");
    //            var annoAttuale = document.getElementById("calendarId--calendar--Head-B2").textContent;
    //            var meseAttuale = document.getElementById("calendarId--calendar--Head-B1").textContent;
    //        } else {
    //            var oCal1 = sap.ui.getCore().getElementById(idCalendar);
    //            var annoAttuale = document.getElementById(idCalendar + "--Head-B2").textContent;
    //            var meseAttuale = document.getElementById(idCalendar + "--Head-B1").textContent;
    //        };
    //        var dataX = "inDate";
    //        var oRefDate = new Date();
    //
    //        meseAttuale = utils.Util._getMeseAttuale(meseAttuale, annoAttuale).mese;

    //        var firstDay = new Date(utils.Util.getFirstDayFromMonth(parseInt(annoAttuale), parseInt(meseAttuale) - 1)).getDate();
    //        var lastDay = new Date(utils.Util.getLastDayFromMonth(parseInt(annoAttuale), parseInt(meseAttuale))).getDate();
    //        for (var i = firstDay; i < lastDay + 1; i++) {
    //            if (i < 10) {
    //                i = "0" + i;
    //            } else {
    //                i = i.toString();
    //            };
    //            if (parseInt(meseAttuale) < 10) {
    //                meseAttuale = "0" + parseInt(meseAttuale);
    //            } else {
    //                meseAttuale = meseAttuale.toString();
    //            };
    //            var data = annoAttuale + "" + meseAttuale + "" + i;
    //            if (!idCalendar) {
    //                document.getElementById("calendarId--calendar--Month0-" + data).style.boxShadow = "none";
    //            } else {
    //                document.getElementById(idCalendar + "--Month0-" + data).style.boxShadow = "none";
    //            };
    //        };

    //        if (this.datiModel.getData().dati) {
    //
    //            if (activityToRelease) {
    //                var anno = _.groupBy(activityToRelease, function (n) {
    //                    return n.dataEvento.split("/")[2]
    //                });
    //            } else {
    //                var anno = _.groupBy(this.datiModel.getData().dati, function (n) {
    //                    return n.dataEvento.split("/")[2]
    //                });
    //            };
    //
    //            for (var d in anno) {
    //                if (annoAttuale === d) {
    //                    var ciccio = _.remove(anno[d], {
    //                        'delete': "X"
    //                    });
    //                    var annoOK = anno[d];
    //                    var mese = _.groupBy(annoOK, function (n) {
    //                        return n.dataEvento.split("/")[1]
    //                    });
    //                    break;
    //                } else {
    //                    var mese = undefined;
    //                };
    //            };
    //
    //            if (mese) {
    //                for (var j in mese) {
    //                    if (meseAttuale === j) {
    //                        var meseOK = mese[j]
    //                        meseOK = _.sortBy(meseOK, 'dataEvento');
    //                    };
    //                };
    //            };
    //            if (meseOK) {
    //                var activityByDate = _.groupBy(meseOK, function (n) {
    //                    return n.dataEvento.split("/")[0]
    //                });
    //                for (var l in activityByDate) {
    //                    var f = activityByDate[l][0].dataEvento.split("/");
    //                    var datamia = f[2] + "" + f[1] + "" + f[0];
    //                    if (!idCalendar) {
    //                        var daySelected = document.getElementById("calendarId--calendar--Month0-" + datamia);
    //                    } else {
    //                        var daySelected = document.getElementById(idCalendar + "--Month0-" + datamia);
    //                    };
    //
    //                    var spessore = 4;
    //
    //                    var orderTemp = _.sortBy(activityByDate[l], function (m) {
    //                        var orarioInizio = parseFloat(m.oraEventoInizio.split(":")[0] + "." + m.oraEventoInizio.split(":")[1]);
    //                        return orarioInizio;
    //                    });
    //                    //inverto l'ordine dell'array cosicché le linee compaiano in maniera corretta
    //                    orderTemp.reverse();
    //                    var arrayStringa = [];
    //                    var stringa = "";
    //                    var supplemento = [];
    //                    for (var y = orderTemp.length; y > 0; y--) {
    //                        if (orderTemp[y - 1].colore === undefined) {
    //                            orderTemp[y - 1].colore = "#000000";
    //                        };
    //                        if (orderTemp.length > 1) {
    //                            if (y !== orderTemp.length) {
    //                                arrayStringa.push("inset " + (spessore * (orderTemp.length - y) + 2 + (supplemento.length * 2)) + "px 0rem 0 #FFFFFF")
    //                                supplemento.push(y);
    //                            };
    //                        };
    //                        arrayStringa.push("inset " + (spessore * (orderTemp.length - y) + spessore + (supplemento.length * 2)) + "px 0rem 0 " + orderTemp[y - 1].colore)
    //                    };
    //                    for (var i = 0; i < arrayStringa.length; i++) {
    //                        stringa = stringa + ", " + arrayStringa[i];
    //                    };
    //                    daySelected.style.boxShadow = stringa.substr(2, stringa.length);
    //                };
    //            };
    //        };
    //        if (!idCalendar && this.c) {
    //            this.c.removeSelections();
    //        };
    //        this._workDay();
    //        this._findActivityToRelease();
    //
    //        /* Codice per nascondere i giorni dal calendario del mese seguente e precedente */
    //        if (!idCalendar) {
    //            var meseAttuale = $("#calendarId--calendar--Head-B1").text();
    //            var annoAttuale = $("#calendarId--calendar--Head-B2").text();
    //        } else {
    //            var meseAttuale = $("#" + idCalendar + "--Head-B1").text();
    //            var annoAttuale = $("#" + idCalendar + "--Head-B2").text();
    //        }
    //        var result = utils.Util._getMeseAttuale(meseAttuale, annoAttuale);
    //        var mese = parseInt(result.mese);
    //        var annoSucc = annoAttuale;
    //        var annoPrec = annoAttuale;
    //        var meseSucc = mese + 1;
    //        var mesePrec = mese - 1;
    //        if (meseSucc < 10) {
    //            meseSucc = "0" + meseSucc;
    //        } else {
    //            meseSucc = meseSucc.toString();
    //        };
    //        if (mesePrec < 10) {
    //            mesePrec = "0" + mesePrec;
    //        } else {
    //            mesePrec = mesePrec.toString();
    //        };
    //        if (mese === 12) {
    //            annoSucc = (parseInt(annoAttuale) + 1).toString();
    //            meseSucc = "01";
    //        };
    //        if (mese === 01) {
    //            annoPrec = (parseInt(annoAttuale) - 1).toString();
    //            mesePrec = "12";
    //        };
    //        for (var i = 1; i <= 6; i++) {
    //            var giorno = i;
    //            var giornoMeseVecchio = parseInt(annoSucc) + "" + meseSucc + "0" + giorno;
    //            if (!idCalendar) {
    //                $("#calendarId--calendar--Month0-" + giornoMeseVecchio).addClass("visible");
    //            } else {
    //                $("#" + idCalendar + "--Month0-" + giornoMeseVecchio).addClass("visible");
    //            }
    //        };
    //        for (var i = 23; i <= 31; i++) {
    //            var giorno = i;
    //            var giornoMeseVecchio = parseInt(annoPrec) + "" + mesePrec + giorno;
    //            if (!idCalendar) {
    //                $("#calendarId--calendar--Month0-" + giornoMeseVecchio).addClass("visible");
    //            } else {
    //                $("#" + idCalendar + "--Month0-" + giornoMeseVecchio).addClass("visible");
    //            }
    //        };
    //
    //        /* popolo la tabella della legenda */
    //
    //        var commesseLegendaColore = utils.LocalStorage.getItem("commessaColorSet");
    //
    //        var rapportiniLS = utils.LocalStorage.getItem("attivitaSet");
    //        var commesseMeseCorrente = [];
    //        for (var k = 0; k < rapportiniLS.length; k++) {
    //            if (rapportiniLS[k].dataCommessa.split("/")[1] === result.mese && rapportiniLS[k].dataCommessa.split("/")[2] === annoAttuale && rapportiniLS[k].delete !== "X") {
    //                commesseMeseCorrente.push(rapportiniLS[k].commessa);
    //            };
    //        };
    //
    //        var commesseLegendaColore;
    //        var commesseDaTenere = [];
    //        for (var r = 0; r < commesseMeseCorrente.length; r++) {
    //            var commessaTrovata = _.find(commesseLegendaColore, {
    //                'commessa': commesseMeseCorrente[r]
    //            });
    //            commesseDaTenere.push(commessaTrovata);
    //        };
    //        commesseDaTenere = _.uniq(commesseDaTenere);
    //        var progettiPerLegenda = utils.LocalStorage.getItem("projectSet");
    //        var progettiPerLegendaDesc = progettiPerLegenda;
    //        for (var i = 0; i < commesseDaTenere.length; i++) {
    //            commesseDaTenere[i].nome = _.find(progettiPerLegendaDesc, {
    //                'commessa': commesseDaTenere[i].commessa
    //            }).descrizione
    //        };
    //        this.legendModel.setProperty("/commesse", commesseDaTenere);
    //        if (commesseDaTenere.length === 0) {
    //            this.legendModel.setProperty("/rowsNumber", 1);
    //        } else if (commesseDaTenere.length > 0 && commesseDaTenere.length <= 7) {
    //            this.legendModel.setProperty("/rowsNumber", commesseDaTenere.length);
    //        } else {
    //            this.legendModel.setProperty("/rowsNumber", 7);
    //        };
    //        if (window.screen.width >= 1280) {
    //            this.legendModel.setProperty("/visible", true);
    //        } else {
    //            this.legendModel.setProperty("/visible", false);
    //        };
    //    },

    onAppointmentSelect: function (evt) {
        var path = evt.getSource().getBindingContextPath();
        path = parseInt(path.split("/")[2]);
        var obj = this.tableModel.getData().dati[path];
        //        if (obj.counterFiori) {
        //            var objClone = _.clone(obj);
        //            objClone.counter = obj.counterFiori;
        //        } else {
        //            var objClone = _.clone(obj);
        //            objClone.counter = obj.counter;
        //        };
        this.getView().byId("idTable").removeSelections();
        var dataPattern = obj.dataEvento.split("/").join("");
        sessionStorage.setItem("selectedAppointment", JSON.stringify(obj));
        this.passatoInOnAfterRendering = false;
        this.router.navTo("appointments", {
            state: "edit",
            data: dataPattern
        });
    },

    onSynchroPress: function () {
        if (Offline.state === "up") {
            var attivitaToSync = utils.LocalStorage.getItem("attivitaSet");
            _.remove(attivitaToSync, {
                'counterFiori': ""
            });
            _.remove(attivitaToSync, {
                'counter': "",
                'delete': "X"
            });
            var notaSpeseToSync = utils.LocalStorage.getItem("notaSpeseSet");
            _.remove(notaSpeseToSync, {
                'counterFiori': ""
            });
            for (var h = 0; h < notaSpeseToSync.length; h++) {
                if (notaSpeseToSync[h].numProgressivoGiustificativo === "" && notaSpeseToSync[h].delete === "X") {
                    _.remove(notaSpeseToSync, notaSpeseToSync[h]);
                };
            };
            if (attivitaToSync.length === 0 && notaSpeseToSync.length === 0) {
                sap.m.MessageBox.show(this._getLocaleText("noDatiSincronizzabili"),
                    sap.m.MessageBox.Icon.NONE,
                    this._getLocaleText("confermaSincronizzazione"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
                    _.bind(this.onOkSynchroPress, this));
            } else {
                var cidSet = {};
                var cidElement = utils.LocalStorage.getItem("cidSet")[0];
                var datiUtenteLS = $.parseJSON(atob(localStorage.getItem("user")));
                for (var i = 0; i < datiUtenteLS.length; i++) {
                    if (datiUtenteLS[i].id === sessionStorage.getItem("cid")) {
                        this.password = datiUtenteLS[i].p;
                        break;
                    };
                };
                cidElement.password = this.password;
                var user = model.odata.serializer.cidEntity.toSap(cidElement);

                TimesheetSet = [];
                for (var i = 0; i < attivitaToSync.length; i++) {
                    TimesheetSet.push(attivitaToSync[i]);
                };
                this.TimeSheet = [];
                for (var i = 0; i < TimesheetSet.length; i++) {
                    this.TimeSheet.push(model.odata.serializer.attivitaEntity.toSap(TimesheetSet[i]));
                };

                TravelSet = [];
                for (var i = 0; i < notaSpeseToSync.length; i++) {
                    TravelSet.push(notaSpeseToSync[i]);
                };
                this.TravelSet = [];
                for (var i = 0; i < TravelSet.length; i++) {
                    this.TravelSet.push(model.odata.serializer.notaSpeseEntity.toSap(TravelSet[i]));
                };

                cidElement = {};
                cidElement = user;
                cidElement.TravelSet = this.TravelSet;
                cidElement.TimesheetSet = this.TimeSheet;
                cidSet.cidElement = cidElement;

                //// scrittura su sap //////

                var that = this;
                this.oDialog.open();
                this.getOdataModel();
                this.modelloOdata = this.getView().getModel().createEntry("CIDSet", cidSet.cidElement);
                this._odataModel.submitChanges(function () {
                    if (!that.nonEntrareInCallAttivitaSet) {
                        that._odataCallAttivitaSetRefresh(sessionStorage.getItem("cid"), that);
                    } else {
                        that.oDialog.close();
                        sap.m.MessageBox.alert(that._getLocaleText("SincronizzazioneRiuscita"), {
                            title: that._getLocaleText("WARNING")
                        });
                    }
                }, function (err) {
                    that.oDialog.close();
                    that._setDayFull();
                    sap.m.MessageBox.error(JSON.parse(err.response.body).error.message.value, {
                        title: that._getLocaleText("WARNING")
                    });
                });
            };
            var oModel = new sap.ui.model.json.JSONModel(this._data);
            this.getView().setModel(oModel);
        } else {
            var that = this;
            sap.m.MessageBox.alert(that._getLocaleText("noConnessione"), {
                title: that._getLocaleText("WARNING")
            });
        }
    },

    onGoogleSynchro: function () {
        var clientId = '1081157756063-sem1d4ssgjhjdcrkvuf4le42ilp4f0r6.apps.googleusercontent.com';
        var apiKey = 'w4BqVBYAHoXCrvTfnlzxHIcY';
        var scopes = 'https://www.googleapis.com/auth/calendar';
        gapi.client.setApiKey(apiKey);
        window.setTimeout(function () {
            gapi.auth.authorize({
                    client_id: clientId,
                    scope: scopes,
                    immediate: false
                },
                function (authResult) {
                    if (authResult && !authResult.error) {
                        gapi.client.setApiKey("");
                        gapi.client.load('calendar', 'v3').then(function () {
                            var now = new Date();
                            var start = now.toISOString();
                            var end = new Date(now.getTime() + (2 * 1000 * 60 * 60));
                            end = end.toISOString();
                            gapi.client.calendar.events.insert({
                                'calendarId': 'primary', // calendar ID
                                'resource': {
                                    "summary": "Sample Event",
                                    "start": {
                                        "dateTime": start
                                    },
                                    "end": {
                                        "dateTime": end
                                    }
                                }
                            }).then(function (response) {
                                sap.m.MessageBox.alert("Synchronized with Google Calendar", {
                                    title: "SUCCESS"
                                });
                            }, function (reason) {
                                console.log('Error ' + JSON.stringify(reason));
                                sap.m.MessageBox.alert("Error synchronizing with Google Calendar", {
                                    title: "WARNING"
                                });
                            });
                        });
                    } else {
                        sap.m.MessageBox.alert("Error authenticating to Google Calendar", {
                            title: "WARNING"
                        });
                    }
                });
        }, 1);
    },

    onOkSynchroPress: function (evt) {
        if (evt === sap.m.MessageBox.Action.YES || evt === undefined) {
            if (this.refresh === true) {
                utils.LocalStorage.setItem("attivitaSet", []);
                utils.LocalStorage.setItem("notaSpeseSet", []);
            };
            this.oDialog.open();
            that = this;
            this._odataCallAttivitaSetRefresh(sessionStorage.getItem("cid"), that)
        } else {
            this.refresh = false;
        };
    },

    getOdataModel: function () {
        if (this._odataModel) {
            this._odataModel.destroy();
        };
        var url = model.odata.chiamateOdata._serviceUrl;
        this._odataModel = new sap.ui.model.odata.ODataModel(url, maxDataServiceVersion = '2.0');
        return this.getView().setModel(this._odataModel);
    },

    onReportsPress: function () {
        clearInterval(this.mySetInterval);
        this.passatoInOnAfterRendering = false;
        var meseAtt = document.getElementById("calendarId--calendar--Head-B1").textContent;
        var annoAttuale = document.getElementById("calendarId--calendar--Head-B2").textContent;
        var meseAttuale = parseInt(utils.Util._getMeseAttuale(meseAtt, annoAttuale).mese);
        sessionStorage.setItem("objMese", meseAttuale);
        sessionStorage.setItem("objAnno", annoAttuale);
        this.router.navTo("reports");
    },

    pressSelectMulti: function (evt) {
        var tabellaAppuntamenti = this.getView().byId("idTable");
        var appuntamentiSelezionati = tabellaAppuntamenti.getSelectedItems();
        if (appuntamentiSelezionati.length > 0) {
            var checkSelezionato = true;
        } else {
            var checkSelezionato = false;
        };
        if (checkSelezionato) {
            this.enableModel.setProperty("/deleteEnable", true);
        } else {
            this.enableModel.setProperty("/deleteEnable", false);
        };
    },

    onDeleteMultiPress: function () {
        this._oListFragment = sap.ui.xmlfragment("icms.view.fragment.selectDates", this);
        this.enableModel.setProperty("/confirmEnable", false);
        this.enableModel.setProperty("/testoBottoneVerde", this._getLocaleText("Confirm"));
        this.enableModel.setProperty("/testoBottoneRosso", this._getLocaleText("CANCEL"));
        this._oListFragment.setModel(this.enableModel);
        this._oListFragment.open();
        var idCalendar = "calendarDuplicateActivity";
        var rapportiniTotali = utils.LocalStorage.getItem("attivitaSet");
        _.remove(rapportiniTotali, {
            'stato': "30"
        });
        _.remove(rapportiniTotali, {
            'stato': "20"
        });
        _.remove(rapportiniTotali, {
            'stato': "40"
        });
        _.remove(rapportiniTotali, {
            'stato': "50"
        });
        this.activityToDelete = rapportiniTotali;
        this._setDayFull(idCalendar, rapportiniTotali);
        this._variabileConfirm = "cancellaDuplica";
        $("#calendarDuplicateActivity--Head-B1").attr("disabled", "disabled");
        $("#calendarDuplicateActivity--Head-B2").attr("disabled", "disabled");

        /* DISABILITO LA POSSIBILITA DI SELEZIONARE ALTRI MESI */
        $("#calendarDuplicateActivity--Head-next").addClass("visible");
        $("#calendarDuplicateActivity--Head-prev").addClass("visible");
        var annoAttuale = document.getElementById("calendarDuplicateActivity--Head-B2").textContent;
        var meseAttuale = document.getElementById("calendarDuplicateActivity--Head-B1").textContent;
        var result = utils.Util._getMeseAttuale(meseAttuale, annoAttuale);
        var mese = parseInt(result.mese);
        var mesePrec = mese - 1;
        var meseSucc = mese + 1;
        if (mesePrec < 10) {
            mesePrec = "0" + mesePrec;
        } else {
            mesePrec = mesePrec.toString();
        }
        if (meseSucc < 10) {
            meseSucc = "0" + meseSucc;
        } else {
            meseSucc = meseSucc.toString();
        };
        if (mese === 1) {
            annoAttuale = (parseInt(annoAttuale) - 1).toString();
            mesePrec = "12";
        };
        if (mese === 12) {
            annoAttuale = (parseInt(annoAttuale) + 1).toString();
            meseSucc = "01";
        };
        for (var i = 23; i <= 31; i++) {
            var giorno = i;
            var giornoMeseVecchio = parseInt(annoAttuale) + "" + mesePrec + "" + giorno;
            $("#calendarDuplicateActivity--Month0-" + giornoMeseVecchio).addClass("visible");
        };
        for (var i = 1; i <= 6; i++) {
            var giorno = i;
            var giornoMeseVecchio = parseInt(annoAttuale) + "" + meseSucc + "0" + giorno;
            $("#calendarDuplicateActivity--Month0-" + giornoMeseVecchio).addClass("visible");
        };
        /********************************/
    },

    onDuplicatePress: function () {
        //        if (!this._oListFragment) {
        this._oListFragment = sap.ui.xmlfragment("icms.view.fragment.selectDates", this);
        //        };
        this.enableModel.setProperty("/confirmEnable", false);
        this.enableModel.setProperty("/testoBottoneVerde", this._getLocaleText("Duplicate"));
        this.enableModel.setProperty("/testoBottoneRosso", this._getLocaleText("CANCEL"));
        this._oListFragment.setModel(this.enableModel);
        this._oListFragment.open();

        var cal = this._oListFragment.getAggregation("content")[0]; // 04/02/2016

        var idCalendar = "calendarDuplicateActivity";
        //        this._setDayFull(idCalendar); // 04/02/2016
        this._variabileConfirm = "duplica";
        //        $("#calendarDuplicateActivity--Head-B1").attr("disabled", "disabled"); // 04/02/2016
        //        $("#calendarDuplicateActivity--Head-B2").attr("disabled", "disabled"); // 04/02/2016

        /* DISABILITO LA POSSIBILITA DI SELEZIONARE ALTRI MESI */
        //        $("#calendarDuplicateActivity--Head-next").addClass("visible"); // 04/02/2016
        //        $("#calendarDuplicateActivity--Head-prev").addClass("visible"); // 04/02/2016
        var annoAttuale = document.getElementById("calendarDuplicateActivity--Head-B2").textContent;
        var meseAttuale = document.getElementById("calendarDuplicateActivity--Head-B1").textContent;
        var result = utils.Util._getMeseAttuale(meseAttuale, annoAttuale);
        var mese = parseInt(result.mese);

        // 04/02/2016
        var meseCalendarioHome = document.getElementById("calendarId--calendar--Head-B1").textContent;
        var annoCalendarioHome = document.getElementById("calendarId--calendar--Head-B2").textContent;
        var resultMeseCalendarioHome = utils.Util._getMeseAttuale(meseCalendarioHome, annoCalendarioHome);
        var intMeseCalendarioHome = parseInt(resultMeseCalendarioHome.mese);
        var dataOdierna = new Date();
        var meseOdierno = dataOdierna.getMonth() + 1;
        var periodoOpen = utils.LocalStorage.getItem("perioSet").AttivitaChiusa;
        if (((meseOdierno - intMeseCalendarioHome) === 1 || (meseOdierno === 0 && intMeseCalendarioHome === 11)) && periodoOpen !== "X") {
            var d = new Date(parseInt(annoCalendarioHome), intMeseCalendarioHome, 0);
            cal._oFocusedDate._oInnerDate = d;
            this._oListFragment.rerender();
            $("#calendarDuplicateActivity--Head-prev").addClass("visible");
            var mesePrec = intMeseCalendarioHome - 1;
            var meseSucc = intMeseCalendarioHome + 1;
        } else {
            cal._oFocusedDate._oInnerDate = dataOdierna;
            this._oListFragment.rerender();
            $("#calendarDuplicateActivity--Head-prev").addClass("visible");
            $("#calendarDuplicateActivity--Head-next").addClass("visible");
            var mesePrec = mese - 1;
            var meseSucc = mese + 1;
        }

        $("#calendarDuplicateActivity--Head-B1").attr("disabled", "disabled");
        $("#calendarDuplicateActivity--Head-B2").attr("disabled", "disabled");
        this._setDayFull(idCalendar);

        if (mesePrec < 10) {
            mesePrec = "0" + mesePrec;
        } else {
            mesePrec = mesePrec.toString();
        }
        if (meseSucc < 10) {
            meseSucc = "0" + meseSucc;
        } else {
            meseSucc = meseSucc.toString();
        };
        if (mese === 1 || intMeseCalendarioHome === 1) {
            annoAttuale = (parseInt(annoAttuale) - 1).toString();
            mesePrec = "12";
        };
        if (mese === 12 || intMeseCalendarioHome === 12) {
            annoAttuale = (parseInt(annoAttuale) + 1).toString();
            meseSucc = "01";
        };
        for (var i = 23; i <= 31; i++) {
            var giorno = i;
            var giornoMeseVecchio = parseInt(annoAttuale) + "" + mesePrec + "" + giorno;
            $("#calendarDuplicateActivity--Month0-" + giornoMeseVecchio).addClass("visible");
        };
        for (var i = 1; i <= 6; i++) {
            var giorno = i;
            var giornoMeseVecchio = parseInt(annoAttuale) + "" + meseSucc + "0" + giorno;
            $("#calendarDuplicateActivity--Month0-" + giornoMeseVecchio).addClass("visible");
        };
        /********************************/
        setTimeout(_.bind(this.onAfterOpenDialog, this)); // 04/02/2016
    },

    // 04/02/2016
    onAfterOpenDialog: function () {
        var frecceDialog = function (evt) {
            if (document.getElementById("calendarDuplicateActivity--Head-B1")) {
                var buttonPress = (evt.currentTarget.id).split("-");
                buttonPress = buttonPress[buttonPress.length - 1];
                if (buttonPress === "prev") {
                    console.log("prev")
                    $("#calendarDuplicateActivity--Head-prev").addClass("visible");
                    $("#calendarDuplicateActivity--Head-next").removeClass("visible");
                } else if (buttonPress === "next") {
                    console.log("next")
                    $("#calendarDuplicateActivity--Head-prev").removeClass("visible");
                    $("#calendarDuplicateActivity--Head-next").addClass("visible");
                };
            }
            var idCalendar = "calendarDuplicateActivity";
            setTimeout(_.bind(this._setDayFull, this), 300, idCalendar);
        };
        frecceDialog = _.bind(frecceDialog, this);
        $("#calendarDuplicateActivity--Head-next").on("click", frecceDialog);
        $("#calendarDuplicateActivity--Head-prev").on("click", frecceDialog);
        $("#calendarDuplicateActivity--Head-B1").on("click", function () {
            preventDefault();
        });
        $("#calendarDuplicateActivity--Head-B2").on("click", function () {
            preventDefault();
        });

        var annoAttuale = document.getElementById("calendarDuplicateActivity--Head-B2").textContent;
        var meseAttuale = document.getElementById("calendarDuplicateActivity--Head-B1").textContent;
        var result = utils.Util._getMeseAttuale(meseAttuale, annoAttuale);
        var mese = parseInt(result.mese);
        var mesePrec = mese - 1;
        var meseSucc = mese + 1;
        if (mesePrec < 10) {
            mesePrec = "0" + mesePrec;
        } else {
            mesePrec = mesePrec.toString();
        }
        if (meseSucc < 10) {
            meseSucc = "0" + meseSucc;
        } else {
            meseSucc = meseSucc.toString();
        };
        if (mese === 1) {
            annoAttuale = (parseInt(annoAttuale) - 1).toString();
            mesePrec = "12";
        };
        if (mese === 12) {
            annoAttuale = (parseInt(annoAttuale) + 1).toString();
            meseSucc = "01";
        };
        for (var i = 23; i <= 31; i++) {
            var giorno = i;
            var giornoMeseVecchio = parseInt(annoAttuale) + "" + mesePrec + "" + giorno;
            $("#calendarDuplicateActivity--Month0-" + giornoMeseVecchio).addClass("visible");
        };
        for (var i = 1; i <= 6; i++) {
            var giorno = i;
            var giornoMeseVecchio = parseInt(annoAttuale) + "" + meseSucc + "0" + giorno;
            $("#calendarDuplicateActivity--Month0-" + giornoMeseVecchio).addClass("visible");
        };
    },
    //

    setDates: function (oEvent) {
        var oCalendar = oEvent.oSource;
        var aSelectedDates = oCalendar.getSelectedDates();
        var oDate;
        this.oData = {
            selectedDates: []
        };
        if (aSelectedDates.length > 0) {
            this.enableModel.setProperty("/confirmEnable", true);
            for (var i = 0; i < aSelectedDates.length; i++) {
                oDate = aSelectedDates[i].getStartDate();
                this.oData.selectedDates.push(utils.Util.dateToString(oDate));
            };
        } else {
            this.enableModel.setProperty("/confirmEnable", false);
            this._clearModel();
        };
    },

    cancelPress: function (oEvent) {
        this._clearModel();
        this._oListFragment.close();
        this._oListFragment.destroy(true);
        this.enableModel.setProperty("/duplicateEnable", false);
        this.enableModel.setProperty("/deleteEnable", false);
        this.enableModel.setProperty("/unReleaseEnable", false);
        var LS = utils.LocalStorage.getItem("attivitaSet");
        var trovatoAlmenoUno = false;
        if (LS.length !== 0) {
            var contoStato = 0;
            var contoDelete = 0;
            for (var g = 0; g < LS.length; g++) {
                if (LS[g].stato === "10") {
                    contoStato = contoStato + 1;
                };
                if (LS[g].delete === "X") {
                    contoDelete = contoDelete + 1;
                };
            };
            if (contoStato > contoDelete) {
                trovatoAlmenoUno = true;
            } else {
                trovatoAlmenoUno = false;
            };
        };
        var meseAttText = document.getElementById("calendarId--calendar--Head-B1").textContent;
        var annoAttualeText = document.getElementById("calendarId--calendar--Head-B2").textContent;
        var meseAttuale = parseInt(utils.Util._getMeseAttuale(meseAttText, annoAttualeText).mese);
        var meseReale = new Date().getMonth() + 1;
        var annoReale = (new Date().getFullYear()).toString();
        var periodoAttivita = utils.LocalStorage.getItem("perioSet").AttivitaChiusa;
        if (trovatoAlmenoUno === false || periodoAttivita === "X" || meseAttuale !== meseReale || annoReale !== annoAttualeText) {
            this.enableModel.setProperty("/deleteEnableMulti", false);
        } else {
            this.enableModel.setProperty("/deleteEnableMulti", true);
        };
        this._setDayFull();
    },

    _clearModel: function () {
        var oData = {
            selectedDates: []
        };
        this.oModel.setData(oData);
    },

    confirmPress: function () {
        if (this.c !== undefined) {
            this.c.removeSelections()
        };
        var dataToSync = this.datiModel.getData().dati;
        var dataToSave = this.obj;

        if (this._variabileConfirm === "duplica") {
            this._confirmDuplicate(dataToSync, dataToSave);
        } else if (this._variabileConfirm === "rilascia") {
            this._confirmRelease();
        } else if (this._variabileConfirm === "cancellaDuplica") {
            this._confirmMultiDelete();
        };
        this._oListFragment.close();
        this._oListFragment.destroy(true);
        this.updateTable(this.selectedDate);
        this._setDayFull();
    },

    _confirmMultiDelete: function () {
        this.periodoSbagliato = false
        var notaSpeseSet = utils.LocalStorage.getItem("notaSpeseSet");
        var dataX = "inDate";
        var oRefDate = new Date();
        for (var l = 0; l < this.oData.selectedDates.length; l++) {
            var activityToDelete = _.where(this.activityToDelete, {
                'dataCommessa': this.oData.selectedDates[l],
                'stato': "10"
            });

            var dataToSync = this.datiModel.getData().dati;

            for (var r = 0; r < activityToDelete.length; r++) {
                if (activityToDelete[r].counter !== "") {
                    _.remove(this.datiModel.getData().dati, {
                        'counter': activityToDelete[r].counter
                    });
                } else {
                    _.remove(this.datiModel.getData().dati, {
                        'counterFiori': activityToDelete[r].counterFiori
                    });
                };
                var dataUltimaModifica = new Date();
                dataUltimaModifica = dataUltimaModifica.getDate() + "/" + parseInt(dataUltimaModifica.getMonth() + 1) + "/" + +dataUltimaModifica.getFullYear();
                var oraUltimaModifica = new Date();
                oraUltimaModifica = oraUltimaModifica.getHours() + ":" + oraUltimaModifica.getMinutes() + ":" + oraUltimaModifica.getSeconds();
                activityToDelete[r].dataUltimaModifica = dataUltimaModifica;
                activityToDelete[r].oraUltimaModifica = oraUltimaModifica;
                activityToDelete[r].delete = "X";
                highest = 0;
                for (var y = 0; y < dataToSync.length; y++) {
                    if (parseInt(dataToSync[y].counterFiori) > highest) {
                        highest = parseInt(dataToSync[y].counterFiori);
                    };
                };

                // guardo se c'è un rapportino in un mese vecchio
                var meseAttuale = (new Date().getMonth() + 1).toString();
                var annoAttuale = (new Date().getFullYear()).toString();
                if (meseAttuale.length < 2) {
                    meseAttuale = "0" + meseAttuale;
                };
                var meseRapportino = activityToDelete[r].dataCommessa.split("/")[1];
                var annoRapportino = activityToDelete[r].dataCommessa.split("/")[2];
                if (meseRapportino !== meseAttuale || annoRapportino !== annoAttuale) {
                    this.periodoSbagliato = true;
                };


                activityToDelete[r].counterFiori = (highest + 1).toString();
                this.datiModel.getData().dati.push(activityToDelete[r]);
                oRefDate.setDate(1);
                oRefDate.setMonth(parseInt(activityToDelete[r].dataCommessa.split("/")[1]) - 1);
                oRefDate.setYear(parseInt(activityToDelete[r].dataCommessa.split("/")[2]));
                document.getElementById("calendarId--calendar--Month0-" + utils.Util.dateToString(oRefDate.setDate(activityToDelete[r].dataCommessa.slice(0, 2)), dataX)).style.boxShadow = "none";

                //////////////// cancello le corrispondenti noteSpese ////////////////
                var noteSpeseToDelete = _.remove(notaSpeseSet, {
                    'dataNotaSpese': activityToDelete[r].dataCommessa,
                    'attivita': activityToDelete[r].attivita
                });

                highest = 0;
                for (var y = 0; y < notaSpeseSet.length; y++) {
                    if (parseInt(notaSpeseSet[y].counterFiori) > highest) {
                        highest = parseInt(notaSpeseSet[y].counterFiori);
                    };
                };

                for (var i = 0; i < noteSpeseToDelete.length; i++) {
                    if (noteSpeseToDelete[i].counterFiori === "") {
                        noteSpeseToDelete[i].counterFiori = (highest + 1).toString();
                    };
                    noteSpeseToDelete[i].dataUltimaModifica = dataUltimaModifica;
                    noteSpeseToDelete[i].oraUltimaModifica = oraUltimaModifica;
                    noteSpeseToDelete[i].delete = "X";
                    notaSpeseSet.push(noteSpeseToDelete[i]);
                };
            };
            utils.LocalStorage.setItem("attivitaSet", this.datiModel.getData().dati);
            utils.LocalStorage.setItem("notaSpeseSet", notaSpeseSet);
        };
        var LS = utils.LocalStorage.getItem("attivitaSet");
        var trovatoAlmenoUno = false;
        if (LS.length !== 0) {
            var contoStato = 0;
            var contoDelete = 0;
            for (var g = 0; g < LS.length; g++) {
                if (LS[g].stato === "10") {
                    contoStato = contoStato + 1;
                }
                if (LS[g].delete === "X") {
                    contoDelete = contoDelete + 1;
                };
            };
            if (contoStato > contoDelete) {
                trovatoAlmenoUno = true;
            } else {
                trovatoAlmenoUno = false;
            };
        };
        var annoAttualeText = document.getElementById("calendarId--calendar--Head-B2").textContent;
        var annoReale = (new Date().getFullYear()).toString();
        var meseAttText = document.getElementById("calendarId--calendar--Head-B1").textContent;
        var meseAttuale = parseInt(utils.Util._getMeseAttuale(meseAttText, annoAttualeText).mese);
        var meseReale = new Date().getMonth() + 1;
        var periodoAttivita = utils.LocalStorage.getItem("perioSet").AttivitaChiusa;
        if (trovatoAlmenoUno === false || periodoAttivita === "X" || meseAttuale !== meseReale || annoAttualeText !== annoReale.toString()) {
            this.enableModel.setProperty("/deleteEnableMulti", false);
        } else {
            this.enableModel.setProperty("/deleteEnableMulti", true);
        };
        this.calcolaOreLavorate();
        this.enableModel.refresh();
        if (this.periodoSbagliato === true) {
            sap.m.MessageToast.show(this._getLocaleText("periodoSbagliato"), {
                duration: 2000
            });
            this.periodoSbagliato = false;
        };
    },

    _confirmDuplicate: function (dataToSync, dataToSave) {
        this.periodoSbagliato = false
        var validateCommessa = true;
        var trovataNotaSpese = false;
        var dateToSaveClone = []
        this.inserimentoAttivitaErrore = false;
        this.trovateSpese = [];
        this.cambiaOraRapportino = false;
        this.orarioMinNove = false;

        for (var h = 0; h < this.oData.selectedDates.length; h++) {
            var oneTime = true;
            for (var cc = 0; cc < dataToSave.length; cc++) {
                this.trovateSpese = _.where(this.notaSpeseSet, {
                    'dataNotaSpese': dataToSave[cc].dataCommessa,
                    'attivita': dataToSave[cc].attivita
                });
                _.remove(this.trovateSpese, {
                    'delete': "X"
                });
                dateToSaveClone = _.cloneDeep(dataToSave);
                this.dateToSaveClone = dateToSaveClone[cc];

                highest = 0;
                for (var y = 0; y < dataToSync.length; y++) {
                    if (parseInt(dataToSync[y].counterFiori) > highest) {
                        highest = parseInt(dataToSync[y].counterFiori);
                    };
                };
                var dataUltimaModifica = new Date();
                dataUltimaModifica = dataUltimaModifica.getDate() + "/" + parseInt(dataUltimaModifica.getMonth() + 1) + "/" + +dataUltimaModifica.getFullYear();
                var oraUltimaModifica = new Date();
                oraUltimaModifica = oraUltimaModifica.getHours() + ":" + oraUltimaModifica.getMinutes() + ":" + oraUltimaModifica.getSeconds();
                dateToSaveClone[cc].counterFiori = (highest + 1).toString();
                dateToSaveClone[cc].counter = "";
                dateToSaveClone[cc].dataCommessa = this.oData.selectedDates[h];
                // guardo se c'è un rapportino in un mese vecchio
                var meseAttuale = (new Date().getMonth() + 1).toString();
                var annoAttuale = (new Date().getFullYear()).toString();
                if (meseAttuale.length < 2) {
                    meseAttuale = "0" + meseAttuale;
                };
                var meseRapportino = this.oData.selectedDates[h].split("/")[1];
                var annoRapportino = this.oData.selectedDates[h].split("/")[2];
                if (meseRapportino !== meseAttuale || annoRapportino !== annoAttuale) {
                    this.periodoSbagliato = true;
                };


                var dataRiferimento = new Date();
                var meseCheMiServe = dataRiferimento.getMonth() + 1;
                // 05/02/2015
                if (parseInt(this.oData.selectedDates[h].split("/")[1]) === meseCheMiServe) {
                    dateToSaveClone[cc].stato = "10";
                } else {
                    dateToSaveClone[cc].stato = "20";
                }
                //
                //                dateToSaveClone[cc].stato = "10"; // 05/02/2015
                dateToSaveClone[cc].dataUltimaModifica = dataUltimaModifica;
                dateToSaveClone[cc].oraUltimaModifica = oraUltimaModifica;
                var commessa = dataToSave[cc].commessa;
                var attivita = dataToSave[cc].attivita;
                var dataSelezionata = this.oData.selectedDates[h];
                var durataSelezionata = parseInt(dataToSave[cc].durata);
                var durataSelezionata = parseInt(dataToSave[cc].durata);

                /* Controllo ore per WBS */
                var commesseGiornoSelezionato = _.where(dataToSync, {
                    'dataCommessa': this.oData.selectedDates[h],
                    'tipoOrario': ""
                });

                _.remove(commesseGiornoSelezionato, {
                    'delete': "X"
                });

                var oreMaxPerCommessa = 8;
                for (var q = 0; q < commesseGiornoSelezionato.length; q++) {
                    if (commesseGiornoSelezionato[q].tipoOrario === "" && dataToSave[cc].tipoOrario === "") {
                        oreMaxPerCommessa = oreMaxPerCommessa - parseFloat(commesseGiornoSelezionato[q].durata);
                    };
                };

                var oreDifferenza = oreMaxPerCommessa - parseFloat(dataToSave[cc].durata);
                if (oreDifferenza < 0) {
                    sap.m.MessageBox.error(this._getLocaleText("oreWBSSuperate"), {
                        title: this._getLocaleText("WARNING")
                    });
                    validateCommessa = false;
                };
                /*******************/

                /* Controllo ore per giornata max 24 */
                var commesseGiornoSelezionato = _.where(dataToSync, {
                    'dataCommessa': this.oData.selectedDates[h]
                });
                var oreMaxPerGiorno = 24;
                for (var q = 0; q < commesseGiornoSelezionato.length; q++) {
                    oreMaxPerGiorno = oreMaxPerGiorno - parseFloat(commesseGiornoSelezionato[q].durata);
                };
                var oreDifferenza = oreMaxPerGiorno - parseFloat(dataToSave[cc].durata);
                if (oreDifferenza < 0) {
                    sap.m.MessageBox.error(this._getLocaleText("oreGiornoSuperate"), {
                        title: this._getLocaleText("WARNING")
                    });
                    validateCommessa = false;
                };
                /*******************/

                var attivitaSet = utils.LocalStorage.getItem("attivitaSet");

                var attivitaGiornaliere = _.where(dataToSync, {
                    'dataCommessa': dateToSaveClone[cc].dataCommessa
                });

                _.remove(attivitaGiornaliere, {
                    'delete': "X"
                });

                var orarioInizio = dateToSaveClone[cc].orarioInizio;
                var orarioFine = dateToSaveClone[cc].orarioFine;

                var oraInizio = parseInt(orarioInizio.split(":")[0]);
                var minutiInizio = parseInt(orarioInizio.split(":")[1]);
                if (!minutiInizio) {
                    var minutiInizio = 0;
                };
                var oraFine = parseInt(orarioFine.split(":")[0]);
                var minutiFine = parseInt(orarioFine.split(":")[1]);
                if (!minutiFine) {
                    var minutiFine = 0;
                };
                /// TRASFORMO L'ORARIO DI INIZIO E FINE IN MODO DA RENDERLI CONFRONTABILI ///
                var orarioInizio = parseFloat(oraInizio + "." + minutiInizio);
                var orarioFine = parseFloat(oraFine + "." + minutiFine);

                if (attivitaGiornaliere.length > 0) {
                    for (var i = 0; i < attivitaGiornaliere.length; i++) {
                        var oraInizioAttivitaIesima = parseInt(attivitaGiornaliere[i].orarioInizio.split(":")[0]);
                        var oraFineAttivitaIesima = parseInt(attivitaGiornaliere[i].orarioFine.split(":")[0]);
                        var minutiInizioAttivitaIesima = parseInt(attivitaGiornaliere[i].orarioInizio.split(":")[1]);
                        var minutiFineAttivitaIesima = parseInt(attivitaGiornaliere[i].orarioFine.split(":")[1]);
                        var orarioInizioAttivitaIesima = parseFloat(oraInizioAttivitaIesima + "." + minutiInizioAttivitaIesima);
                        var orarioFineAttivitaIesima = parseFloat(oraFineAttivitaIesima + "." + minutiFineAttivitaIesima);
                        if (orarioInizio >= orarioFineAttivitaIesima || orarioFine <= orarioInizioAttivitaIesima) {
                            this.inserimentoAttivitaErrore = false;
                        } else {
                            this.cambiaOraRapportino = true;
                            break;
                        };
                    };
                } else {
                    this.orarioMinNove = true;
                }

                if (dataToSave.length > 1) {
                    if (h === (this.oData.selectedDates.length - 1) && this.trovateSpese.length !== 0) {
                        if (oneTime == true) {
                            sap.m.MessageBox.error(this._getLocaleText("noDuplicaNotaSpese"), {
                                title: this._getLocaleText("WARNING")
                            });
                            //                            validateCommessa = false; // 05/02/2016
                            break;
                        };
                        oneTime = false;
                    } else if (this.trovateSpese.length !== 0) { // 05/02/2016
                        validateCommessa = false; // 05/02/2016
                    } // 05/02/2016
                };

                if (validateCommessa) {
                    if (this.orarioMinNove === true) {
                        var maxFine = 9;
                        this.orarioMinNove = false;
                    } else {
                        var maxFine = 0;
                    };
                    for (var u = 0; u < attivitaGiornaliere.length; u++) {
                        var oraFineAttivitaIesima = parseInt(attivitaGiornaliere[u].orarioFine.split(":")[0]);
                        var minutiFineAttivitaIesima = parseInt(attivitaGiornaliere[u].orarioFine.split(":")[1]);
                        var orarioFineAttivitaIesima = parseFloat(oraFineAttivitaIesima + "." + minutiFineAttivitaIesima);
                        if (orarioFineAttivitaIesima > maxFine) {
                            maxFine = orarioFineAttivitaIesima;
                        };
                    };
                    var orarioInizioRapportino = maxFine;
                    var oraInizioRapportino = parseInt(orarioInizioRapportino.toString().split(".")[0]);
                    var minutiInizioRapportino = parseInt(orarioInizioRapportino.toString().split(".")[1]);
                    if (minutiInizioRapportino) {
                        minutiInizioRapportino = 50;
                    } else {
                        minutiInizioRapportino = 0;
                    };
                    orarioInizioRapportino = parseFloat(oraInizioRapportino + "." + minutiInizioRapportino);
                    var oraDurataLavoro = parseInt(dateToSaveClone[cc].durata.split(".")[0]);
                    var minutiDurataLavoro = parseInt(dateToSaveClone[cc].durata.split(".")[1]);
                    if (!minutiDurataLavoro) {
                        minutiDurataLavoro = 0;
                    };
                    var durataLavoro = parseFloat(oraDurataLavoro + "." + minutiDurataLavoro);
                    var orarioFineRapportino = orarioInizioRapportino + durataLavoro;
                    orarioInizioRapportino = orarioInizioRapportino.toString().replace(".", ":");
                    if (orarioInizioRapportino.split(":")[1]) {
                        orarioInizioRapportino = orarioInizioRapportino.split(":")[0] + ":30:00";
                    } else {
                        orarioInizioRapportino = orarioInizioRapportino.split(":")[0] + ":00:00";
                    };
                    orarioFineRapportino = orarioFineRapportino.toString().replace(".", ":");
                    if (orarioFineRapportino.split(":")[1]) {
                        orarioFineRapportino = orarioFineRapportino.split(":")[0] + ":30:00";
                    } else {
                        orarioFineRapportino = orarioFineRapportino.split(":")[0] + ":00:00";
                    };
                    dateToSaveClone[cc].orarioInizio = orarioInizioRapportino;
                    dateToSaveClone[cc].orarioFine = orarioFineRapportino;
                    validateCommessa = true;
                };

                if (validateCommessa) {
                    if (dataToSync) {
                        dataToSync.push(_.cloneDeep(dateToSaveClone[cc]));
                    } else {
                        dataToSync = [];
                        dataToSync.push(dateToSaveClone[cc]);
                    };

                    if (this.trovateSpese.length !== 0 && h === (this.oData.selectedDates.length - 1)) {
                        if (this.periodoSbagliato === true) {
                            this.daFarComparireMessageToast = true;
                        };

                        var projectSet = utils.LocalStorage.getItem("projectSet");
                        var obj = _.find(projectSet, {
                            'commessa': dataToSave[cc].commessa
                        });
                        this.commessaPerNotaSpesa = obj.descrizione;

                        var wbsSet = utils.LocalStorage.getItem("commessaSet").dati;
                        var obj = _.find(wbsSet, {
                            'elementoWBSID': dataToSave[cc].attivita
                        });
                        this.attivitaPerNotaSpesa = obj.descrizione;

                        var testo = "'" + this.commessaPerNotaSpesa + " - " + this.attivitaPerNotaSpesa + "' " + this._getLocaleText("domandaCopiaNote");

                        that = this;
                        var dialog = new sap.m.Dialog({
                            title: this._getLocaleText("Confirm"),
                            type: 'Message',
                            content: new sap.m.Text({
                                text: testo
                            }),
                            beginButton: new sap.m.Button({
                                text: 'SI',
                                press: function () {
                                    that._duplicaNoteSpese();
                                    dialog.close();
                                }
                            }),
                            endButton: new sap.m.Button({
                                text: 'NO',
                                press: function () {
                                    that._removeSymbolDollar();
                                    dialog.close();
                                    if (that.periodoSbagliato === true) {
                                        sap.m.MessageToast.show(that._getLocaleText("periodoSbagliato"), {
                                            duration: 2000
                                        });
                                        that.periodoSbagliato = false;
                                    }
                                }
                            }),
                            afterClose: function () {
                                dialog.destroy();
                            }
                        });
                        dialog.open();
                    };
                };
            };
        };
        utils.LocalStorage.setItem("attivitaSet", dataToSync);

        var dati = this.datiModel.getData().dati;
        for (var i = 0; i < dataToSync.length; i++) {
            for (var j = 0; j < dataToSync.length; j++) {
                var obj = _.find(this.datiModel.getData().dati, {
                    'dataCommessa': dataToSync[j].dataCommessa,
                    'commessa': dataToSync[j].commessa
                });
                if (!obj) {
                    dati.push(dataToSync[j]);
                };
            };
        };
        this.enableModel.setProperty("/duplicateEnable", false);
        this.enableModel.setProperty("/deleteEnable", false);
        this.enableModel.setProperty("/unReleaseEnable", false);
        var LS = utils.LocalStorage.getItem("attivitaSet");
        var trovatoAlmenoUno = false;
        if (LS.length !== 0) {
            var contoStato = 0;
            var contoDelete = 0;
            for (var g = 0; g < LS.length; g++) {
                if (LS[g].stato === "10") {
                    contoStato = contoStato + 1;
                }
                if (LS[g].delete === "X") {
                    contoDelete = contoDelete + 1;
                };
            };
            if (contoStato > contoDelete) {
                trovatoAlmenoUno = true;
            } else {
                trovatoAlmenoUno = false;
            };
        };
        var annoAttualeText = document.getElementById("calendarId--calendar--Head-B2").textContent;
        var annoReale = (new Date().getFullYear()).toString();
        var periodoAttivita = utils.LocalStorage.getItem("perioSet").AttivitaChiusa;
        var meseAttText = document.getElementById("calendarId--calendar--Head-B1").textContent;
        var meseAttuale = parseInt(utils.Util._getMeseAttuale(meseAttText, annoAttualeText).mese);
        var meseReale = (new Date().getMonth()) + 1;
        if (trovatoAlmenoUno === false || LS.length === 0 || periodoAttivita === "X" || meseAttuale !== meseReale || annoAttualeText !== annoReale) {
            this.enableModel.setProperty("/deleteEnableMulti", false);
        } else {
            this.enableModel.setProperty("/deleteEnableMulti", true);
        };
        this.calcolaOreLavorate();
        this.enableModel.refresh();
        if (this.periodoSbagliato === true && this.daFarComparireMessageToast !== true) {
            sap.m.MessageToast.show(this._getLocaleText("periodoSbagliato"), {
                duration: 2000
            });
            this.periodoSbagliato = false;
            this.daFarComparireMessageToast = false;
        };
    },

    _confirmRelease: function () {
        for (var i = 0; i < this.oData.selectedDates.length; i++) {
            var activityToRelease = _.where(this._activityToRelease, {
                'dataCommessa': this.oData.selectedDates[i]
            });

            var dataToSync = this.datiModel.getData().dati;

            for (var r = 0; r < activityToRelease.length; r++) {
                if (activityToRelease[r].counter !== "") {
                    _.remove(this.datiModel.getData().dati, {
                        'counter': activityToRelease[r].counter
                    });
                } else {
                    _.remove(this.datiModel.getData().dati, {
                        'counterFiori': activityToRelease[r].counterFiori
                    });
                };
                activityToRelease[r].stato = "20";
                var dataUltimaModifica = new Date();
                dataUltimaModifica = dataUltimaModifica.getDate() + "/" + parseInt(dataUltimaModifica.getMonth() + 1) + "/" + +dataUltimaModifica.getFullYear();
                var oraUltimaModifica = new Date();
                oraUltimaModifica = oraUltimaModifica.getHours() + ":" + oraUltimaModifica.getMinutes() + ":" + oraUltimaModifica.getSeconds();
                activityToRelease[r].dataUltimaModifica = dataUltimaModifica;
                activityToRelease[r].oraUltimaModifica = oraUltimaModifica;
                highest = 0;
                for (var y = 0; y < dataToSync.length; y++) {
                    if (parseInt(dataToSync[y].counterFiori) > highest) {
                        highest = parseInt(dataToSync[y].counterFiori);
                    };
                };
                activityToRelease[r].counterFiori = (highest + 1).toString();

                this.datiModel.getData().dati.push(activityToRelease[r]);
            };
            utils.LocalStorage.setItem("attivitaSet", this.datiModel.getData().dati);
        };
        var LS = utils.LocalStorage.getItem("attivitaSet");
        var trovatoAlmenoUno = false;
        if (LS.length !== 0) {
            var contoStato = 0;
            var contoDelete = 0;
            for (var g = 0; g < LS.length; g++) {
                if (LS[g].stato === "10") {
                    contoStato = contoStato + 1;
                }
                if (LS[g].delete === "X") {
                    contoDelete = contoDelete + 1;
                };
            };
            if (contoStato > contoDelete) {
                trovatoAlmenoUno = true;
            } else {
                trovatoAlmenoUno = false;
            };
        };
        var annoAttualeText = document.getElementById("calendarId--calendar--Head-B2").textContent;
        var annoReale = (new Date().getFullYear()).toString();
        var periodoAttivita = utils.LocalStorage.getItem("perioSet").AttivitaChiusa;
        var meseAttText = document.getElementById("calendarId--calendar--Head-B1").textContent;
        var meseAttuale = parseInt(utils.Util._getMeseAttuale(meseAttText, annoAttualeText).mese);
        var meseReale = (new Date().getMonth()) + 1;
        if (trovatoAlmenoUno === false || LS.length === 0 || periodoAttivita === "X" || meseAttuale !== meseReale || annoAttualeText !== annoReale) {
            this.enableModel.setProperty("/deleteEnableMulti", false);
        } else {
            this.enableModel.setProperty("/deleteEnableMulti", true);
        };
    },

    _removeSymbolDollar: function () {
        this.dateToSaveClone.existNote = 0;
        this.updateTable(this.selectedDate);
        this._setDayFull();
    },

    _duplicaNoteSpese: function () {
        for (var h = 0; h < this.oData.selectedDates.length; h++) {
            for (var cc = 0; cc < this.trovateSpese.length; cc++) {
                var trovateSpeseClone = _.cloneDeep(this.trovateSpese);
                /* DA TESTARE */
                var noteSpeseLS = utils.LocalStorage.getItem("notaSpeseSet");
                highest = 0;
                for (var y = 0; y < noteSpeseLS.length; y++) {
                    if (parseInt(noteSpeseLS[y].counterFiori) > highest) {
                        highest = parseInt(noteSpeseLS[y].counterFiori);
                    };
                };
                trovateSpeseClone[cc].counterFiori = (highest + 1).toString();
                trovateSpeseClone[cc].numProgressivoGiustificativo = "";
                trovateSpeseClone[cc].numeroGiustificativo = "";
                /******************/
                trovateSpeseClone[cc].dataNotaSpese = this.oData.selectedDates[h];


                var dataUltimaModifica = new Date();
                dataUltimaModifica = dataUltimaModifica.getDate() + "/" + parseInt(dataUltimaModifica.getMonth() + 1) + "/" + +dataUltimaModifica.getFullYear();
                var oraUltimaModifica = new Date();
                oraUltimaModifica = oraUltimaModifica.getHours() + ":" + oraUltimaModifica.getMinutes() + ":" + oraUltimaModifica.getSeconds();


                trovateSpeseClone[cc].oraUltimaModifica = oraUltimaModifica;
                trovateSpeseClone[cc].dataUltimaModifica = dataUltimaModifica;
                trovateSpeseClone[cc].numProgressivoGiustificativo = "";
                trovateSpeseClone[cc].numeroGiustificativo = "";
                trovateSpeseClone[cc].numeroPeriodoTrasferta = "";
                trovateSpeseClone[cc].numeroProgressivoAttCost = "";
                trovateSpeseClone[cc].numeroTrasferta = "";
                this.notaSpeseSet.push(_.cloneDeep(trovateSpeseClone[cc]));
                if (h === (this.oData.selectedDates.length - 1)) {
                    sap.m.MessageToast.show(this._getLocaleText("notaAggiuntaPerCommessa") + " '" + this.commessaPerNotaSpesa + " - " + this.attivitaPerNotaSpesa + "'");
                };
                utils.LocalStorage.setItem("notaSpeseSet", this.notaSpeseSet);
                this.modelloNotaSpese.setProperty("/arrayNotaSpese", []);
                for (var i = 0; i < this.notaSpeseSet.length; i++) {
                    if (this.notaSpeseSet[i].dataNotaSpese === this.dataNotaSpese && this.notaSpeseSet[i].commessa === this.commessa) {
                        this.modelloNotaSpese.getProperty("/arrayNotaSpese").push(this.notaSpeseSet[i]);
                    };
                };
                this.modelloNotaSpese.refresh();
            };
        };
        this.updateTable(this.selectedDate);
        this._setDayFull();
        if (this.periodoSbagliato === true && this.daFarComparireMessageToast === true) {
            sap.m.MessageToast.show(this._getLocaleText("periodoSbagliato"), {
                duration: 2000
            });
            this.periodoSbagliato = false;
            this.daFarComparireMessageToast = false;
        };
    },

    calcolaNumeroAttivitaNoteSpese: function () {
        var rapportini = utils.LocalStorage.getItem("attivitaSet");
        var noteSpese = utils.LocalStorage.getItem("notaSpeseSet");
        var attivitaDaRilasciare = [];
        var attivitaDaRilasciare = _.where(rapportini, {
            'stato': "10",
            'delete': ""
        });
        var attivitaDaSincronizzare = [];
        for (var i = 0; i < rapportini.length; i++) {
            if ((rapportini[i].counterFiori && rapportini[i].delete !== "X") || (rapportini[i].counter && rapportini[i].delete === "X")) {
                attivitaDaSincronizzare.push(rapportini[i]);
            };
        };
        var noteSpeseDaSincronizzare = [];
        for (var i = 0; i < noteSpese.length; i++) {
            if ((noteSpese[i].counterFiori && noteSpese[i].delete !== "X") || (noteSpese[i].numProgressivoGiustificativo && noteSpese[i].delete === "X")) {
                noteSpeseDaSincronizzare.push(noteSpese[i]);
            };
        };
        this.nAttDaRil = attivitaDaRilasciare.length;
        this.nAttDaSinc = attivitaDaSincronizzare.length;
        this.nNoteSpeseDaSinc = noteSpeseDaSincronizzare.length;
    },

    handleMessagePopoverPress: function (oEvent) {
        this.calcolaNumeroAttivitaNoteSpese();
        var oggetti = [];
        oggetti.push({
            title: 'Attivita da rilasciare: ',
            type: 'Success',
            description: this.nAttDaRil
        });
        oggetti.push({
            title: 'Oggetti da sincronizzare: ',
            type: 'Information',
            description: "Attività: " + this.nAttDaSinc + " - Note Spese: " + this.nNoteSpeseDaSinc
        });
        this.oggettiModel.setProperty("/oggetti", oggetti);

        var oMessageTemplate = new sap.m.MessagePopoverItem({
            type: '{type}',
            title: '{title}',
            description: '{description}'
        });
        var oMessagePopover1 = new sap.m.MessagePopover({
            items: {
                path: '/oggetti',
                template: oMessageTemplate
            }
        });
        oMessagePopover1.setModel(this.oggettiModel);
        oMessagePopover1.openBy(oEvent.getSource());
    },

    onPressLogOut: function () {
        this.sincronizzare = undefined;
        this.calcolaNumeroAttivitaNoteSpese();
        if ((this.nAttDaRil || this.nAttDaSinc || this.nNoteSpeseDaSinc) && Offline.state === "up") {
            if (this.nAttDaRil) {
                var testoAttDaRil = this.nAttDaRil + " " + this._getLocaleText("attDaRil");
                if (this.nAttDaSinc) {
                    var testoAttDaSinc = " - " + this.nAttDaSinc + " " + this._getLocaleText("nAttDaSinc");
                } else {
                    var testoAttDaSinc = "";
                };
                if (this.nNoteSpeseDaSinc) {
                    var testoNoteSpeseDaSinc = " - " + this.nNoteSpeseDaSinc + " " + this._getLocaleText("nNoteSpeseDaSinc");
                } else {
                    var testoNoteSpeseDaSinc = "";
                };
            } else if (this.nAttDaSinc) {
                var testoAttDaRil = "";
                var testoAttDaSinc = this.nAttDaSinc + " " + this._getLocaleText("nAttDaSinc");
                if (this.nNoteSpeseDaSinc) {
                    var testoNoteSpeseDaSinc = " - " + this.nNoteSpeseDaSinc + " " + this._getLocaleText("nNoteSpeseDaSinc");
                } else {
                    var testoNoteSpeseDaSinc = "";
                };
            } else {
                var testoAttDaRil = "";
                var testoAttDaSinc = "";
                var testoNoteSpeseDaSinc = this.nNoteSpeseDaSinc + " " + this._getLocaleText("nNoteSpeseDaSinc");
            };

            if (this.nAttDaSinc === 0 && this.nNoteSpeseDaSinc === 0) {
                sap.m.MessageBox.show(this._getLocaleText("avvisoLogOut") + " " +
                    testoAttDaRil + testoAttDaSinc + testoNoteSpeseDaSinc + this._getLocaleText("confirmLogoutSenzaSincronizzazione"),
                    sap.m.MessageBox.Icon.NONE,
                    this._getLocaleText("confirmLogoutTitle"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
                    _.bind(this.finireLogout, this)
                );
            } else {
                this.sincronizzare = true;
                sap.m.MessageBox.show(this._getLocaleText("avvisoLogOut") + " " +
                    testoAttDaRil + testoAttDaSinc + testoNoteSpeseDaSinc + this._getLocaleText("confirmLogoutConSincronizzazione"),
                    sap.m.MessageBox.Icon.NONE,
                    this._getLocaleText("confirmLogoutTitle"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
                    _.bind(this.finireLogout, this)
                );
            };
        } else {
            var meseAnnoAttuale = new Date();
            var annoAttuale = meseAnnoAttuale.getFullYear();
            var mese = meseAnnoAttuale.getMonth() + 1;
            var dataFine = utils.Util.getLastDayFromMonth(parseInt(annoAttuale), parseInt(mese));
            var dataInizio = utils.Util.getFirstDayFromMonth(parseInt(annoAttuale), parseInt(mese) - 1);
            var meseAttText = document.getElementById("calendarId--calendar--Head-B1").textContent;
            var annoAttualeText = document.getElementById("calendarId--calendar--Head-B2").textContent;
            var meseAttuale = parseInt(utils.Util._getMeseAttuale(meseAttText, annoAttualeText).mese);
            this._doLogout();
        };
    },

    finireLogout: function (evt) {
        if (evt === sap.m.MessageBox.Action.YES) {
            var meseAnnoAttuale = new Date();
            var annoAttuale = meseAnnoAttuale.getFullYear();
            var mese = meseAnnoAttuale.getMonth() + 1;
            var dataFine = utils.Util.getLastDayFromMonth(parseInt(annoAttuale), parseInt(mese));
            var dataInizio = utils.Util.getFirstDayFromMonth(parseInt(annoAttuale), parseInt(mese) - 1);
            var meseAttText = document.getElementById("calendarId--calendar--Head-B1").textContent;
            var annoAttualeText = document.getElementById("calendarId--calendar--Head-B2").textContent;
            var meseAttuale = parseInt(utils.Util._getMeseAttuale(meseAttText, annoAttualeText).mese);
            if (meseAttuale !== new Date().getMonth() + 1 || parseInt(annoAttualeText) !== new Date().getFullYear()) {
                this.getView().getModel("appModel").setProperty("/nascondiSincroButton", true);
            } else {
                this.getView().getModel("appModel").setProperty("/nascondiSincroButton", undefined);
            };
            Offline.check();
            if (this.sincronizzare === true) {
                this.onSynchroPress();
            };
            this._doLogout();
        } else if (evt === sap.m.MessageBox.Action.NO && this.nAttDaSinc === 0 && this.nNoteSpeseDaSinc === 0) {
            this.sincronizzare = undefined;
        } else if (evt === sap.m.MessageBox.Action.NO) {
            this.sincronizzare = undefined;
            this._doLogout();
        };
    },

    _doLogout: function () {
        //resetto dati appModel
        this.getView().getModel("appModel").setProperty("/isLogged", false);
        this.getView().getModel("appModel").setProperty("/entity", undefined);
        this.getView().getModel("appModel").setProperty("/username", "");
        this.getView().getModel("appModel").setProperty("/password", "");
        this.getView().getModel("appModel").setProperty("/ID", "");
        this.getView().getModel("appModel").setProperty("/CID", "");
        var oCalendar = this.getView().byId("calendar");
        oCalendar.removeAllSelectedDates();
        clearInterval(this.mySetInterval);
        this.passatoInOnAfterRendering = false;
        if (!this.sincronizzare === true) {
            sessionStorage.clear();
            this.router.navTo("");
            this.getView().getModel("appModel").setProperty("/arrivatoDaAltrePagine", false);
        };
    },

    handleStethoscopePress: function (evt) {
        var path = evt.getSource().oPropagatedProperties.oBindingContexts.table.getPath().split("/")[2];
        path = path.substr(path.length - 1);
        var modelThisDate = this.tableModel.getData().dati;
        var dataPattern = modelThisDate[path].dataCommessa.split("/").join("");
        clearInterval(this.mySetInterval);
        this.passatoInOnAfterRendering = false;

        if (modelThisDate[path].counterFiori) {
            var objClone = _.clone(modelThisDate[path]);
            objClone.counter = modelThisDate[path].counterFiori;
        } else {
            var objClone = _.clone(modelThisDate[path]);
            objClone.counter = modelThisDate[path].counter;
        };
        this.router.navTo("notaSpese", {
            data: dataPattern,
            commessa: objClone.commessa,
            attivita: objClone.attivita
        });
    },

    onElaboratePress: function () {
        if (Offline.state === "up") {
            this.dataToElaborate = [];
            this.dataElaborated = [];
            var dataToElaborate = _.clone(this.obj);
            var dataGiornaliera = dataToElaborate[0].dataCommessa;
            this.TimeSheet = [];
            this.TravelSet = [];
            var dataUltimaModifica = new Date();
            dataUltimaModifica = dataUltimaModifica.getDate() + "/" + parseInt(dataUltimaModifica.getMonth() + 1) + "/" + dataUltimaModifica.getFullYear();
            var oraUltimaModifica = new Date();
            oraUltimaModifica = oraUltimaModifica.getHours() + ":" + oraUltimaModifica.getMinutes() + ":" + oraUltimaModifica.getSeconds();

            for (var j = 0; j < dataToElaborate.length; j++) {
                this.rapportini = utils.LocalStorage.getItem("attivitaSet");
                dataToElaborate[j].dataUltimaModifica = dataUltimaModifica;
                dataToElaborate[j].oraUltimaModifica = oraUltimaModifica;
                highest = 0;
                for (var y = 0; y < this.rapportini.length; y++) {
                    if (parseInt(this.rapportini[y].counterFiori) > highest) {
                        highest = parseInt(this.rapportini[y].counterFiori);
                    };
                };
                dataToElaborate[j].counterFiori = (highest + 1).toString();
                this.dataToElaborate.push(dataToElaborate[j]);
                dataToElaborate[j].stato = "10";
                this.dataElaborated.push(dataToElaborate[j]);
                this.TimeSheet.push(model.odata.serializer.attivitaEntity.toSap(dataToElaborate[j]));
            };

            var cidSet = {};
            var cidElement = utils.LocalStorage.getItem("cidSet")[0];
            var datiUtenteLS = $.parseJSON(atob(localStorage.getItem("user")));
            for (var i = 0; i < datiUtenteLS.length; i++) {
                if (datiUtenteLS[i].id === sessionStorage.getItem("cid")) {
                    this.password = datiUtenteLS[i].p;
                    break;
                };
            };
            cidElement.password = this.password;
            var user = model.odata.serializer.cidEntity.toSap(cidElement);
            cidElement = {};
            cidElement = user;
            cidElement.TravelSet = this.TravelSet;
            cidElement.TimesheetSet = this.TimeSheet;
            cidSet.cidElement = cidElement;
            //// scrittura su sap //////
            var that = this;
            this.oDialog.open();
            this.getOdataModel();
            this.modelloOdata = this.getView().getModel().createEntry("CIDSet", cidSet.cidElement);
            this._odataModel.submitChanges(function () {
                that._odataCallAttivitaSetRefreshForElaborate(sessionStorage.getItem("cid"), that);

                var dataReale = new Date();
                var meseReale = (dataReale.getMonth() + 1).toString();
                if (meseReale.length < 2) {
                    meseReale = "0" + meseReale;
                };
                var annoReale = dataReale.getFullYear().toString();
                var annoAttuale = document.getElementById("calendarId--calendar--Head-B2").textContent;
                var meseAttuale = document.getElementById("calendarId--calendar--Head-B1").textContent;
                var result = utils.Util._getMeseAttuale(meseAttuale, annoAttuale);
                meseAttuale = result.mese;
                that.enableModel.setProperty("/unReleaseEnable", false);
                that.enableModel.setProperty("/duplicateEnable", false);
                if (meseAttuale === meseReale && annoAttuale === annoReale) {
                    that.enableModel.setProperty("/releaseEnable", true);
                    that.enableModel.setProperty("/deleteEnableMulti", true);
                } else {
                    that.enableModel.setProperty("/releaseEnable", false);
                    that.enableModel.setProperty("/deleteEnableMulti", false);
                };

            }, function (err) {
                sap.m.MessageBox.error(JSON.parse(err.response.body).error.message.value, {
                    title: that._getLocaleText("WARNING")
                });
                that.oDialog.close();
            });
        } else {
            var that = this;
            sap.m.MessageBox.alert(that._getLocaleText("noConnessione"), {
                title: that._getLocaleText("WARNING")
            });
        };
    },

    //    onDeletePress: function () {
    //        sap.m.MessageBox.show(this._getLocaleText("confirmDelete"),
    //            sap.m.MessageBox.Icon.NONE,
    //            this._getLocaleText("confirmDeleteTitle"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
    //            _.bind(this.deleteAppointment, this)
    //        );
    //    },
    //
    //    deleteAppointment: function (evt) {
    //        var appointmentsTable = this.getView().byId("idTable");
    //        var selectedIndexAppointments = appointmentsTable.getSelectedContextPaths();
    //        for (var i = 0; i < selectedIndexAppointments.length; i++) {
    //            var index = parseInt(selectedIndexAppointments[i].split("/")[2]);
    //            var appointmentToDelete = this.tableModel.getData().dati[index];
    //            _.remove(this.appointments, {
    //                'counterFiori': appointmentToDelete.counterFiori
    //            });
    //            var data = appointmentToDelete.dataEvento;
    //        };
    //        model.persistence.Storage.local.save(this.username + "-appointments", this.appointments);
    ////        localStorage.setItem(this.username + "-appointments", this.appointments);
    //        this.updateTable(data);
    //        this._setDayFull();
    //        appointmentsTable.removeSelections();
    //        this.periodoSbagliato = false;
    //        if (evt === sap.m.MessageBox.Action.YES) {
    //            this.c.getSelectedItem();
    //            var dataX = "inDate";
    //            var oRefDate = new Date();
    //
    //            attivita = _.clone(utils.LocalStorage.getItem("attivitaSet"));
    //            notaSpese = _.clone(utils.LocalStorage.getItem("notaSpeseSet"));
    //            var dataToDelete = _.clone(this.obj);
    //            var dataGiornaliera = dataToDelete[0].dataCommessa;
    //            var attivitaGiornaliere = [];
    //
    //            ///////////// ELIMINO TUTTE LE ATTIVITA GIORNALIERE CHE POI AGGIUNGERO' CON LE OPPORTUNE MODIFICHE FATTE /////////////
    //            for (var i = 0; i < dataToDelete.length; i++) {
    //                this.max = 0;
    //                for (var j = 0; j < attivita.length; j++) {
    //                    if (parseInt(attivita[j].counterFiori) > this.max) {
    //                        this.max = parseInt(attivita[j].counterFiori);
    //                    };
    //                };
    //                if (dataToDelete[i].counter !== "") {
    //                    _.remove(attivita, {
    //                        'counter': dataToDelete[i].counter
    //                    });
    //                } else {
    //                    _.remove(attivita, {
    //                        'counterFiori': dataToDelete[i].counterFiori
    //                    });
    //                };
    //                dataToDelete[i].delete = "X";
    //                if (!dataToDelete[i].counterFiori) {
    //                    dataToDelete[i].counterFiori = (this.max + 1).toString();
    //                };
    //                dataToDelete[i].counter = dataToDelete[i].counter;
    //                var dataUltimaModifica = new Date();
    //                dataUltimaModifica = dataUltimaModifica.getDate() + "/" + parseInt(dataUltimaModifica.getMonth() + 1) + "/" + +dataUltimaModifica.getFullYear();
    //                var oraUltimaModifica = new Date();
    //                oraUltimaModifica = oraUltimaModifica.getHours() + ":" + oraUltimaModifica.getMinutes() + ":" + oraUltimaModifica.getSeconds();
    //                dataToDelete[i].dataUltimaModifica = dataUltimaModifica;
    //                dataToDelete[i].oraUltimaModifica = oraUltimaModifica;
    //
    //                // guardo se c'è un rapportino in un mese vecchio
    //                var meseAttuale = (new Date().getMonth() + 1).toString();
    //                var annoAttuale = (new Date().getFullYear()).toString();
    //                if (meseAttuale.length < 2) {
    //                    meseAttuale = "0" + meseAttuale;
    //                };
    //                var meseRapportino = dataToDelete[i].dataCommessa.split("/")[1];
    //                var annoRapportino = dataToDelete[i].dataCommessa.split("/")[2];
    //                if (meseRapportino !== meseAttuale || annoRapportino !== annoAttuale) {
    //                    this.periodoSbagliato = true;
    //                };
    //
    //                attivita.push(dataToDelete[i]);
    //                attivitaGiornaliere.push(dataToDelete[i]);
    //                oRefDate.setDate(1);
    //                oRefDate.setMonth(parseInt(dataToDelete[i].dataCommessa.split("/")[1]) - 1);
    //                oRefDate.setYear(parseInt(dataToDelete[i].dataCommessa.split("/")[2]));
    //                document.getElementById("calendarId--calendar--Month0-" + utils.Util.dateToString(oRefDate.setDate(dataToDelete[i].dataCommessa.slice(0, 2)), dataX)).style.boxShadow = "none";
    //            };
    //
    //            ///////////// COMINCIO A CAPIRE QUALI NOTESPESE VANNO CANCELLATE IN BASE ALLE WBS DA ELIMINARE //////////////////////
    //            var cloneAttivitaGiornaliere = _.clone(attivitaGiornaliere);
    //            this.progettiDaEliminare = [];
    //            var attivitaDaEliminare = _.remove(cloneAttivitaGiornaliere, {
    //                'delete': "X"
    //            });
    //            for (var i = 0; i < attivitaDaEliminare.length; i++) {
    //                this.progettiDaEliminare.push({
    //                    'attivita': attivitaDaEliminare[i].attivita
    //                });
    //            };
    //            var attivitaDaTenere = cloneAttivitaGiornaliere;
    //            this.progettiDaTenere = [];
    //            for (var i = 0; i < attivitaDaTenere.length; i++) {
    //                this.progettiDaTenere.push({
    //                    'attivita': attivitaDaTenere[i].attivita
    //                });
    //            };
    //
    //            ///////////// TENGO SOLO LE WBS DA CANCELLARE CHE NON SONO PRESENTI ANCHE COME WBS DA TENERE ////////////////////////
    //            for (var i = 0; i < this.progettiDaTenere.length; i++) {
    //                for (var j = 0; j < _.clone(this.progettiDaEliminare).length; j++) {
    //                    if (
    //                        this.progettiDaTenere[i].attivita === this.progettiDaEliminare[j].attivita) {
    //                        _.remove(progettiDaEliminare, {
    //                            'attivita': this.progettiDaEliminare[j].attivita
    //                        });
    //                        break;
    //                    };
    //                };
    //            };
    //
    //            this.progettiDaEliminare = _.uniq(this.progettiDaEliminare); //////non voglio wbs duplicate
    //
    //            ///////////// METTO LA PROPRIETA DELETE = "X" A TUTTE LE NOTESPESE CON LE WBS DA CANCELLARE ///////////////////////
    //            for (var j = 0; j < this.progettiDaEliminare.length; j++) {
    //                var obj = _.find(attivita, {
    //                    'dataCommessa': dataGiornaliera,
    //                    'delete': "",
    //                    'attivita': this.progettiDaEliminare[j].attivita
    //                });
    //                if (!obj) {
    //                    var max = 0;
    //                    for (var k = 0; k < notaSpese.length; k++) {
    //                        if (notaSpese[k]) {
    //                            if (notaSpese[k].counterFiori) {
    //                                if (parseInt(notaSpese[k].counterFiori) > max) {
    //                                    max = parseInt(notaSpese[k].counterFiori);
    //                                };
    //                            };
    //                        };
    //                    };
    //                    var noteSpeseDaEliminare = _.remove(notaSpese, {
    //                        'dataNotaSpese': dataGiornaliera,
    //                        'attivita': this.progettiDaEliminare[j].attivita
    //                    });
    //                    for (var i = 0; i < noteSpeseDaEliminare.length; i++) {
    //                        noteSpeseDaEliminare[i].dataUltimaModifica = dataUltimaModifica;
    //                        noteSpeseDaEliminare[i].oraUltimaModifica = oraUltimaModifica;
    //                        noteSpeseDaEliminare[i].delete = "X";
    //                        noteSpeseDaEliminare[i].counterFiori = (max + 1).toString();
    //                        notaSpese.push(noteSpeseDaEliminare[i]);
    //                    };
    //                };
    //            };
    //            utils.LocalStorage.setItem("attivitaSet", attivita);
    //            utils.LocalStorage.setItem("notaSpeseSet", notaSpese);
    //            this.datiModel.setProperty("/dati", attivita);
    //            this.datiModel.refresh();
    //            this.enableModel.setProperty("/duplicateEnable", false);
    //            this.enableModel.setProperty("/deleteEnable", false);
    //            this.enableModel.setProperty("/unReleaseEnable", false);
    //            this.updateTable(dataGiornaliera);
    //            this._setDayFull();
    //
    //            var LS = utils.LocalStorage.getItem("attivitaSet");
    //            var trovatoAlmenoUno = false;
    //            if (LS.length !== 0) {
    //                var contoStato = 0;
    //                var contoDelete = 0;
    //                var meseAttuale = (new Date().getMonth() + 1).toString();
    //                var annoAttuale = (new Date().getFullYear()).toString();
    //                if (meseAttuale.length === 1) {
    //                    meseAttuale = "0" + meseAttuale;
    //                };
    //                for (var g = 0; g < LS.length; g++) {
    //                    if (LS[g].stato === "10" && LS[g].dataCommessa.split("/")[1] === meseAttuale && LS[g].dataCommessa.split("/")[2] === annoAttuale) {
    //                        contoStato = contoStato + 1;
    //                    };
    //                    if (LS[g].delete === "X" && LS[g].dataCommessa.split("/")[1] === meseAttuale && LS[g].dataCommessa.split("/")[2] === annoAttuale) {
    //                        contoDelete = contoDelete + 1;
    //                    };
    //                };
    //                if (contoStato > contoDelete) {
    //                    trovatoAlmenoUno = true;
    //                } else {
    //                    trovatoAlmenoUno = false;
    //                };
    //            };
    //            if (trovatoAlmenoUno === false || LS.length === 0) {
    //                this.enableModel.setProperty("/deleteEnableMulti", false);
    //            } else {
    //                this.enableModel.setProperty("/deleteEnableMulti", true);
    //            };
    //            this.calcolaOreLavorate();
    //            this.enableModel.refresh();
    //        };
    //        if (this.periodoSbagliato === true) {
    //            sap.m.MessageToast.show(this._getLocaleText("periodoSbagliato"), {
    //                duration: 2000
    //            });
    //            this.periodoSbagliato = false;
    //        };
    //    },

    calcolaOreLavorate: function () {
        var rapportiniLocalStorage = utils.LocalStorage.getItem("attivitaSet");
        var oreTotaliLavorate = 0;
        for (var b = 0; b < rapportiniLocalStorage.length; b++) {
            if (rapportiniLocalStorage[b].delete !== "X" && rapportiniLocalStorage[b].dataCommessa === this.enableModel.getProperty("/giornoSelezionato")) {
                rapportiniLocalStorage[b].durata.replace(",", ".");
                oreTotaliLavorate = oreTotaliLavorate + parseFloat(rapportiniLocalStorage[b].durata);
            };
        };
        if (oreTotaliLavorate.toString().split(".")[1]) {
            oreTotaliLavorate = oreTotaliLavorate.toFixed(2);
        } else {
            oreTotaliLavorate = oreTotaliLavorate + ".00";
        };
        this.enableModel.setProperty("/oreLavorateGiornoSelezionato", oreTotaliLavorate);
    },

    _findActivityToRelease: function () {
        var activity = this.datiModel.getData().dati;
        var attivitaTrovate = _.where(activity, {
            'stato': "10",
            'delete': ""
        });
        var meseAtt = document.getElementById("calendarId--calendar--Head-B1").textContent;
        var annoAttuale = document.getElementById("calendarId--calendar--Head-B2").textContent;
        var meseAttuale = parseInt(utils.Util._getMeseAttuale(meseAtt, annoAttuale).mese);
        var meseAttualeStringa = meseAttuale.toString();
        if (meseAttualeStringa.length === 1) {
            meseAttualeStringa = "0" + meseAttualeStringa;
        };
        var dataOggi = new Date();
        var meseOggi = dataOggi.getMonth() + 1;
        for (var t = 0; t < attivitaTrovate.length; t++) {
            if (attivitaTrovate[t].dataCommessa.split("/")[1] === meseAttualeStringa && attivitaTrovate[t].dataCommessa.split("/")[2] === annoAttuale) {
                var objDaRilasciare = attivitaTrovate[t];
                break;
            };
        };
        var periodoAttivita = utils.LocalStorage.getItem("perioSet").AttivitaChiusa;
        if (objDaRilasciare && meseOggi === parseInt(meseAttuale) && periodoAttivita === "") {
            this.enableModel.setProperty("/releaseEnable", true);
        } else {
            this.enableModel.setProperty("/releaseEnable", false);
        };
    },

    onReleaseSinglePress: function () {
        if (Offline.state === "up") {
            this.dataToElaborate = [];
            this.dataElaborated = [];
            var dataToElaborate = _.clone(this.obj);
            var dataGiornaliera = dataToElaborate[0].dataCommessa;
            this.TimeSheet = [];
            this.TravelSet = [];
            var dataUltimaModifica = new Date();
            dataUltimaModifica = dataUltimaModifica.getDate() + "/" + parseInt(dataUltimaModifica.getMonth() + 1) + "/" + dataUltimaModifica.getFullYear();
            var oraUltimaModifica = new Date();
            oraUltimaModifica = oraUltimaModifica.getHours() + ":" + oraUltimaModifica.getMinutes() + ":" + oraUltimaModifica.getSeconds();

            for (var j = 0; j < dataToElaborate.length; j++) {
                this.rapportini = utils.LocalStorage.getItem("attivitaSet");
                dataToElaborate[j].dataUltimaModifica = dataUltimaModifica;
                dataToElaborate[j].oraUltimaModifica = oraUltimaModifica;
                highest = 0;
                for (var y = 0; y < this.rapportini.length; y++) {
                    if (parseInt(this.rapportini[y].counterFiori) > highest) {
                        highest = parseInt(this.rapportini[y].counterFiori);
                    };
                };
                dataToElaborate[j].counterFiori = (highest + 1).toString();
                this.dataToElaborate.push(dataToElaborate[j]);
                dataToElaborate[j].stato = "20";
                this.dataElaborated.push(dataToElaborate[j]);
                this.TimeSheet.push(model.odata.serializer.attivitaEntity.toSap(dataToElaborate[j]));
            };

            var cidSet = {};
            var cidElement = utils.LocalStorage.getItem("cidSet")[0];
            var datiUtenteLS = $.parseJSON(atob(localStorage.getItem("user")));
            for (var i = 0; i < datiUtenteLS.length; i++) {
                if (datiUtenteLS[i].id === sessionStorage.getItem("cid")) {
                    this.password = datiUtenteLS[i].p;
                    break;
                };
            };
            cidElement.password = this.password;
            var user = model.odata.serializer.cidEntity.toSap(cidElement);
            cidElement = {};
            cidElement = user;
            cidElement.TravelSet = this.TravelSet;
            cidElement.TimesheetSet = this.TimeSheet;
            cidSet.cidElement = cidElement;
            //// scrittura su sap //////
            var that = this;
            this.oDialog.open();
            this.getOdataModel();
            this.modelloOdata = this.getView().getModel().createEntry("CIDSet", cidSet.cidElement);
            this._odataModel.submitChanges(function () {
                that._odataCallAttivitaSetRefreshForElaborate(sessionStorage.getItem("cid"), that);
                that.enableModel.setProperty("/releaseEnable", false);
                that.enableModel.setProperty("/unReleaseEnable", false);
                that.enableModel.setProperty("/duplicateEnable", false);
                that.enableModel.setProperty("/deleteEnableMulti", false);
                that.enableModel.setProperty("/releaseSingleEnable", false);
                that.enableModel.setProperty("/deleteEnable", false);
            }, function (err) {
                sap.m.MessageBox.error(JSON.parse(err.response.body).error.message.value, {
                    title: that._getLocaleText("WARNING")
                });
                that.oDialog.close();
            });
        } else {
            var that = this;
            sap.m.MessageBox.alert(that._getLocaleText("noConnessione"), {
                title: that._getLocaleText("WARNING")
            });
        };

    },

    onReleasePress: function () {
        this._oListFragment = sap.ui.xmlfragment("icms.view.fragment.selectDates", this);
        this.enableModel.setProperty("/confirmEnable", false);
        this.enableModel.setProperty("/testoBottoneVerde", this._getLocaleText("Release"));
        this.enableModel.setProperty("/testoBottoneRosso", this._getLocaleText("CANCEL"));
        this._oListFragment.setModel(this.enableModel);
        this._oListFragment.open();
        var activity = this.datiModel.getData().dati;
        var activityToRelease = _.where(activity, {
            'stato': "10"
        });
        this._activityToRelease = activityToRelease;
        var idCalendar = "calendarDuplicateActivity";
        this._setDayFull(idCalendar, activityToRelease);
        this._variabileConfirm = "rilascia";
        $("#calendarDuplicateActivity--Head-B1").attr("disabled", "disabled");
        $("#calendarDuplicateActivity--Head-B2").attr("disabled", "disabled");
        /* DISABILITO LA POSSIBILITA DI SELEZIONARE ALTRI MESI */
        $("#calendarDuplicateActivity--Head-next").addClass("visible");
        $("#calendarDuplicateActivity--Head-prev").addClass("visible");
        var annoAttuale = document.getElementById("calendarDuplicateActivity--Head-B2").textContent;
        var meseAttuale = document.getElementById("calendarDuplicateActivity--Head-B1").textContent;
        var result = utils.Util._getMeseAttuale(meseAttuale, annoAttuale);
        var annoPrec = annoAttuale;
        var annoSucc = annoAttuale;
        var mese = parseInt(result.mese);
        var mesePrec = mese - 1;
        var meseSucc = mese + 1;
        if (mesePrec < 10) {
            mesePrec = "0" + mesePrec;
        } else {
            mesePrec = mesePrec.toString();
        };
        if (meseSucc < 10) {
            meseSucc = "0" + meseSucc;
        } else {
            meseSucc = meseSucc.toString();
        };
        if (mese === 1) {
            annoPrec = (parseInt(annoPrec) - 1).toString();
            mesePrec = "12";
        };
        if (mese === 12) {
            annoSucc = (parseInt(annoSucc) + 1).toString();
            meseSucc = "01";
        };
        for (var i = 23; i <= 31; i++) {
            var giorno = i;
            var giornoMeseVecchio = parseInt(annoPrec) + "" + mesePrec + "" + giorno;
            $("#calendarDuplicateActivity--Month0-" + giornoMeseVecchio).addClass("visible");
        };
        for (var i = 1; i <= 6; i++) {
            var giorno = i;
            var giornoMeseVecchio = parseInt(annoSucc) + "" + meseSucc + "0" + giorno;
            $("#calendarDuplicateActivity--Month0-" + giornoMeseVecchio).addClass("visible");
        };
        /********************************/
    },

    _workDay: function () {
        var meseCorrente = new Date();
        meseCorrente = (meseCorrente.getMonth() + 1).toString();
        var annoAttuale = document.getElementById("calendarId--calendar--Head-B2").textContent;
        var meseAttuale = document.getElementById("calendarId--calendar--Head-B1").textContent;
        this.giornoPasqua = utils.calcoloPasqua.calcolaPasqua(parseInt(annoAttuale));
        var result = utils.Util._getMeseAttuale(meseAttuale, annoAttuale);
        meseAttuale = result.mese;
        var giorniMax = parseInt(result.giorno);

        var arrayGiorniFestivi = []
        var arrayGiorniFeriali = []
        for (var f = 0; f < giorniMax; f++) {
            var dd = f + 1;
            if (dd < 10) {
                dd = "0" + dd;
            } else {
                dd = dd.toString();
            };
            var giornoString = dd
            var giorno = annoAttuale + "" + meseAttuale + "" + giornoString;
            var giornoSettimana = document.getElementById("calendarId--calendar--Month0-" + giorno);
            giornoSettimana = giornoSettimana.getAttribute("class");
            giornoSettimana = giornoSettimana.split(" ");
            var arrayFestivita = {
                "01": ["01", "06"],
                "03": [],
                "04": [],
                "05": ["01"],
                "06": ["02"],
                "08": ["15"],
                "11": ["01"],
                "12": ["08", "25", "26"]
            };
            arrayFestivita[this.giornoPasqua[1]].push((this.giornoPasqua[0] + 1).toString())

            if (arrayFestivita[meseAttuale]) {
                for (var k = 0; k < arrayFestivita[meseAttuale].length; k++) {
                    if (giornoString === arrayFestivita[meseAttuale][k]) {
                        $('#calendarId--calendar--Month0-' + giorno).addClass("sapUiCalDayWeekEnd");
                        $('#calendarId--calendar--Month0-' + giorno).addClass("sapUiCalItemWeekEnd");
                    };
                };
            };
            if (giornoSettimana.length === 3 || giornoSettimana.length === 4) {
                if (giornoSettimana[1] === "sapUiCalItemWeekEnd" || giornoSettimana[1] === "sapUiCalDayWeekEnd" || giornoSettimana[2] === "sapUiCalItemWeekEnd" || giornoSettimana[2] === "sapUiCalDayWeekEnd") {
                    arrayGiorniFestivi.push(giornoString);
                } else {
                    arrayGiorniFeriali.push(giornoString);
                };

            } else {
                arrayGiorniFeriali.push(giornoString);
            };
        };
        trovaMese = parseInt(meseAttuale);
        for (var n in arrayFestivita) {
            if (trovaMese === parseInt(n)) {
                for (var t = 0; t < arrayFestivita[n].length; t++) {
                    for (var i = 0; i < arrayGiorniFeriali.length; i++) {
                        if (arrayGiorniFeriali[i] === arrayFestivita[n][t]) {
                            arrayGiorniFestivi.push(arrayGiorniFeriali[i]);
                            arrayGiorniFeriali.splice(i, 1);
                        };
                    };
                };
            };
        };
        var oreTotali = arrayGiorniFeriali.length * 8;
        var oreLavorate = 0;
        if (!this.datiModel.getData().dati) {
            this.datiModel.setProperty("/dati", utils.LocalStorage.getItem("attivitaSet"));
        };
        for (var i = 0; i < this.datiModel.getData().dati.length; i++) {
            if (this.datiModel.getData().dati[i].delete === undefined || this.datiModel.getData().dati[i].delete !== "X") {
                var data = (this.datiModel.getData().dati[i].dataCommessa).split("/");
                if (data[2] == annoAttuale) {
                    if (data[1] == meseAttuale) {
                        if (this.datiModel.getData().dati[i].durata) {
                            oreLavorate = (parseFloat(this.datiModel.getData().dati[i].durata) + oreLavorate);
                        };
                    };
                };
            };
        };
        oreLavoratePerc = parseInt(100 / oreTotali * oreLavorate);
        var ariaLabel = "Giorni lavorativi: " + arrayGiorniFeriali.length + "\n" + "Ore lavorative: " + oreTotali + "\n" + "Ore lavorate: " + oreLavorate;
        this.getView().getModel("appModel").setProperty("/oreTotali", oreTotali);
        this.getView().getModel("appModel").setProperty("/oreTotaliPerc", oreLavoratePerc);
        this.getView().getModel("appModel").setProperty("/oreLavorate", oreLavorate);
        this.getView().getModel("appModel").setProperty("/tooltip", ariaLabel);
    },

    /* CHIAMATE ODATA */
    _odataCallAttivitaSet: function (dataInizio, dataFine, exit) {
        this.oDialog.open();
        var deferA = Q.defer();

        var fError = function (err) {
            sap.m.MessageToast.show(this._getLocaleText("ErroreCaricamentoCommesse"));
            this.oDialog.close();
            return;
        };
        var cid = sessionStorage.getItem("cid");

        var fSuccessAttivitaSet = function (ris) {
            var attivitaSetOdata = ris.dati;
            for (var o = 0; o < attivitaSetOdata.length; o++) {
                if (attivitaSetOdata[o].counterFiori === "000000000000") {
                    attivitaSetOdata[o].counterFiori = "";
                };
            };
            _.sortBy(attivitaSetOdata, ['dataCommessa', 'counter']);
            var attivitaLS = utils.LocalStorage.getItem("attivitaSet");

            var meseAttText = document.getElementById("calendarId--calendar--Head-B1").textContent;
            var annoAttualeText = document.getElementById("calendarId--calendar--Head-B2").textContent;
            meseAtt = parseInt(utils.Util._getMeseAttuale(meseAttText, annoAttualeText).mese);

            var dataNuova = new Date();
            meseConfronto = dataNuova.getMonth() + 1;
            annoConfronto = dataNuova.getFullYear();
            if (exit === "exit") {
                annoAttualeText = annoConfronto.toString();
                var diff = meseConfronto - meseAtt;
                meseAtt = meseConfronto;
            };

            if (annoConfronto === parseInt(annoAttualeText)) {
                if (exit === "prev") {
                    if (meseConfronto > meseAtt) {
                        var diff = meseConfronto - meseAtt;
                        meseAtt = meseAtt + 1;
                    } else if (meseConfronto < meseAtt) {
                        meseAtt = meseAtt - 1;
                    } else {
                        var temp = _.find(attivitaLS, function (n) {
                            return n.dataCommessa.split("/")[1] === (meseAtt - 1).toString();
                        });
                        if (temp) {
                            meseAtt = meseAtt - 1;
                        } else {
                            meseAtt = meseAtt + 1;
                        };
                    };
                } else {
                    if (diff !== undefined) {
                        meseDiff = diff;
                        diff = undefined;
                    } else {
                        meseDiff = 1;
                    };
                    var temp = _.find(attivitaLS, function (n) {
                        return n.dataCommessa.split("/")[1] === (meseAtt - meseDiff).toString();
                    });
                    if (temp) {
                        meseAtt = meseAtt - meseDiff;
                    } else {
                        meseAtt = meseAtt + 1;
                    };
                };
            } else if ((annoConfronto < parseInt(annoAttualeText))) {
                if (exit === "prev" || exit === "next") {
                    if (meseConfronto === 12 && meseAtt === 1) {
                        meseAtt = 12;
                    } else if (meseConfronto > meseAtt) {
                        meseAtt = meseAtt + 1;
                    } else if (meseConfronto < meseAtt) {
                        meseAtt = meseAtt - 1;
                    } else {
                        var temp = _.find(attivitaLS, function (n) {
                            return n.dataCommessa.split("/")[1] === (meseAtt - 1).toString();
                        });
                        if (temp) {
                            meseAtt = meseAtt - 1;
                        } else {
                            meseAtt = meseAtt + 1;
                        };
                    };
                } else {
                    if (meseConfronto > meseAtt) {
                        meseAtt = meseAtt - 1;
                    } else {
                        var temp = _.find(attivitaLS, function (n) {
                            return n.dataCommessa.split("/")[1] === (meseAtt - 1).toString();
                        });
                        if (temp) {
                            meseAtt = meseAtt - 1;
                        } else {
                            meseAtt = meseAtt + 1;
                        };
                    };
                };
            } else if ((annoConfronto > parseInt(annoAttualeText))) {
                if (exit === "prev") {
                    if (meseConfronto === 12 && meseAtt === 12) {
                        meseAtt = 1;
                    } else if (meseConfronto > meseAtt) {
                        meseAtt = meseAtt + 1;
                    } else if (meseConfronto < meseAtt) {
                        meseAtt = meseAtt - 1;
                    } else {
                        var temp = _.find(attivitaLS, function (n) {
                            return n.dataCommessa.split("/")[1] === (meseAtt - 1).toString();
                        });
                        if (temp) {
                            meseAtt = meseAtt - 1;
                        } else {
                            meseAtt = meseAtt + 1;
                        };
                    };
                } else {
                    if (meseConfronto > meseAtt) {
                        meseAtt = meseAtt - 1;
                    } else {
                        var temp = _.find(attivitaLS, function (n) {
                            return n.dataCommessa.split("/")[1] === (meseAtt - 1).toString();
                        });
                        if (temp) {
                            meseAtt = meseAtt - 1;
                        } else {
                            meseAtt = meseAtt + 1;
                        };
                    };
                };
            };

            var attivitaLS = utils.LocalStorage.getItem("attivitaSet");

            var attivitaDaTenere = [];
            var meseConfrontoStringa = meseConfronto.toString();
            if (meseConfrontoStringa.length === 1) {
                meseConfrontoStringa = "0" + meseConfrontoStringa;
            };
            for (var i = 0; i < attivitaLS.length; i++) {
                if ((attivitaLS[i].dataCommessa.split("/")[1] !== meseConfrontoStringa ||
                        attivitaLS[i].dataCommessa.split("/")[2] !== (annoConfronto).toString()) &&
                    attivitaLS[i].counterFiori) {
                    attivitaDaTenere.push(attivitaLS[i]);
                };
            };

            _.remove(attivitaLS, function (n) {
                return n.dataCommessa.split("/")[1] !== meseConfrontoStringa.toString();
            });

            for (var k = 0; k < attivitaDaTenere.length; k++) {
                attivitaLS.push(attivitaDaTenere[k]);
            };

            for (var gabio = 0; gabio < attivitaLS.length; gabio++) {
                var rapportinoGabiesimo = attivitaLS[gabio];
                var counter = attivitaLS[gabio].counter;
                var dataCommessa = attivitaLS[gabio].dataCommessa;
                var obj = _.find(attivitaSetOdata, {
                    'counter': counter,
                    'dataCommessa': dataCommessa
                });
                if (obj && JSON.stringify(obj) !== JSON.stringify(rapportinoGabiesimo)) {
                    _.remove(attivitaSetOdata, {
                        'counter': counter,
                        'dataCommessa': dataCommessa
                    });
                };
            };

            var commessaColoreSet = utils.LocalStorage.getItem("commessaColorSet");
            for (var w = 0; w < attivitaSetOdata.length; w++) {
                var color = _.find(commessaColoreSet, {
                    'commessa': attivitaSetOdata[w].commessa
                });
                if (color === undefined) {
                    attivitaSetOdata[w].color = "#000000"
                } else {
                    attivitaSetOdata[w].color = color.colore;
                };
            };
            var commessaSet = utils.LocalStorage.getItem("commessaSet");
            var projectSet = utils.LocalStorage.getItem("projectSet");
            for (var i = 0; i < attivitaSetOdata.length; i++) {
                var objAttivita = _.find(commessaSet.dati, {
                    'elementoWBSID': attivitaSetOdata[i].attivita
                });
                attivitaSetOdata[i].descrizioneAttivita = objAttivita.descrizione;
                var objCommessa = _.find(projectSet, {
                    'commessa': attivitaSetOdata[i].commessa
                });
                attivitaSetOdata[i].descrizioneCommessa = objCommessa.descrizione;
                attivitaLS.push(attivitaSetOdata[i]);
            };

            deferA.resolve();
            utils.LocalStorage.setItem("attivitaSet", attivitaLS)
            this.datiModel.setProperty("/dati", utils.LocalStorage.getItem("attivitaSet"));
            this._odataCallNotaSpeseSet(dataInizio, dataFine, exit);
        };
        fSuccessAttivitaSet = _.bind(fSuccessAttivitaSet, this);
        fError = _.bind(fError, this);

        model.attivitaOdata.getAttivita(dataInizio, dataFine, cid)
            .then(
                fSuccessAttivitaSet,
                fError
            );
        return deferA.promise;
    },

    /*+******************************/

    _odataCallNotaSpeseSet: function (dataInizio, dataFine, exit) {
        var deferNS = Q.defer();

        var fError = function (err) {
            sap.m.MessageToast.show(this._getLocaleText("ErroreCaricamentoNotaSpese"));
            this.oDialog.close();
            return;
        };
        var cid = sessionStorage.getItem("cid");

        var fSuccessNotaSpeseSet = function (ris) {
            var notaSpeseSetOdata = ris;
            for (var o = 0; o < notaSpeseSetOdata.length; o++) {
                if (notaSpeseSetOdata[o].counterFiori === "000000000000") {
                    notaSpeseSetOdata[o].counterFiori = "";
                };
            };
            _.sortBy(notaSpeseSetOdata, ['dataCommessa', 'counter']);
            var notaSpeseLS = utils.LocalStorage.getItem("notaSpeseSet");

            for (var gabio = 0; gabio < notaSpeseLS.length; gabio++) {
                var notaSpesaGabiesima = notaSpeseLS[gabio];
                var numProgressivoGiustificativo = notaSpeseLS[gabio].numProgressivoGiustificativo;
                var numeroGiustificativo = notaSpeseLS[gabio].numeroGiustificativo;
                var numeroPeriodoTrasferta = notaSpeseLS[gabio].numeroPeriodoTrasferta;
                var numeroProgressivoAttCost = notaSpeseLS[gabio].numeroProgressivoAttCost;
                var numeroTrasferta = notaSpeseLS[gabio].numeroTrasferta;
                var categoriaSpesa = notaSpeseLS[gabio].categoriaSpesa;
                var obj = _.find(notaSpeseSetOdata, {
                    'numProgressivoGiustificativo': numProgressivoGiustificativo,
                    'numeroGiustificativo': numeroGiustificativo,
                    'numeroPeriodoTrasferta': numeroPeriodoTrasferta,
                    'numeroProgressivoAttCost': numeroProgressivoAttCost,
                    'numeroTrasferta': numeroTrasferta,
                    'categoriaSpesa': categoriaSpesa
                });
                delete notaSpesaGabiesima.color;
                delete notaSpesaGabiesima.descrizioneAttivita;
                delete notaSpesaGabiesima.descrizioneCommessa;
                if (obj && JSON.stringify(obj) !== JSON.stringify(notaSpesaGabiesima)) {
                    _.remove(notaSpeseSetOdata, {
                        'numProgressivoGiustificativo': numProgressivoGiustificativo,
                        'numeroGiustificativo': numeroGiustificativo,
                        'numeroPeriodoTrasferta': numeroPeriodoTrasferta,
                        'numeroProgressivoAttCost': numeroProgressivoAttCost,
                        'numeroTrasferta': numeroTrasferta,
                        'categoriaSpesa': categoriaSpesa
                    });
                };
            };

            var meseAttText = document.getElementById("calendarId--calendar--Head-B1").textContent;
            var annoAttualeText = document.getElementById("calendarId--calendar--Head-B2").textContent;
            meseAtt = parseInt(utils.Util._getMeseAttuale(meseAttText, annoAttualeText).mese);

            var dataNuova = new Date();
            meseConfronto = dataNuova.getMonth() + 1;
            annoConfronto = dataNuova.getFullYear();
            if (exit === "exit") {
                annoAttualeText = annoConfronto.toString();
                var diff = meseConfronto - meseAtt;
                meseAtt = meseConfronto;
            };

            if (annoConfronto === parseInt(annoAttualeText)) {
                if (exit === "prev") {
                    if (meseConfronto > meseAtt) {
                        var diff = meseConfronto - meseAtt;
                        meseAtt = meseAtt + 1;
                    } else if (meseConfronto < meseAtt) {
                        meseAtt = meseAtt - 1;
                    } else {
                        var temp = _.find(notaSpeseLS, function (n) {
                            return n.dataNotaSpese.split("/")[1] === (meseAtt - 1).toString();
                        });
                        if (temp) {
                            meseAtt = meseAtt - 1;
                        } else {
                            meseAtt = meseAtt + 1;
                        };
                    };
                } else {
                    if (diff !== undefined) {
                        meseDiff = diff;
                        diff = undefined;
                    } else {
                        meseDiff = 1;
                    }
                    var temp = _.find(notaSpeseLS, function (n) {
                        return n.dataNotaSpese.split("/")[1] === (meseAtt - meseDiff).toString();
                    });
                    if (temp) {
                        meseAtt = meseAtt - meseDiff;
                    } else {
                        meseAtt = meseAtt + 1;
                    };
                };
            } else if ((annoConfronto < parseInt(annoAttualeText))) {
                if (exit === "prev" || exit === "next") {
                    if (meseConfronto === 12 && meseAtt === 1) {
                        meseAtt = 12;
                    } else if (meseConfronto > meseAtt) {
                        meseAtt = meseAtt + 1;
                    } else if (meseConfronto < meseAtt) {
                        meseAtt = meseAtt - 1;
                    } else {
                        var temp = _.find(notaSpeseLS, function (n) {
                            return n.dataNotaSpese.split("/")[1] === (meseAtt - 1).toString();
                        });
                        if (temp) {
                            meseAtt = meseAtt - 1;
                        } else {
                            meseAtt = meseAtt + 1;
                        };
                    };
                } else {
                    if (meseConfronto > meseAtt) {
                        meseAtt = meseAtt - 1;
                    } else {
                        var temp = _.find(notaSpeseLS, function (n) {
                            return n.dataNotaSpese.split("/")[1] === (meseAtt - 1).toString();
                        });
                        if (temp) {
                            meseAtt = meseAtt - 1;
                        } else {
                            meseAtt = meseAtt + 1;
                        };
                    };
                };
            } else if ((annoConfronto > parseInt(annoAttualeText))) {
                if (exit === "prev") {
                    if (meseConfronto === 12 && meseAtt === 12) {
                        meseAtt = 1;
                    } else if (meseConfronto > meseAtt) {
                        meseAtt = meseAtt + 1;
                    } else if (meseConfronto < meseAtt) {
                        meseAtt = meseAtt - 1;
                    } else {
                        var temp = _.find(notaSpeseLS, function (n) {
                            return n.dataNotaSpese.split("/")[1] === (meseAtt - 1).toString();
                        });
                        if (temp) {
                            meseAtt = meseAtt - 1;
                        } else {
                            meseAtt = meseAtt + 1;
                        };
                    };
                } else {
                    if (meseConfronto > meseAtt) {
                        meseAtt = meseAtt - 1;
                    } else {
                        var temp = _.find(notaSpeseLS, function (n) {
                            return n.dataNotaSpese.split("/")[1] === (meseAtt - 1).toString();
                        });
                        if (temp) {
                            meseAtt = meseAtt - 1;
                        } else {
                            meseAtt = meseAtt + 1;
                        };
                    };
                };
            };

            var meseAttuale = utils.Util._getMeseAttuale(meseAttText, annoAttualeText).mese;
            var annoAttuale = document.getElementById("calendarId--calendar--Head-B2").textContent
            var noteSpeseDaTenere = [];
            var meseConfrontoStringa = meseConfronto.toString();
            if (meseConfrontoStringa.length === 1) {
                meseConfrontoStringa = "0" + meseConfrontoStringa;
            };
            for (var i = 0; i < notaSpeseLS.length; i++) {
                if ((notaSpeseLS[i].dataNotaSpese.split("/")[1] !== meseConfrontoStringa ||
                        notaSpeseLS[i].dataNotaSpese.split("/")[2] !== annoConfronto.toString()) &&
                    notaSpeseLS[i].counterFiori) {
                    noteSpeseDaTenere.push(notaSpeseLS[i]);
                };
            };

            _.remove(notaSpeseLS, function (n) {
                return n.dataNotaSpese.split("/")[1] !== meseConfrontoStringa;
            });

            for (var k = 0; k < noteSpeseDaTenere.length; k++) {
                notaSpeseLS.push(noteSpeseDaTenere[k]);
            };

            if (notaSpeseSetOdata) {
                for (var x = 0; x < notaSpeseSetOdata.length; x++) {
                    notaSpeseLS.push(notaSpeseSetOdata[x]);
                };
            };

            deferNS.resolve();
            utils.LocalStorage.setItem("notaSpeseSet", notaSpeseLS);
            var primoGiorno = "01/" + meseAttuale + "/" + annoAttuale;
            this.updateTable(primoGiorno);
            this._setDayFull();
            this._odataCallPerioSet(dataInizio, dataFine, exit);
        };
        fSuccessNotaSpeseSet = _.bind(fSuccessNotaSpeseSet, this);
        fError = _.bind(fError, this);

        model.noteSpeseOdata.getNoteSpese(dataInizio, dataFine, cid)
            .then(
                fSuccessNotaSpeseSet,
                fError
            );
        return deferNS.promise;
    },

    /*********************************/

    _odataCallPerioSet: function (dataInizio, dataFine, exit) {
        var deferPerio = Q.defer();
        var fError = function (err) {
            if (err.message) {
                err = this._getLocaleText("ErrorePerioOdata")
            };
            sap.m.MessageToast.show(err);
            this.oDialog.close();
        };

        var fSuccessPerioSet = function (ris) {
            var perioSet = undefined;
            var perioSetOdata = ris.dati;
            var perioSetLS = utils.LocalStorage.getItem("perioSet");
            if (!perioSetLS) {
                perioSetLS = {};
            };
            if (perioSetOdata.length !== 0) {
                perioSetLS.AttivitaChiusa = ris.dati[0].AttivitaChiusa;
                perioSetLS.NotaSpeseChiusa = ris.dati[0].NotaSpeseChiusa;
            } else {
                perioSetLS.AttivitaChiusa = "";
                perioSetLS.NotaSpeseChiusa = "";
            };
            utils.LocalStorage.setItem("perioSet", perioSetLS);
            var periodoAttivita = utils.LocalStorage.getItem("perioSet").AttivitaChiusa;
            var periodoNoteSpese = utils.LocalStorage.getItem("perioSet").NotaSpeseChiusa;
            var meseAtt = document.getElementById("calendarId--calendar--Head-B1").textContent;
            var annoAttuale = document.getElementById("calendarId--calendar--Head-B2").textContent;
            var meseAttuale = parseInt(utils.Util._getMeseAttuale(meseAtt, annoAttuale).mese);
            var meseAttualeStringa = meseAttuale.toString();
            if (meseAttualeStringa.length === 1) {
                meseAttualeStringa = "0" + meseAttualeStringa;
            };
            var meseReale = (new Date().getMonth()) + 1;
            var annoReale = new Date().getFullYear();
            if (periodoAttivita && periodoAttivita === "X" && periodoNoteSpese && periodoNoteSpese === "X" && meseReale !== meseAttuale || annoReale !== parseInt(annoAttuale)) {
                this.enableModel.setProperty("/syncEnable", false);
                this.enableModel.setProperty("/refreshEnable", false);
                this.enableModel.setProperty("/releaseEnable", false);
                this.enableModel.setProperty("/deleteEnableMulti", false);
                this.enableModel.setProperty("/setAddButtonVisible", false);
            } else {
                if (meseReale !== meseAttuale || annoReale !== parseInt(annoAttuale)) {
                    this.enableModel.setProperty("/syncEnable", false);
                    this.enableModel.setProperty("/refreshEnable", false);
                    this.enableModel.setProperty("/releaseEnable", false);
                    this.enableModel.setProperty("/deleteEnableMulti", false);
                } else {
                    this.enableModel.setProperty("/syncEnable", true);
                    this.enableModel.setProperty("/refreshEnable", true);
                    var rapportiniSet = utils.LocalStorage.getItem("attivitaSet");
                    var oggettiDaCancellare = _.where(rapportiniSet, {
                        'delete': ""
                    });
                    _.remove(oggettiDaCancellare, {
                        'stato': "20"
                    });
                    _.remove(oggettiDaCancellare, {
                        'stato': "30"
                    });
                    _.remove(oggettiDaCancellare, {
                        'stato': "40"
                    });
                    _.remove(oggettiDaCancellare, {
                        'stato': "50"
                    });
                    for (var i = 0; i < oggettiDaCancellare.length; i++) {
                        if (oggettiDaCancellare[i].dataCommessa.split("/")[1] !== meseAttualeStringa || oggettiDaCancellare[i].dataCommessa.split("/")[2] !== annoAttuale) {
                            oggettiDaCancellare.splice(i, 1);
                        };
                    };
                    var oggettiDaRilasciare = _.where(rapportiniSet, {
                        'delete': "",
                        'stato': "10"
                    });
                    for (var i = 0; i < oggettiDaRilasciare.length; i++) {
                        if (oggettiDaRilasciare[i].dataCommessa.split("/")[1] !== oggettiDaRilasciare || oggettiDaRilasciare[i].dataCommessa.split("/")[2] !== annoAttuale) {
                            oggettiDaRilasciare.splice(i, 1);
                        };
                    };
                    for (var t = 0; t < oggettiDaCancellare.length; t++) {
                        if (oggettiDaCancellare[t].dataCommessa.split("/")[1] === meseAttualeStringa && oggettiDaCancellare[t].dataCommessa.split("/")[2] === annoAttuale) {
                            var objDaCancellare = oggettiDaCancellare[t];
                            break;
                        };
                    };
                    for (var t = 0; t < oggettiDaRilasciare.length; t++) {
                        if (oggettiDaRilasciare[t].dataCommessa.split("/")[1] === meseAttualeStringa && oggettiDaRilasciare[t].dataCommessa.split("/")[2] === annoAttuale) {
                            var objDaRilasciare = oggettiDaRilasciare[t];
                            break;
                        };
                    };
                    if (objDaCancellare && periodoAttivita === "") {
                        this.enableModel.setProperty("/deleteEnableMulti", true);
                    } else {
                        this.enableModel.setProperty("/deleteEnableMulti", false);
                    };
                    if (objDaRilasciare && periodoAttivita === "") {
                        this.enableModel.setProperty("/releaseEnable", true);
                    } else {
                        this.enableModel.setProperty("/releaseEnable", false);
                    };
                };
                if (periodoAttivita && periodoAttivita === "X") {
                    this.enableModel.setProperty("/setAddButtonVisible", false);
                } else {
                    this.enableModel.setProperty("/setAddButtonVisible", true);
                };
            };
            deferPerio.resolve();
            this.oDialog.close();
            if (exit === "exit") {
                this._doLogout();
                exit = undefined;
            };
        };
        fSuccessPerioSet = _.bind(fSuccessPerioSet, this);
        fError = _.bind(fError, this);

        var meseCorrente = parseInt(utils.Util.dateToString(dataFine).split("/")[1]);
        var annoCorrente = parseInt(utils.Util.dateToString(dataFine).split("/")[2]);
        model.perioOdata.getPerio(sessionStorage.getItem("cid"), annoCorrente, meseCorrente)
            .then(
                fSuccessPerioSet,
                fError
            );
        return deferPerio.promise;
    },

    _odataCallAttivitaSetRefresh: function (cid, that) {
        var deferAR = Q.defer();

        var fError = function (err) {
            sap.m.MessageToast.show(err);
            this.oDialog.close();
            if (this.refresh === true) {
                this.refresh = false;
                this._setDayFull();
            };
        };

        var meseAtt = document.getElementById("calendarId--calendar--Head-B1").textContent;
        var annoAttuale = document.getElementById("calendarId--calendar--Head-B2").textContent;
        var meseAttuale = parseInt(utils.Util._getMeseAttuale(meseAtt, annoAttuale).mese);
        var meseAttualeStringa = meseAttuale.toString();
        var periodoAttivita = utils.LocalStorage.getItem("perioSet").AttivitaChiusa;
        if (meseAttualeStringa.length === 1) {
            meseAttualeStringa = "0" + meseAttualeStringa;
        };
        var dataInizio = utils.Util.getFirstDayFromMonth(parseInt(annoAttuale), meseAttuale - 1);
        var dataFine = utils.Util.getLastDayFromMonth(parseInt(annoAttuale), meseAttuale);

        var fSuccessAttivitaSet = function (ris) {
            var attivitaSetOdata = ris.dati;
            for (var o = 0; o < attivitaSetOdata.length; o++) {
                if (attivitaSetOdata[o].counterFiori === "000000000000") {
                    attivitaSetOdata[o].counterFiori = "";
                };
            };
            _.sortBy(attivitaSetOdata, ['dataCommessa', 'counter']);
            utils.LocalStorage.setItem("attivitaSet", []);
            attivitaSet = attivitaSetOdata;
            utils.LocalStorage.setItem("attivitaSet", attivitaSet);

            if (!utils.LocalStorage.getItem("attivitaSet")) {
                fError(this._getLocaleText("ErroreCaricamentoAttivita"));
            } else {
                var attivita = utils.LocalStorage.getItem("attivitaSet");
                this.datiModel.setProperty("/dati", []);
                this.datiModel.setProperty("/dati", attivita);
                var commessaColoreSet = utils.LocalStorage.getItem("commessaColorSet");
                var attivita = utils.LocalStorage.getItem("attivitaSet");
                for (var w = 0; w < attivita.length; w++) {
                    var color = _.find(commessaColoreSet, {
                        'commessa': attivita[w].commessa
                    });
                    if (color === undefined) {
                        attivita[w].color = "#000000"
                    } else {
                        attivita[w].color = color.colore;
                    };
                };
                var commessaSet = utils.LocalStorage.getItem("commessaSet");
                var projectSet = utils.LocalStorage.getItem("projectSet");
                for (var i = 0; i < attivita.length; i++) {
                    var objAttivita = _.find(commessaSet.dati, {
                        'elementoWBSID': attivita[i].attivita
                    });
                    attivita[i].descrizioneAttivita = objAttivita.descrizione;
                    var objCommessa = _.find(projectSet, {
                        'commessa': attivita[i].commessa
                    });
                    attivita[i].descrizioneCommessa = objCommessa.descrizione;
                };
                utils.LocalStorage.setItem("attivitaSet", "");
                utils.LocalStorage.setItem("attivitaSet", attivita);
                this.attivitaSet = utils.LocalStorage.getItem("attivitaSet");
                this.datiModel.setProperty("/dati", this.attivitaSet);
                if (this.enableModel.getProperty("/giornoSelezionato")) {
                    var data = this.enableModel.getProperty("/giornoSelezionato");
                    this.updateTable(data);
                };
                var oggettiDaCancellare = _.where(this.attivitaSet, {
                    'delete': ""
                });
                _.remove(oggettiDaCancellare, {
                    'stato': "20"
                });
                _.remove(oggettiDaCancellare, {
                    'stato': "30"
                });
                _.remove(oggettiDaCancellare, {
                    'stato': "40"
                });
                _.remove(oggettiDaCancellare, {
                    'stato': "50"
                });
                for (var i = 0; i < oggettiDaCancellare.length; i++) {
                    if (oggettiDaCancellare[i].dataCommessa.split("/")[1] !== meseAttualeStringa || oggettiDaCancellare[i].dataCommessa.split("/")[2] !== annoAttuale) {
                        oggettiDaCancellare.splice(i, 1);
                    };
                };
                var oggettiDaRilasciare = _.where(this.attivitaSet, {
                    'delete': "",
                    'stato': "10"
                });
                for (var i = 0; i < oggettiDaRilasciare.length; i++) {
                    if (oggettiDaRilasciare[i].dataCommessa.split("/")[1] !== oggettiDaRilasciare || oggettiDaRilasciare[i].dataCommessa.split("/")[2] !== annoAttuale) {
                        oggettiDaRilasciare.splice(i, 1);
                    };
                };
                for (var t = 0; t < oggettiDaCancellare.length; t++) {
                    if (oggettiDaCancellare[t].dataCommessa.split("/")[1] === meseAttualeStringa && oggettiDaCancellare[t].dataCommessa.split("/")[2] === annoAttuale) {
                        var objDaCancellare = oggettiDaCancellare[t];
                        break;
                    };
                };
                for (var t = 0; t < oggettiDaRilasciare.length; t++) {
                    if (oggettiDaRilasciare[t].dataCommessa.split("/")[1] === meseAttualeStringa && oggettiDaRilasciare[t].dataCommessa.split("/")[2] === annoAttuale) {
                        var objDaRilasciare = oggettiDaRilasciare[t];
                        break;
                    };
                };
                if (objDaCancellare && periodoAttivita === "") {
                    this.enableModel.setProperty("/deleteEnableMulti", true);
                } else {
                    this.enableModel.setProperty("/deleteEnableMulti", false);
                };
                if (objDaRilasciare && periodoAttivita === "") {
                    this.enableModel.setProperty("/releaseEnable", true);
                } else {
                    this.enableModel.setProperty("/releaseEnable", false);
                };
                deferAR.resolve();
                this._odataCallNotaSpeseSetRefresh(dataInizio, dataFine, cid, that);
            };
        };
        fSuccessAttivitaSet = _.bind(fSuccessAttivitaSet, this);
        fError = _.bind(fError, this);

        model.attivitaOdata.getAttivita(dataInizio, dataFine, cid)
            .then(
                fSuccessAttivitaSet,
                fError
            );
        return deferAR.promise;
    },

    _odataCallNotaSpeseSetRefresh: function (dataInizio, dataFine, cid, that) {
        var deferNSR = Q.defer();

        var fError = function (err) {
            sap.m.MessageToast.show(err);
            this.oDialog.close();
            if (this.refresh === true) {
                this.refresh = false;
                this._setDayFull();
            };
        };

        var fSuccessNotaSpeseSet = function (ris) {
            var notaSpeseSetOdata = ris;
            for (var o = 0; o < notaSpeseSetOdata.length; o++) {
                if (notaSpeseSetOdata[o].counterFiori === "000000000000") {
                    notaSpeseSetOdata[o].counterFiori = "";
                };
            };
            _.sortBy(notaSpeseSetOdata, ['dataNotaSpese', 'attivita']);

            utils.LocalStorage.setItem("notaSpeseSet", []);
            notaSpeseSet = notaSpeseSetOdata;
            utils.LocalStorage.setItem("notaSpeseSet", notaSpeseSet);

            if (!utils.LocalStorage.getItem("notaSpeseSet")) {
                fError(this._getLocaleText("ErroreCaricamentoNotaSpese"));
            } else {
                if (this.sincronizzare === true) {
                    sessionStorage.clear();
                    this.router.navTo("");
                    this.getView().getModel("appModel").setProperty("/arrivatoDaAltrePagine", true);
                };
                sap.m.MessageBox.alert(that._getLocaleText("SincronizzazioneRiuscita"), {
                    title: this._getLocaleText("WARNING")
                });
                deferNSR.resolve();
                if (this.enableModel.getProperty("/giornoSelezionato")) {
                    var data = this.enableModel.getProperty("/giornoSelezionato");
                    this.updateTable(data);
                };
                this.oDialog.close();
            };
            var meseReale = (new Date().getMonth() + 1).toString();
            if (meseReale.length === 1) {
                meseReale = "0" + meseReale;
            };
            if (this.refresh === true) {
                this.refresh = false;
                this._setDayFull();
            };
        };
        fSuccessNotaSpeseSet = _.bind(fSuccessNotaSpeseSet, this);
        fError = _.bind(fError, this);

        model.noteSpeseOdata.getNoteSpese(dataInizio, dataFine, cid)
            .then(
                fSuccessNotaSpeseSet,
                fError
            );
        return deferNSR.promise;
    },

    _odataCallAttivitaSetRefreshForElaborate: function (cid, that) {
        var deferAR = Q.defer();

        var fError = function (err) {
            sap.m.MessageToast.show(err);
            this.oDialog.close();
        };

        var meseAtt = document.getElementById("calendarId--calendar--Head-B1").textContent;
        var annoAttuale = document.getElementById("calendarId--calendar--Head-B2").textContent;
        var meseAttuale = parseInt(utils.Util._getMeseAttuale(meseAtt, annoAttuale).mese);
        var dataInizio = utils.Util.getFirstDayFromMonth(parseInt(annoAttuale), meseAttuale - 1);
        var dataFine = utils.Util.getLastDayFromMonth(parseInt(annoAttuale), meseAttuale);

        var fSuccessAttivitaSet = function (ris) {
            var attivitaSetOdata = ris.dati;
            for (var o = 0; o < attivitaSetOdata.length; o++) {
                if (attivitaSetOdata[o].counterFiori === "000000000000") {
                    attivitaSetOdata[o].counterFiori = "";
                };
            };
            _.sortBy(attivitaSetOdata, ['dataCommessa', 'counter']);

            var attivitaSet = _.clone(attivitaSetOdata);

            var rapportini = utils.LocalStorage.getItem("attivitaSet");
            for (var g = 0; g < this.dataToElaborate.length; g++) {
                _.remove(rapportini, {
                    'dataCommessa': this.dataToElaborate[g].dataCommessa,
                    'attivita': this.dataToElaborate[g].attivita,
                    'orarioInizio': this.dataToElaborate[g].orarioInizio,
                    'orarioFine': this.dataToElaborate[g].orarioFine
                });
                var obj = _.find(attivitaSet, {
                    'dataCommessa': this.dataToElaborate[g].dataCommessa,
                    'attivita': this.dataToElaborate[g].attivita,
                    'orarioInizio': this.dataToElaborate[g].orarioInizio,
                    'orarioFine': this.dataToElaborate[g].orarioFine
                });
                rapportini.push(obj);
            };
            utils.LocalStorage.setItem("attivitaSet", rapportini);

            if (!utils.LocalStorage.getItem("attivitaSet")) {
                fError(this._getLocaleText("ErroreCaricamentoAttivita"));
            } else {
                var attivita = utils.LocalStorage.getItem("attivitaSet");
                this.datiModel.setProperty("/dati", []);
                this.datiModel.setProperty("/dati", attivita);
                var commessaColoreSet = utils.LocalStorage.getItem("commessaColorSet");
                var attivita = utils.LocalStorage.getItem("attivitaSet");
                for (var w = 0; w < attivita.length; w++) {
                    var color = _.find(commessaColoreSet, {
                        'commessa': attivita[w].commessa
                    });
                    if (color === undefined) {
                        attivita[w].color = "#000000"
                    } else {
                        attivita[w].color = color.colore;
                    };
                };
                var commessaSet = utils.LocalStorage.getItem("commessaSet");
                var projectSet = utils.LocalStorage.getItem("projectSet");
                for (var i = 0; i < attivita.length; i++) {
                    var objAttivita = _.find(commessaSet.dati, {
                        'elementoWBSID': attivita[i].attivita
                    });
                    attivita[i].descrizioneAttivita = objAttivita.descrizione;
                    var objCommessa = _.find(projectSet, {
                        'commessa': attivita[i].commessa
                    });
                    attivita[i].descrizioneCommessa = objCommessa.descrizione;
                };
                utils.LocalStorage.setItem("attivitaSet", "");
                utils.LocalStorage.setItem("attivitaSet", attivita);
                this.attivitaSet = utils.LocalStorage.getItem("attivitaSet");
                this.datiModel.setProperty("/dati", this.attivitaSet);
                if (this.enableModel.getProperty("/giornoSelezionato")) {
                    var data = this.enableModel.getProperty("/giornoSelezionato");
                    this.updateTable(data);
                };
                this.oDialog.close();
                this.getView().byId("idTable").removeSelections();
                deferAR.resolve();
                sap.m.MessageToast.show(this._getLocaleText("operazioneCompletata"));
            };
        };
        fSuccessAttivitaSet = _.bind(fSuccessAttivitaSet, this);
        fError = _.bind(fError, this);

        model.attivitaOdata.getAttivita(dataInizio, dataFine, cid)
            .then(
                fSuccessAttivitaSet,
                fError
            );
        return deferAR.promise;
    },

    /******************/

    detailCidPress: function () {
        this.router.navTo("detailCid");
    },

    onRefreshPress: function () {
        if (Offline.state === "up") {
            this.calcolaNumeroAttivitaNoteSpese();
            if (this.nAttDaSinc !== 0 || this.nNoteSpeseDaSinc !== 0) {
                if (this.nAttDaSinc !== 0 && this.nNoteSpeseDaSinc !== 0) {
                    this.refresh = true;
                    var testoAttDaSinc = this.nAttDaSinc + " " + this._getLocaleText("nAttDaSinc");
                    var testoNoteSpeseDaSinc = " - " + this.nNoteSpeseDaSinc + " " + this._getLocaleText("nNoteSpeseDaSinc");
                } else if (this.nAttDaSinc !== 0 && this.nNoteSpeseDaSinc === 0) {
                    var testoAttDaSinc = this.nAttDaSinc + " " + this._getLocaleText("nAttDaSinc");
                    var testoNoteSpeseDaSinc = "";
                } else {
                    var testoAttDaSinc = "";
                    var testoNoteSpeseDaSinc = this.nNoteSpeseDaSinc + " " + this._getLocaleText("nNoteSpeseDaSinc");
                };
                sap.m.MessageBox.show(this._getLocaleText("avvisoReaload") + " " +
                    testoAttDaSinc + testoNoteSpeseDaSinc + " " + this._getLocaleText("procedere"),
                    sap.m.MessageBox.Icon.NONE,
                    this._getLocaleText("avvisoRealoadTitle"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
                    _.bind(this.richiedi, this)
                );
            } else {
                this.refresh = true;
                this.onOkSynchroPress();
            };
        } else {
            var that = this;
            sap.m.MessageBox.alert(that._getLocaleText("noConnessione"), {
                title: that._getLocaleText("WARNING")
            });
        };
    },

    richiedi: function (evt) {
        if (evt === sap.m.MessageBox.Action.YES) {
            if (Offline.state === "up") {
                this.refresh = true;
                sap.m.MessageBox.show(this._getLocaleText("avvisoSecondoReaload"),
                    sap.m.MessageBox.Icon.NONE,
                    this._getLocaleText("avvisoRealoadTitle"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
                    _.bind(this.onOkSynchroPress, this)
                );
            } else {
                var that = this;
                sap.m.MessageBox.alert(that._getLocaleText("noConnessione"), {
                    title: that._getLocaleText("WARNING")
                });
            };
        };
    },




    /*********** PELLINI *************/
    onNavButtonPress: function () {
        this.router.navTo("launchpad")
    },

    onLinkToUserInfoPress: function () {
        this.router.navTo("changePassword");
    },

    //    onLogoutPress: function () {
    //
    //    },

});
