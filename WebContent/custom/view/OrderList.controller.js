jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.OrderList");
jQuery.sap.require("model.ui.CustomRow");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("model.filters.Filter");
jQuery.sap.require("jquery.sap.storage");
jQuery.sap.require("model.Cart");
jQuery.sap.require("model.Current");

view.abstract.AbstractMasterController.extend("view.OrderList", {

  onExit: function () {

  },


  onInit: function () {

    // this.router = sap.ui.core.UIComponent.getRouterFor(this);
    // this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
    view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
    this.uiModel.setProperty("/searchProperty", ["orderId", "customerName"]);
      
      
    this.userModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.userModel, "userModel");
    
  },
    
  


  handleRouteMatched: function (evt) {

    var name = evt.getParameter("name");

    if (name !== "orderList") {
      return;
    }
      
    this.user = model.persistence.Storage.session.get("user");
    this.userModel.setData(this.user);
      
    this.orderListModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.orderListModel, "orderList");
      
     if (!!sessionStorage.getItem("customerSession") && sessionStorage.getItem("customerSession")==="true"){
          var customerData = model.persistence.Storage.session.get("currentCustomer");
          if(!!customerData && !!customerData.registry.id){
              var customerIdValue = customerData.registry.id;
                model.collections.OrderList.loadOrdersByCustomerId(customerIdValue) //forceReload
                .then(_.bind(function(res){
                    console.log(res);

                    this.orderListModel.setData(res);
                    this.orderListModel.refresh(true);

                  }, this));
             
          }
          //var oTable = this.getView().byId("orderListTable");
          
      }else{
      model.collections.OrderList.loadOrders(true) //forceReload
      .then(_.bind(function(res){
            console.log(res);
           
            this.orderListModel.setData(res);
            this.orderListModel.refresh(true);
            
          }, this));
          
        }
      

      
    
//      model.collections.OrderList.loadOrders(true) //forceReload
//      .then(_.bind(function(res){
//            console.log(res);
//           
//            this.orderListModel.setData(res);
//            this.orderListModel.refresh(true);
//            
//          }, this));

//***********Primo metodo usato mettendo il filtro direttamente nella view*********/////////////
//      
//      if (!!sessionStorage.getItem("customerSession") && sessionStorage.getItem("customerSession")==="true"){
//          var customerData = model.persistence.Storage.session.get("currentCustomer");
//          if(!!customerData && !!customerData.registry.id){
//              var customerIdValue = customerData.registry.id;
//              this.applyFilter(customerIdValue, "customerId");
//          }
//          var oTable = this.getView().byId("orderListTable");
//          
//      }else{
//          this.applyFilter("", "customerId");
//          return;
//           }
//***********************************************************************


  },
    
    onInfoPress: function(evt){
       var oTable = this.getView().byId("orderListTable"); 
    },
    
    onLinkToPdfPress: function(evt){
        //sap.m.URLHelper.redirect(this._getVal(evt), true);
        sap.m.URLHelper.redirect("https://drive.google.com/file/d/0Bx1Hy4k97vZPWHNQbkxVMmNqUVE/view?usp=sharing", true);
    },
    
    onAfterRendering: function(){
        

        // Set Row Color Conditionally  

//        var oModel = oTable.getModel();
//        var rowCount = oTable.getVisibleRowCount();
//        //var rowCount = oTable.getVisibleItems().length; //number of visible rows  
//        var rowStart = oTable.getFirstVisibleRow(); //starting Row index  
//        var currentRowContext;  
//        for (var i = 0; i < rowCount; i++) {  
//          currentRowContext = oTable.getContextByIndex(rowStart + i); //content  
//            // Remove Style class else it will overwrite  
//            oTable.getRows()[i].$().removeClass("yellow");  
//            oTable.getRows()[i].$().removeClass("green");  
//            oTable.getRows()[i].$().removeClass("red");  
//            var cellValue = oModel.getProperty("amount", currentRowContext); // Get Amount  
//            // Set Row color conditionally  
//            if (cellValue >= 1500) {  
//                oTable.getRows()[i].$().addClass("green");  
//            } else if (cellValue < 1500 && cellValue > 1000) {  
//                oTable.getRows()[i].$().addClass("yellow");  
//            } else {  
//                oTable.getRows()[i].$().addClass("red");  
//            }  
//        } 
  },

  onItemPress: function (evt) {
    var src = evt.getSource();
    var selectedItem = src.getBindingContext("p").getObject();
    this.getView().getModel("appStatus").setProperty("/currentProduct", selectedItem);
    // this.getView().getModel("appStatus").setProperty("/masterCntrl", this.getView().getController());
    this.navToProduct(selectedItem.getId());


  },
    
    

//  refreshList: function (evt) {
//    //var filters = this.getFiltersOnList();
//    //this.productsModel = model.collections.Products.getModel();
//
//    this.orderListModel.setData();
//
////    if (filters)
////      this.getView().byId("list").getBinding("items").filter(filters);
//
//  },
  onFilterPress: function () {
    this.filterModel = model.filters.Filter.getModel(this.orderListModel.getData().results, "orders");
    this.getView().setModel(this.filterModel, "filter");
    var page = this.getView().byId("orderListPageId");
    this.filterDialog = sap.ui.xmlfragment("view.dialog.filterDialog", this);
    page.addDependent(this.filterDialog);
    this.filterDialog.open();

  },
  onFilterDialogClose: function () {
    this.filterDialog.close();
  },

  onFilterPropertyPress: function (evt) {

    var parentPage = sap.ui.getCore().byId("parent");
    var elementPage = sap.ui.getCore().byId("children");
    console.log(this.getView().getModel("filter").getData().toString());
    var navCon = sap.ui.getCore().byId("navCon");
    var selectedProp = evt.getSource().getBindingContext("filter").getObject();
    this.getView().getModel("filter").setProperty("/selected", selectedProp);
    this.elementListFragment = sap.ui.xmlfragment("view.fragment.FilterList", this);
    elementPage.addContent(this.elementListFragment);

    navCon.to(elementPage, "slide");
    this.getView().getModel("filter").refresh();
  },

  onBackFilterPress: function (evt) {
    // this.addSelectedFilterItem();
    this.navConBack();
    this.getView().getModel("filter").setProperty("/selected", "");
    this.elementListFragment.destroy();
  },
  navConBack: function () {
    var navCon = sap.ui.getCore().byId("navCon");
    navCon.to(sap.ui.getCore().byId("parent"), "slide");
    this.elementListFragment.destroy();
  },
  afterOpenFilter: function (evt) {
    var navCon = sap.ui.getCore().byId("navCon");
    if (navCon.getCurrentPage().getId() == "children")
      navCon.to(sap.ui.getCore().byId("parent"), "slide");
    this.getView().getModel("filter").setProperty("/selected", "");
  },

  onSearchFilter: function (oEvt) {
    var aFilters = [];
    var sQuery = oEvt.getSource().getValue();

    if (sQuery && sQuery.length > 0) {

      // var filter = new sap.ui.model.Filter("value", sap.ui.model.FilterOperator.Contains, sQuery);

      // 	var filter = new sap.ui.model.Filter({path:"value", test:function(val)
      // {
      // 	var property= val.toString().toUpperCase();
      // 	return (property.indexOf(sQuery.toString().toUpperCase())>=0)
      // }});

      aFilters.push(this.createFilter(sQuery, "value"));
    }

    // update list binding
    //var list = sap.ui.getCore().byId("filterList");
    var table = this.getView().byId("orderListTable");
    var binding = table.getBinding("items");
    binding.filter(aFilters);
  },
  createFilter: function (query, property) {
    var filter = new sap.ui.model.Filter({
      path: property,
      test: function (val) {
        var prop = val.toString().toUpperCase();
        return (prop.indexOf(query.toString().toUpperCase()) >= 0)
      }
    });
    return filter;
  },
  onFilterDialogClose: function (evt) {
    if (this.elementListFragment) {
      this.elementListFragment.destroy();
    }
    if (this.filterDialog) {
      this.filterDialog.close();
      this.filterDialog.destroy();
    }
  },
  onFilterDialogOK: function (evt) {
    var filterItems = model.filters.Filter.getSelectedItems("orders");
    if (this.elementListFragment)
      this.elementListFragment.destroy();
    this.filterDialog.close();
    this.getView().getModel("filter").setProperty("/selected", "");
    this.handleFilterConfirm(filterItems);
    this.filterDialog.destroy();
    delete(this.filterDialog);
  },
  handleFilterConfirm: function (selectedItems) {
    var filters = [];
    _.forEach(selectedItems, _.bind(function (item) {
        filters.push(this.createFilter(item.value, item.property));
      },
      this));
    var table = this.getView().byId("orderListTable");
    var binding = table.getBinding("items");
    binding.filter(filters);
  },
  onResetFilterPress: function () {
    model.filters.Filter.resetFilter("orders");
    if (this.elementListFragment) {
      this.elementListFragment.destroy();
    }
    if (this.filterDialog) {
      this.filterDialog.close();
      this.filterDialog.destroy();
    }
    var table = this.getView().byId("orderListTable");
    var binding = table.getBinding("items");
    binding.filter();
    //sap.m.MessageToast.show("All filters cleared!");
    // console.log(model.filters.Filter.getSelectedItems("customers"));
  },



  navToOrderDetails: function (evt) {
     
      var src = evt.getSource();
      var selectedItem = src.getBindingContext("orderList").getObject();
      var id = selectedItem.orderId;
    this.router.navTo("orderInfo", {id : id});
    
  },
    
  toAbnormalPractices: function (evt) {
     
      var src = evt.getSource();
      var selectedItem = src.getBindingContext("orderList").getObject();
      var id = selectedItem.orderId;
      model.persistence.Storage.session.save("orderIdParameter", id);
      this.router.navTo("noDataSplitDetail");
    
  },
    
    
onSearch: function (evt) {
    var src = evt.getSource();
    this.searchValue = src.getValue();
    var searchProperty = this.uiModel.getProperty("/searchProperty");

    this.applyFilter(this.searchValue, searchProperty);

  },
  applyFilter: function (value, params) {

    var table = this.getView().byId("orderListTable");
    if (!table.getBinding("items").oList || table.getBinding("items").oList.length === 0)
      return;


    var temp = table.getBinding("items").oList[0]; // a template just to recover the data types
    var filtersArr = [];
    // var props = utils.ObjectUtils.getKeys(temp);

    if (!_.isEmpty(params)) {
      if (!_.isArray(params)) {
        params = [params];
      }
      for (var i = 0; i < params.length; i++) {


        switch (typeof (utils.ObjectUtils.getValues(temp, params[i]))) {
        case "undefined":
          break;
        case "string":
          filtersArr.push(new sap.ui.model.Filter(params[i], sap.ui.model.FilterOperator.Contains, value));
          break;
        case "number":
          filtersArr.push(new sap.ui.model.Filter(params[i], sap.ui.model.FilterOperator.EQ, value));
          break;
        }

      }
      var filter = new sap.ui.model.Filter({
        filters: filtersArr,
        and: false
      });
      table.getBinding("items").filter(filter);
      return;
    }
    table.getBinding("items").filter();
  },
    
  


  onOrderHeaderPress: function (evt) {
    this.router.navTo("newOrder");
  }








});