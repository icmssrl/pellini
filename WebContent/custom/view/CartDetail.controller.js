jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.Customers");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("model.Order");
jQuery.sap.require("model.Current");
jQuery.sap.require("model.Position");
jQuery.sap.require("model.Cart");

view.abstract.AbstractController.extend("view.CartFull", {

	onExit: function () {

	},


	onInit: function () {

		// this.router = sap.ui.core.UIComponent.getRouterFor(this);
		// this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
		view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

	},


	handleRouteMatched: function (evt) {

		view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
		var routeName = evt.getParameter("name");

		if ( routeName !== "cartDetail"){
			return;
		}

		this.order = model.Current.getOrder();
		if(!this.order)
		{
			var orderData = model.persistence.Storage.session.get("currentOrder");
			this.order = new model.Order(orderData);
		};
		this.customer = model.Current.getCustomer();
		if(!this.customer)
		{
			var customerData = model.persistence.Storage.session.get("currentCustomer");
			this.customer = new model.Customer(customerData);
		}
		this.getView().setModel(this.customer.getModel(), "customer");


		this.cartModel = this.order.getModel();
		this.getView().setModel(this.cartModel, "c");


	},

	onSave: function() {

//						var username = 'ADMINISTRATION01';
//						var password = 'Welcome1';
//
//						// var clientID = sap.ui.getCore().byId(this.getView().getId()).byId("code");
//						// var productID = sap.ui.getCore().byId(this.getView().getId()).byId("productID");
//						// var quantity = sap.ui.getCore().byId(this.getView().getId()).byId("quantity");
//						// var description = sap.ui.getCore().byId(this.getView().getId()).byId("description");
//						var counter = 1000;
//						var xmlhttp = new XMLHttpRequest();
//						xmlhttp.open('POST', 'https://my303832.crm.ondemand.com/sap/bc/srt/scs/sap/customerorderprocessingmanagec?sap-vhost=my303832.crm.ondemand.com/', true);
//
//
//				var sr =
//						'<?xml version="1.0" encoding="utf-8"?>' +
//
//							 '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" '+
//							 'xmlns:glob="http://sap.com/xi/SAPGlobal20/Global">'+
//							 '<soapenv:Header/>'+
//							 '<soapenv:Body>'+
//									'<glob:CustomerOrderBundleMaintainRequest_sync_V1>'+
//
//										//  '<BasicMessageHeader>'+
//										//
//										//     '<ID schemeAgencySchemeAgencyID="?">'+clientID.getValue()+'</ID>'+
//										 //
//										//  '</BasicMessageHeader>'+
//						'<CustomerOrder actionCode="01" >' +
//							'<ProcessingTypeCode>OR</ProcessingTypeCode>' +
//							'<BuyerParty >'+
//							 '<BusinessPartnerInternalID>'+this.customer.getId()+'</BusinessPartnerInternalID>'+
//							'</BuyerParty>'+
//
//							'<PayerParty>' +
//							 '<BusinessPartnerInternalID>'+this.customer.getId()+'</BusinessPartnerInternalID>' +
//							'</PayerParty>' +
//							'<EmployeeResponsibleParty>' +
//							 '<BusinessPartnerInternalID></BusinessPartnerInternalID>' +
//							 '<EmployeeID></EmployeeID>' +
//							'</EmployeeResponsibleParty>';
//
//							if(this.order.getPositions() && this.order.getPositions().length > 0)
//							{
//								for(var i = 0; i< this.order.getPositions().length; i++, counter++)
//								{
//									sr += '<Item actionCode="01">' +
//									  '<ID></ID>' +
//
//                                        '<Description languageCode="">'+this.order.getPositions()[i].description+'</Description>'+
//
//		 							 '<ItemProduct>' +
//		 								'<ProductID>'+ this.order.getPositions()[i].productId +'</ProductID>' +
//		 							 '</ItemProduct>' +
//		 							 '<ItemRequestedScheduleLine>' +
//		 								'<Quantity unitCode="EA">'+this.order.getPositions()[i].quantity+'</Quantity>' +
//		 							 '</ItemRequestedScheduleLine>' +
//		 							 '<ItemText actionCode="01">' +
//
//
//		 								'<TextTypeCode>10011</TextTypeCode>' +
//		 								'<ContentText>"test"</ContentText>' +
//
//		 							 '</ItemText>' +
//		 							'</Item>'
//								}
//
//							}
//
//							sr += '</CustomerOrder>' +
//									'</glob:CustomerOrderBundleMaintainRequest_sync_V1>'+
//							 '</soapenv:Body>'+
//						'</soapenv:Envelope>';
//
//
//
//						xmlhttp.onreadystatechange = function (r) {
//								if (xmlhttp.readyState == 4) {
//										if (xmlhttp.status == 200) {
//
//												console.log(r.target.responseText);
//												console.log(r.target.statusText);
//												sap.m.MessageToast.show("Inserimento Ordine OK!!", {
//														duration: 3000
//
//												});
//
//										}else{
//												sap.m.MessageToast.show("Error!! Please retry Save!", {
//														duration: 3000
//
//												});
//										}
//								}
//						}
//
//						xmlhttp.setRequestHeader("Authorization", "Basic " + btoa(username+':'+password));
//						// Send the POST request
//						xmlhttp.setRequestHeader('Content-Type', 'text/xml');
//						xmlhttp.send(sr);
						// send request
						// ...
		}

	// refreshView : function(data, route)
	// {
	// 		var enable = {"editable": false};
	// 		var page = this.getView().byId("detailPage");
	// 		if(route && route.toUpperCase().indexOf("edit".toUpperCase())>=0)
	// 		{
	// 			var clone = _.cloneDeep(data.getModel().getData());
	// 			var clonedModel = new sap.ui.model.json.JSONModel(clone);
	// 			this.getView().setModel(clonedModel, "c");
	// 			enable.editable = true;
	// 			var toolbar = sap.ui.xmlfragment("view.fragment.editToolbar", this);
	// 			page.setFooter(toolbar);
	// 			//Maybe to parameterize
	// 			this.populateSelect();
	// 			//------------------------
	// 		}
	// 		else
	// 		{
	// 			this.getView().setModel(data.getModel(), "c");
	// 			enable.editable=false;
	// 			var toolbar = sap.ui.xmlfragment("view.fragment.detailToolbar", this);
	// 			page.setFooter(toolbar);
	// 		}
	// 		var enableModel = new sap.ui.model.json.JSONModel(enable);
	// 		this.getView().setModel(enableModel, "en");
	//
	// },
	// refreshView : function(data)//maybe to abstract
	// {
	// 		var enable = {"editable": false};
	// 		var page = this.getView().byId("detailPage");
	// 		this.getView().setModel(data.getModel(), "p");
	// 		enable.editable=false;
	// 		var toolbar = sap.ui.xmlfragment("view.fragment.catalogueToolbar", this);
	// 		page.setFooter(toolbar);
	// 		// var enableModel = new sap.ui.model.json.JSONModel(enable);
	// 		// this.getView().setModel(enableModel, "en");
	//
	// },

	// onAddPress : function(evt)
	// {
	// 		this.order = model.Current.getOrder();
	//
	// 		//Now it's here, maybe the next code needs to be moved
	// 		//As we are in orderCreqtion, maybe we need another property to verify the application status
	// 			if(!this.order)
	// 			{
	// 				var orderData = model.persistence.Storage.session.get("currentOrder");
	// 				this.order  = new model.Order(orderData);
	//
	// 			}
	//
	// 		//------------------------------------------------
	// 		var position = new model.Position();
	// 		position.create(this.order, this.product);
	// 		this.order.addPosition(position);
	// 		model.Current.setOrder(this.order);
	// 		console.log(this.order);
	//
	//
	//
	// }
	// onStartSessionPress : function()
	// {
	// 	model.persistence.Storage.session.save("customerSession", true);
	// 	model.persistence.Storage.session.save("currentCustomer", this.customer);
	// 	//Maybe is better correcting save class
	// 	model.Current.setCustomer(this.customer);
	// 	this.router.navTo("launchpad");
	// },
	// onEditPress : function()
	// {
	// 	this.router.navTo("customerEdit", {id:this.customer.getId()});
	// 	// this.refreshView(this.customer, "edit");
	// },

	// onCreatePress:function(evt)
	// {
	// 	this.router.navTo("newCustomer");
	// }
	// onSavePress :function()
	// {
	// 	var editedCustomer = this.getView().getModel("c").getData();
	// 	this.customer.update(editedCustomer);
	// 	this.router.navTo("customerDetail", {id:this.customer.getId()});
	// 	// this.refreshView(this.customer);
	// },
	// onResetPress : function()
	// {
	// 	this.getView().getModel("c").setData(_.cloneDeep(this.customer.getModel().getData()));
	// },

	// populateSelect :  function()
	// {
	// 	//HP1 : Maybe it's possible creating a mapping file on which every collection has its params to select
	// 	//HP2 : Every collections contains an array of params where a select is needed
	// 	var pToSelArr = [
	// 		{"type":"registryTypes", "namespace":"rt"},
	// 		{"type":"billTypes", "namespace":"bt"},
	// 		{"type":"contactTypes", "namespace":"ct"},
	// 		{"type":"paymentConditions", "namespace": "pc"},
	// 		{"type": "places", "namespace":"p"}
	// 		];
	//
	// 	_.map(pToSelArr, _.bind(function(item)
	// {
	// 	utils.Collections.getModel(item.type)
	// 	.then(_.bind
	// 		(function(result)
	// 	{
	// 		this.getView().setModel(result, item.namespace);
	//
	// 	}, this))
	// }, this));
	//
	//
	// }


});
