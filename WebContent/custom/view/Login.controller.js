jQuery.sap.require("model.User");
jQuery.sap.require("model.i18n");
jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("sap.m.MessageToast");

view.abstract.AbstractController.extend("view.Login", {

    onExit: function () {

    },

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
        // this.router = sap.ui.core.UIComponent.getRouterFor(this);
        // this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");

        if (name !== "login") {
            return;
        }

        this.user = new model.User();
        var credentialModel = this.user.getNewCredential();
        this.getView().setModel(credentialModel, "usr");

        model.i18n.setModel(this.getView().getModel("i18n"));
    },

    onAfterRendering: function (evt) {
        view.abstract.AbstractController.prototype.onAfterRendering.apply(this, arguments);
        $(document).on("keypress", _.bind(function (evt) {
            if (evt.keyCode === 13 && (this.validateCheck())) {
                this.onLoginPress();
            }
        }, this));
    },

    onLoginPress: function (evt) {
        var username = this.getView().getModel("usr").getProperty("/username");
        var pwd = this.getView().getModel("usr").getProperty("/password");
        // model.User.setUserName(username);
        // model.User.setPassword(pwd);
        this.user.setCredential(username, pwd);

        // model.User.doLogin()
        // .then(_.bind(function(result){
        // 	this.router.navTo("launchpad");
        //
        // }, this));
        this.user.doLogin()
            .then(_.bind(this.choosePath, this));
        // .then(_.bind(function(result){
        //
        // 	this.router.navTo("launchpad");
        //
        // }, this));
    },

    choosePath: function () {
//        if (!this.user.organizationData || _.chain(this.user.organizationData).pluck('division').uniq().value().length === 1) {
            this.router.navTo("launchpad");

//        } else {
//            if (_.chain(this.user.organizationData).pluck('division').uniq().value().length > 1) {
//                this.router.navTo("soLaunchpad");
//            }
//        }
    },

    onSwitchLanguage: function (oEvent) {
        var control = oEvent.getSource();
        var buttonID = control.getId();
        var oI18nModel;
        switch (buttonID) {
        case "loginId--italyFlag":

            sap.ui.getCore().getConfiguration().setLanguage("it_IT");
            sessionStorage.setItem("language", "it_IT");
            this.getView().getModel("i18n").refresh();
            sap.m.MessageToast.show(model.i18n._getLocaleText("LANGUAGE_CHANGED"), {
                at: "center top"
            });
            location.reload();

            break;
        case "loginId--britainFlag":
            sap.ui.getCore().getConfiguration().setLanguage("en_EN");
            sessionStorage.setItem("language", "en_EN");
            this.getView().getModel("i18n").refresh();
            sap.m.MessageToast.show(model.i18n._getLocaleText("LANGUAGE_CHANGED"), {
                at: "center top"
            });
            location.reload();
            break;
        case "loginId--usaFlag":
            sap.ui.getCore().getConfiguration().setLanguage("en_US");
            sessionStorage.setItem("language", "en_EN");
            this.getView().getModel("i18n").refresh();
            sap.m.MessageToast.show(model.i18n._getLocaleText("LANGUAGE_CHANGED"), {
                at: "center top"
            });
            location.reload();
            break;
        case "loginId--spainFlag":
            sap.ui.getCore().getConfiguration().setLanguage("en_EN");
            sessionStorage.setItem("language", "en_EN");
            this.getView().getModel("i18n").refresh();
            sap.m.MessageToast.show(model.i18n._getLocaleText("LANGUAGE_CHANGED"), {
                at: "center top"
            });
            break;
        case "loginId--germanyFlag":
            sap.ui.getCore().getConfiguration().setLanguage("en_EN");
            sessionStorage.setItem("language", "en_EN");
            this.getView().getModel("i18n").refresh();
            sap.m.MessageToast.show(model.i18n._getLocaleText("LANGUAGE_CHANGED"), {
                at: "center top"
            });
            break;
        case "loginId--netherlandFlag":
            sap.ui.getCore().getConfiguration().setLanguage("en_EN");
            sessionStorage.setItem("language", "en_EN");
            this.getView().getModel("i18n").refresh();
            sap.m.MessageToast.show(model.i18n._getLocaleText("LANGUAGE_CHANGED"), {
                at: "center top"
            });
            break;
        case "loginId--franceFlag":
            sap.ui.getCore().getConfiguration().setLanguage("en_EN");
            sessionStorage.setItem("language", "en_EN");
            this.getView().getModel("i18n").refresh();
            sap.m.MessageToast.show(model.i18n._getLocaleText("LANGUAGE_CHANGED"), {
                at: "center top"
            });
            break;
        default:
            sap.ui.getCore().getConfiguration().setLanguage("it_IT");
            sessionStorage.setItem("language", "it_IT");
            this.getView().getModel("i18n").refresh();
            sap.m.MessageToast.show(model.i18n._getLocaleText("LANGUAGE_CHANGED"), {
                at: "center top"
            });

        }
        //        var state = control.getState();
        //        var i18nModel;
        //        if (state) {
        //            sap.ui.getCore().getConfiguration().setLanguage("EN"); 
        //            //i18nModel = new sap.ui.model.resource.ResourceModel({bundleUrl: "custom/i18n/text_en.properties"});
        //            //model.i18n.setModel(i18nModel);
        //            //sap.ui.getCore().setModel(i18nModel, "i18n");
        //        } else {
        //            sap.ui.getCore().getConfiguration().setLanguage("IT");
        ////            i18nModel = new sap.ui.model.resource.ResourceModel({bundleUrl:"custom/i18n/text_it.properties"});
        ////            model.i18n.setModel(i18nModel);
        //            //sap.ui.getCore().setModel(i18nModel, "i18n");
        //        }
    }

});