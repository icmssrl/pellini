jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.Customers");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("model.Current");
jQuery.sap.require("model.i18n");


view.abstract.AbstractController.extend("view.VisitsTourDetail", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var routeName = evt.getParameter("name");

        if (routeName !== "visitsTourDetail") {
            return;
        }

        var sellingPoint = evt.getParameters().arguments.sellingPoint;
        var channel = evt.getParameters().arguments.channel;
        this.currentChannel = channel;
        var page = this.getView().byId("visitsTourDetailPage");
        var currentSellingPoint = model.persistence.Storage.session.get("currentSellingPoint");
        if (!!currentSellingPoint)
            this.getView().getModel("ui").setProperty("/sellingPoint", currentSellingPoint.description);
        /********************Test****************/
        this.removeContentFromPage();
        switch (channel) {
            case "Horeca":
                {
                    var channels = [{
                        description: "Nessuna Segnalazione"
                    }, {
                        description: "Segnalazione"
                    }];
                    this.dataOrgModel = new sap.ui.model.json.JSONModel();
                    this.dataOrgModel.setProperty("/orgData", channels);
                    this.getView().setModel(this.dataOrgModel, "dataOrg");
                    this._innerDataOrgDialogOpen();
                    break;
                }
            case "Retail":
                {
                    this.createWizardRetail();
                    break;
                }
            case "PrivateLabel":
                {
                    this.createWizardPrivateLabel();
                    break;
                }
        }
    },

    _innerDataOrgDialogOpen: function () {

        if (!this.segnalazioneDialog)
            this.segnalazioneDialog = sap.ui.xmlfragment("view.fragment.Horeca.Criticita", this);

        var page = this.getView().byId("visitsTourDetailPage");
        page.addDependent(this.segnalazioneDialog);
        this.segnalazioneDialog.open();

        this.dataOrgSelectList = sap.ui.getCore().byId("segnalazioneListId");
        if (!this.dataOrgSelectList.getSelectedItem()) {
            this.dataOrgSelectList.setSelectedItem(this.dataOrgSelectList.getItems()[0]);
        }

    },

    onDataOrgDialogConfirm: function (evt) {
        if (!!this.segnalazioneDialog) {
            this.segnalazioneDialog.close();
        }
    },

    onDataOrgDialogBeforeClose: function (evt) {
        var itemSelected = evt.getSource().getContent()[0].getSelectedItem().getBindingContext("dataOrg").getObject();
        if (!!itemSelected) {
            if (itemSelected.description === "Segnalazione") {
                this.createWizard1Horeca();
            } else {
                this.createWizard2Horeca();
            }
        }
    },

    createWizardRetail: function () {
        var page = this.getView().byId("visitsTourDetailPage");
        var fragment = sap.ui.xmlfragment("view.fragment.Retail.RetailWizard", this);
        if (fragment.constructor === Array) {
            for (var i = 0; i < fragment.length; i++) {
                page.addContent(fragment[i]);
            }
        } else {
            page.addContent(fragment);
        }
    },

    createWizardPrivateLabel: function () {
        var page = this.getView().byId("visitsTourDetailPage");
//        this.removeContentFromPage();
        //        var wizardId = "createPrivateLabelWizard";
        //        this.seekAndDestroyWizard(wizardId);
        var fragment = sap.ui.xmlfragment("view.fragment.PrivateLabel.PrivateLabelWizard", this);
        if (fragment.constructor === Array) {
            for (var i = 0; i < fragment.length; i++) {
                page.addContent(fragment[i]);
            }
        } else {
            page.addContent(fragment);
        }
        //        sap.ui.getCore().byId("createPrivateLabelWizard").discardProgress(sap.ui.getCore().byId("step1PrivateLabel"));
    },

    createWizard1Horeca: function () {
        var page = this.getView().byId("visitsTourDetailPage");
        this.removeContentFromPage();
        var wizardId = "wizard1Horeca";
        this.seekAndDestroyWizard(wizardId);
        var fragment = sap.ui.xmlfragment("view.fragment.Horeca.Wizard1Horeca", this);
        if (fragment.constructor === Array) {
            for (var i = 0; i < fragment.length; i++) {
                page.addContent(fragment[i]);
            }
        } else {
            page.addContent(fragment);
        }
        sap.ui.getCore().byId("wizard1Horeca").discardProgress(sap.ui.getCore().byId("step1Wizard1Horeca"));
    },

    createWizard2Horeca: function () {
        var page = this.getView().byId("visitsTourDetailPage");
//        this.removeContentFromPage();
        wizardId = "wizard2Horeca";
        this.seekAndDestroyWizard(wizardId);
        var fragment = sap.ui.xmlfragment("view.fragment.Horeca.Wizard2Horeca", this);
        if (fragment.constructor === Array) {
            for (var i = 0; i < fragment.length; i++) {
                page.addContent(fragment[i]);
            }
        } else {
            page.addContent(fragment);
        }
        this.tableDataModel = new sap.ui.model.json.JSONModel();

        var dataForTable = {};
        var controlli = [];
        controlli[0] = {
            tipoControllo: "Depurazione",
            descrizione: "Svaporizzare la macchina togliendo aria interna",
            frequenza: "Facoltativo"
        };
        controlli[1] = {
            tipoControllo: "Pressione pompa 8-9 ATM",
            descrizione: "Agire sulla vite del pompante  +/-",
            frequenza: "Obbligatorio per ogni visita "
        };
        controlli[2] = {
            tipoControllo: "Tubi car./scar. acqua macchina e depuratore controllo fascette",
            descrizione: "",
            frequenza: "Obbligatorio per ogni visita"
        };
        controlli[3] = {
            tipoControllo: "Pressione caldaia:0,9 - 1 ATM per MC vecchie, 1 - 1,2 ATM per E98",
            descrizione: "Agire sul pressostato all'interno della macchina sotto il portatazze o sul regolatore del gas  +/-",
            frequenza: "Obbligatorio per ogni visita"
        };
        controlli[4] = {
            tipoControllo: "Otturazione coppe/beccuccio",
            descrizione: "",
            frequenza: "Obbligatorio per ogni visita"
        };
        controlli[5] = {
            tipoControllo: "Livello acqua",
            descrizione: "Basso: aggiungere attraverso la leva su frontale Alto: togliere attraverso rubinetto di scarico",
            frequenza: "Obbligatorio per ogni visita"
        };
        controlli[6] = {
            tipoControllo: "Vasca di recupero",
            descrizione: "Posizionare correttamente e pulizia",
            frequenza: "Obbligatorio per ogni visita"
        };
        controlli[7] = {
            tipoControllo: "Pulizie delle coppe",
            descrizione: "1)Lavaggio coppe e pulizia con paglietta 2)Lavaggio gruppo con filtro cieco e pulicaf",
            frequenza: "Obbligatorio per ogni visita"
        };
        controlli[8] = {
            tipoControllo: "Sale al depuratore",
            descrizione: "Con sale grosso di cucina 1 kg. per dep.   8 lt 2 kg. per dep. 12 lt 1)Scaricare un po' di acqua dal dep. dal rubinetto superiore 2)Togliere il coperchio 3)Inserire il sale 4)Far scorrere l'acqua a flusso lento per circa 30",
            frequenza: "Obbligatorio per ogni visita"
        };
        controlli[9] = {
            tipoControllo: "Doccette e guarnizioni",
            descrizione: "1)Sostituzione con cacciavite 2)Sostituzione con chiave brugola",
            frequenza: "Mensile"
        };

        this.tableDataModel.setProperty("/controlli", controlli);
        this.getView().setModel(this.tableDataModel, "wizardTableModel");
    },

    createNextWizardHoreca: function (wizardnr) {
        var page = this.getView().byId("visitsTourDetailPage");
//        this.removeContentFromPage();
        wizardId = "wizard" + wizardnr + "Horeca";
        this.seekAndDestroyWizard(wizardId);
        var fragment = sap.ui.xmlfragment("view.fragment.Horeca.Wizard" + wizardnr + "Horeca", this);
        if (fragment.constructor === Array) {
            for (var i = 0; i < fragment.length; i++) {
                page.addContent(fragment[i]);
            }
        } else {
            page.addContent(fragment);
        }
        sap.ui.getCore().byId("wizard" + wizardnr + "Horeca").discardProgress(sap.ui.getCore().byId("step1Wizard" + wizardnr + "Horeca"));
    },

    wizard1HorecaCompletedHandler: function (evt) {
        var wizardId = evt.getSource().getId();
        this.seekAndDestroyWizard(wizardId);
        var wizardNr = wizardId.match(/\d+/)[0];
        var nextWizard = parseInt(wizardNr) + 1;
        var nextWizardStr = nextWizard.toString();
        if (nextWizardStr === "2") {
            this.createWizard2Horeca();
        } else {
            this.createNextWizardHoreca(nextWizardStr);
        }
    },

    wizard3HorecaCompletedHandler: function (evt) {
        var wizardId = evt.getSource().getId();
        this.seekAndDestroyWizard(wizardId);
        var wizardNr = wizardId.match(/\d+/)[0];
        var nextWizard = parseInt(wizardNr) + 1;
        var nextWizardStr = nextWizard.toString();
        this.createNextWizardHoreca(nextWizardStr);
    },

    goToNextStep: function (evt) {
        this.seekAndDestroyWizard("wizard2Horeca");
        this.createNextWizardHoreca("3");
    },

    wizard4HorecaCompletedHandler: function (evt) {
        var wizardId = evt.getSource().getId();
        this.seekAndDestroyWizard(wizardId);
        sap.m.MessageBox.show(
            'Invia Questionario',
            sap.m.MessageBox.Icon.INFORMATION,
            "info", [sap.m.MessageBox.Action.OK],
            _.bind(this._okPress, this)
        );
    },

    _okPress: function (evt) {
        if (evt === sap.m.MessageBox.Action.OK) {
            this.router.navTo("emptyVisitsTour");
        }
    },

    wizardRetailCompletedHandler: function (evt) {
        var wizardId = evt.getSource().getId();
        this.seekAndDestroyWizard(wizardId);
        sap.m.MessageBox.show(
            'Invia Questionario',
            sap.m.MessageBox.Icon.INFORMATION,
            "info", [sap.m.MessageBox.Action.OK],
            _.bind(this._okPress, this)
        );
    },

    wizardPrivateLabelCompletedHandler: function (evt) {
        var wizardId = evt.getSource().getId();
        this.seekAndDestroyWizard(wizardId);
        sap.m.MessageBox.show(
            'Invia Questionario',
            sap.m.MessageBox.Icon.INFORMATION,
            "info", [sap.m.MessageBox.Action.OK],
            _.bind(this._okPress, this)
        );
    },

    seekAndDestroyWizard: function (id) {
        if (!!sap.ui.getCore().byId(id))
            sap.ui.getCore().byId(id).destroy();
    },

    removeContentFromPage: function () {
        var page = this.getView().byId("visitsTourDetailPage");
        var content = page.getContent();
        if (!!content && content.length > 0) {
            for (var i = 0; i < content.length; i++) {
                page.removeContent(content[i]);
            }
        }
    },

    onProductPress: function () {
        var page = this.getView().byId("visitsTourDetailPage");
        this.otherInfoDialog = sap.ui.xmlfragment("view.fragment.otherInfoDialog", this);
        page.addDependent(this.otherIngoDialog);
        this.otherInfoDialog.open();
    },

    okPressDialog: function () {
        this.otherInfoDialog.close();
        this.otherInfoDialog.destroy(true);
    }
});
