jQuery.sap.require("model.Tiles");
jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.i18n");
jQuery.sap.require("model.collections.Customers");
jQuery.sap.require("model.Current");
jQuery.sap.require("model.persistence.Storage");
//jQuery.sap.require("model.User");

view.abstract.AbstractController.extend("view.Launchpad", {

    onInit: function () {
        this.router = sap.ui.core.UIComponent.getRouterFor(this);
        this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
        // view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
        this.tileModel = new sap.ui.model.json.JSONModel();

        this.getView().setModel(this.tileModel, "tiles");

        this.userModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.userModel, "userModel");

        if (model.persistence.Storage.session.get("currentCustomer")) {
            model.persistence.Storage.session.remove("currentCustomer");
        }

        if (model.persistence.Storage.session.get("currentChannel"))
            model.persistence.Storage.session.remove("currentChannel");

    },

    handleRouteMatched: function (oEvent) {


        var oParameters = oEvent.getParameters();

        if (oParameters.name !== "launchpad" && oParameters.name !== "launchpadTemp") {
            return;
        }

        var arguments = oEvent.getParameters().arguments;
        var customerId = arguments.customerId;
        var inSession = arguments.inSession;
        var isAgent = arguments.isAgent;
        if (!!customerId && !!inSession && !!isAgent) {
            model.persistence.Storage.session.save("customerSession", true);
            model.collections.Customers.getCustomerById(customerId)
                .then(

                    _.bind(function (result) {

                        this.customer = result;
                        model.persistence.Storage.session.save("currentCustomer", this.customer);
                        model.Current.setCustomer(this.customer);
                        this.customer.setCustomerStatus();
                        this.cssReload();

                    }, this)
                );
        }

        var appModel = this.getView().getModel("appStatus");
        appModel.setProperty("/navBackVisible", false);



        if (model.persistence.Storage.session.get("orderIdParameter"))
            model.persistence.Storage.session.remove("orderIdParameter");

        this.user = model.persistence.Storage.session.get("user");
        if (!this.user) {
            this.user = model.persistence.Storage.local.get("user");
            model.persistence.Storage.session.save("user", this.user);
        }
        this.userModel.setData(this.user);
        this.refreshTiles(this.user);
    },


    onTilePress: function (oEvent) {
        //recupero la tile
        var tilePressed = oEvent.getSource().getBindingContext("tiles").getObject();
        var url = tilePressed.url;

        /******Sessione cliente gestione da app esterna e poi ritono alla launchpad********/

        if (tilePressed.title === model.i18n.getText("CUSTOMER_MANAGEMENT")) {
            model.persistence.Storage.session.remove("currentCustomer");
            this.router.navTo("launchpadTemp", {
                customerId: "12000",
                inSession: "true",
                isAgent: "true"
            });
        } else {
            /***********************************************************************************/

            //verifico se chiudo la sessione cliente
            if (tilePressed.title === model.i18n.getText("CLOSE_SESSION")) {
                model.persistence.Storage.session.remove("currentCustomer");
                model.persistence.Storage.session.remove("customerSession");
                this.refreshTiles(this.user);
            }
            //lancio l'app
            this.launchApp(url);
        }
    },

    onLinkToUserInfoPress: function (evt) {
        this.router.navTo("changePassword");
    },

    launchApp: function (url) {
        this.router.navTo(url);
    },

    //**
    refreshTiles: function (user) {
        var session = sessionStorage.getItem("customerSession") ? model.persistence.Storage.session.get("customerSession") : false;
        var tile = model.Tiles.getMenu(user.type, session);
        this.tileModel.setData(tile);
        this.tileModel.refresh();
    },
    onNavButtonPress: function () {
        this.router.navTo("soLaunchpad");
    }
});
