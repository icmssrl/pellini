jQuery.sap.require("model.Tiles");
jQuery.sap.require("model.ui.LogoTile");
jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.i18n");

//jQuery.sap.require("model.User");

view.abstract.AbstractController.extend("view.SOlaunchpad", {

  onInit: function () {
    this.router = sap.ui.core.UIComponent.getRouterFor(this);
    this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
    // view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
    this.tileModel = new sap.ui.model.json.JSONModel();

    this.getView().setModel(this.tileModel, "tiles");

    this.userModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.userModel, "userModel");


  },
    
  onAfterRendering: function(evt){
    this.user = model.persistence.Storage.session.get("user");

    this.userModel.setData(this.user);
    this.refreshTiles(this.user);
  },

  handleRouteMatched: function (oEvent) {


    var oParameters = oEvent.getParameters();

    if (oParameters.name !== "soLaunchpad") {
      return;
    }
      
    var appModel = this.getView().getModel("appStatus");
      if(appModel.getProperty("/navBackVisible")===true){
          appModel.setProperty("/navBackVisible", false);
      }
//    this.user = model.persistence.Storage.session.get("user");
//
//    this.userModel.setData(this.user);
//    this.refreshTiles(this.user);
  },

  // onLogoutPress: function (oEvent) {
  //   this.doLogout();
  // },
  //
  // doLogout: function () {
  //   //resetto dati
  //   sessionStorage.clear();
  //   this.router.navTo("login");
  // },

  onTilePress: function (oEvent) {
    //recupero la tile
    var tilePressed = oEvent.getSource().getBindingContext("tiles").getObject();
    var url = tilePressed.url;
    model.persistence.Storage.session.save("division", tilePressed.division);

    //lancio l'app
    this.launchApp(url, tilePressed.division);
  },

  onLinkToUserInfoPress: function(evt){
      this.router.navTo("changePassword");
    },

  launchApp: function (url, division) {
      if(url === "launchpad"){ 
    this.router.navTo(url);
      } 
      jQuery.sap.includeStyleSheet("custom/css/custom"+division+".css","custom_style");
  },

  //**
  refreshTiles: function(user)
  {
    var divisions = _.chain(this.user.organizationData).pluck('division').uniq().value();
    var tiles = [];
    for(var i = 0; i<divisions.length; i++)
    {
      tiles.push(model.Tiles.getDivisionTile(divisions[i]));
    }
    this.tileModel.setData(tiles);
    // var session = sessionStorage.getItem("customerSession") ? model.persistence.Storage.session.get("customerSession") : false;
    // var tile = model.Tiles.getMenu(user.type, session);
    // this.tileModel.setData(tile);
    // this.tileModel.refresh();
  }
});



//jQuery.sap.require("model.Tiles");
//jQuery.sap.require("model.ui.LogoTile");
//jQuery.sap.require("view.abstract.AbstractController");
//jQuery.sap.require("model.persistence.Storage");
//jQuery.sap.require("model.i18n");
//
////jQuery.sap.require("model.User");
//
//view.abstract.AbstractController.extend("view.SOlaunchpad", {
//
//  onInit: function () {
//    this.router = sap.ui.core.UIComponent.getRouterFor(this);
//    this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
//    // view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
//    this.tileModel = new sap.ui.model.json.JSONModel();
//
//    this.getView().setModel(this.tileModel, "tiles");
//
//    this.userModel = new sap.ui.model.json.JSONModel();
//    this.getView().setModel(this.userModel, "userModel");
//
//
//  },
//    
//  onAfterRendering: function(evt){
//    this.user = model.persistence.Storage.session.get("user");
//
//    this.userModel.setData(this.user);
//    this.refreshTiles(this.user);
//  },
//
//  handleRouteMatched: function (oEvent) {
//
//
//    var oParameters = oEvent.getParameters();
//
//    if (oParameters.name !== "soLaunchpad") {
//      return;
//    }
////    this.user = model.persistence.Storage.session.get("user");
////
////    this.userModel.setData(this.user);
////    this.refreshTiles(this.user);
//  },
//
//  // onLogoutPress: function (oEvent) {
//  //   this.doLogout();
//  // },
//  //
//  // doLogout: function () {
//  //   //resetto dati
//  //   sessionStorage.clear();
//  //   this.router.navTo("login");
//  // },
//
//  onTilePress: function (oEvent) {
//    //recupero la tile
//    var tilePressed = oEvent.getSource().getBindingContext("tiles").getObject();
//    var url = tilePressed.url;
//    model.persistence.Storage.session.save("division", tilePressed.division);
//
//    //lancio l'app
//    this.launchApp(url);
//  },
//
//  onLinkToUserInfoPress: function(evt){
//      this.router.navTo("changePassword");
//    },
//
//  launchApp: function (url) {
//    this.router.navTo(url);
//  },
//
//  //**
//  refreshTiles: function(user)
//  {
//    var divisions = _.chain(this.user.organizationData).pluck('division').uniq().value();
//    var tiles = [];
//    for(var i = 0; i<divisions.length; i++)
//    {
//      tiles.push(model.Tiles.getDivisionTile(divisions[i]));
//    }
//    this.tileModel.setData(tiles);
//    // var session = sessionStorage.getItem("customerSession") ? model.persistence.Storage.session.get("customerSession") : false;
//    // var tile = model.Tiles.getMenu(user.type, session);
//    // this.tileModel.setData(tile);
//    // this.tileModel.refresh();
//  }
//});




