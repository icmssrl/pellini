jQuery.sap.declare("icms.Component");
jQuery.sap.require("icms.MyRouter");


sap.ui.core.UIComponent.extend("icms.Component", {
	metadata: {
		config: {
			resourceBundle: "custom/i18n/text.properties",

			//puntati dalla classe settings.core [custom/settins]
			settings: {
				lang: "IT",
				isMock:true,
				serverUrl:"sap.blabla:8000"
			},

			//rootView: 'view.App'
		},
		includes : [  //importare css di custom e lib
			"css/custom.css",
			"libs/lodash.js",
			"libs/q.js",
			],
		dependencies: {
			libs: [
				"sap.m",
				"sap.ui.layout"
				]
		},

		routing: {
			config: {
				viewType: "XML",
				viewPath: "view",
				clearTarget: false,
				// targetControl: "app",
				transition: "slide",
				// targetAggregation: "detailPages",
			},

			routes:
			[
				{
					name: "login",
					pattern: "",
					view: "Login",
					viewId: "loginId",
					targetAggregation : "pages",
					targetControl:"app"
				},
				{
					name: "soLaunchpad",
					pattern: "SOlaunchpad",
					view: "SOlaunchpad",
					viewId: "soLaunchpadId",
					targetAggregation:"pages",
					targetControl:"app"
				},

				{
					name: "launchpad",
					pattern: "home/:customerId:/:inSession:/:isAgent:",
					view: "Launchpad",
					viewId: "launchpadId",
					targetAggregation:"pages",
					targetControl:"app"
				},

				{
					name: "launchpadTemp",
					pattern: "homeTemp/:customerId:/:inSession:/:isAgent:",
					view: "Launchpad",
					viewId: "launchpadId",
					targetAggregation:"pages",
					targetControl:"app"
				},
                {
					name: "changePassword",
					pattern: "changePassword",
					view: "ChangePassword",
					viewId: "changePasswordId",
					targetAggregation:"pages",
					targetControl:"app"
				},
                {
					name: "orderList",
					pattern: "orderList",
					view: "OrderList",
					viewId: "orderListId",
					targetAggregation:"pages",
					targetControl:"app"
				},
                {
					name: "orderAnomalyList",
					pattern: "orderAnomalyList",
					view: "OrderAnomalyList",
					viewId: "OrderAnomalyListId",
					targetAggregation:"pages",
					targetControl:"app"
				},
				{
					name: "newOrder",
					pattern: "newOrder",
					view: "OrderCreate",
					viewId: "orderCreateId",
					targetAggregation:"pages",
					targetControl:"app"
				},
                {
					name: "orderInfo",
					pattern: "orderInfo/{id}",
					view: "OrderInfo",
					viewId: "orderInfoId",
					targetAggregation:"pages",
					targetControl:"app"
				},
				{
					pattern:"split",
					name:"splitApp",
					view:"SplitApp",
					viewType:"JS",
					targetControl:"app",
					targetAggregation:"pages",
					subroutes:
					[
						{
							name: "productsList",
							pattern: "products",
							view: "ProductsList",
							viewId: "productsListId",
							targetAggregation:"masterPages",
							targetControl:"splitApp",

							subroutes:
							[
								{
									name: "productDetail",
									pattern: "products/detail/{id}",
									view: "ProductDetail",
									viewId: "productDetailId",
									targetAggregation:"detailPages",
									targetControl:"splitApp",

								},
								{
									name: "empty",
									pattern: "products/empty",
									view: "Empty",
									viewId: "emptyId",
									targetAggregation:"detailPages",
									targetControl:"splitApp"
								}
								
							]
					 	},
                        {
							name: "visitsTourMaster",
							pattern: "visitsTourMaster",
							view: "VisitsTourMaster",
							viewId: "visitsTourMasterId",
							targetAggregation:"masterPages",
							targetControl:"splitApp",

							subroutes:
							[
								{
									name: "visitsTourDetail",
									pattern: "sellingPointSurvey/detail/{channel}/{sellingPoint}",
									view: "VisitsTourDetail",
									viewId: "visitsTourDetailId",
									targetAggregation:"detailPages",
									targetControl:"splitApp",

								},
								{
									name: "emptyVisitsTour",
									pattern: "emptyVisitsTour",
									view: "Empty",
									viewId: "emptyId",
									targetAggregation:"detailPages",
									targetControl:"splitApp"
								}
								
							]
					 	},
						{
							name: "shippment.shippmentsList",
							pattern: "shippments",
							view: "shippment.ShippmentsList",
							viewId: "ShippmentsListId",
							targetAggregation:"masterPages",
							targetControl:"splitApp",

							subroutes:
							[
								{
									name: "shippment.shippmentDetail",
									pattern: "shippment/detail/{id}",
									view: "shippment.ShippmentDetail",
									viewId: "ShippmentDetailId",
									targetAggregation:"detailPages",
									targetControl:"splitApp",

								},
								{
									name: "noDataSplitDetail",
									pattern: "shippment/empty",
									view: "NoDataSplitDetail",
									viewId: "noDataSplitDetailId",
									targetAggregation:"detailPages",
									targetControl:"splitApp"
								}
								
							]
					 	},
                        {
							name: "readOnlyHierarchy",
							pattern: "readOnlyHierarchy",
							view: "ReadOnlyHierarchy",
							viewId: "readOnlyHierarchyId",
							targetAggregation:"masterPages",
							targetControl:"splitApp",

							subroutes:
							[
								{
									name: "productRODetail",
									pattern: "productsRO/detail/{id}",
									view: "ProductRODetail",
									viewId: "productRODetailId",
									targetAggregation:"detailPages",
									targetControl:"splitApp",

								},
								{
									name: "emptyROHierarchy",
									pattern: "roHierarchy/empty",
									view: "Empty",
									viewId: "emptyId",
									targetAggregation:"detailPages",
									targetControl:"splitApp"
								}
								
							]
					 	},
                        {
							name: "historyCartsList",
							pattern: "historyCarts",
							view: "HistoryCartsList",
							viewId: "historyCartsListId",
							targetAggregation:"masterPages",
							targetControl:"splitApp",

							subroutes:
							[
								{
									name: "historyCartsDetail",
									pattern: "historyCarts/detail/{id}",
									view: "HistoryCartsDetail",
									viewId: "HistoryCartsDetailId",
									targetAggregation:"detailPages",
									targetControl:"splitApp"

								},
								{
									name: "emptyHistoryCarts",
									pattern: "historyCarts/empty",
									view: "Empty",
									viewId: "emptyId",
									targetAggregation:"detailPages",
									targetControl:"splitApp"
								}

							]
					 	},
                        {
							name: "planningMaster",
							pattern: "plannings",
							view: "PlanningMaster",
							viewId: "planningMasterId",
							targetAggregation:"masterPages",
							targetControl:"splitApp",
							subroutes:
							[
								{
									name: "planningDetail",
									pattern: "plannings/detail/{id}",
									view: "PlanningDetail",
									viewId: "PlanningDetailId",
									targetAggregation:"detailPages",
									targetControl:"splitApp"

								},
								{
									name: "emptyPlanningDetail",
									pattern: "emptyPlannings",
									view: "EmptyPlannings",
									viewId: "emptyPlanningDetail",
									targetAggregation:"detailPages",
									targetControl:"splitApp"
								}
							]
					 	}
					]
				},
				{
					name: "cartFullView",
					pattern: "cart/detail",
					view: "CartFull",
					viewId: "cartFullId",
					targetAggregation:"pages",
					targetControl:"app",
					transition:"flip"
				},
				{
					name: "praticheAnomale.ResoConNC",
					pattern: "resoConNotaCredito/consegnaNr{shippmentId}",
					view: "praticheAnomale.ResoConNC",
					viewId: "resoConNCId",
					targetAggregation:"pages",
					targetControl:"app"

				},
				{
					name: "praticheAnomale.ResoSenzaNC",
					pattern: "resoSenzaNotaCredito/consegnaNr{shippmentId}",
					view: "praticheAnomale.ResoSenzaNC",
					viewId: "resoSenzaNCId",
					targetAggregation:"pages",
					targetControl:"app"

				},
				{
					name: "praticheAnomale.RichiestaRicambi",
					pattern: "richiestaRicambi/consegnaNr{shippmentId}",
					view: "praticheAnomale.RichiestaRicambi",
					viewId: "richiestaRicambiId",
					targetAggregation:"pages",
					targetControl:"app"

				},
				{
					name: "praticheAnomale.MancanzaMerce",
					pattern: "mancanzaMerce/consegnaNr{shippmentId}",
					view: "praticheAnomale.MancanzaMerce",
					viewId: "mancanzaMerceId",
					targetAggregation:"pages",
					targetControl:"app"

				},
				{
					name: "newCustomer",
					pattern: "customer/new",
					view: "CustomerCreate",
					viewId: "newCustomerId",
					targetAggregation:"pages",
					targetControl:"app"

				},
                {
					name: "calendar",
					pattern: "calendar",
					view: "Calendar",
					viewId: "calendarId",
					targetAggregation:"pages",
					targetControl:"app"

				},
                {
					name: "appointments",
					pattern: "appointments/{state}/{data}",
					view: "Appointments",
					viewId: "appointmentsId",
					targetAggregation:"pages",
					targetControl:"app"

				},
                {
					name: "planning",
					pattern: "planning",
					view: "Planning",
					viewId: "planningId",
					targetAggregation:"pages",
					targetControl:"app"

				}
			]

	}
},

	/**
	 * !!! The steps in here are sequence dependent !!!
	 */
	init: function() {

		//il ns icms va cambiato a seconda del nome app e nome cliente
		jQuery.sap.registerModulePath("icms", "custom");
		jQuery.sap.registerModulePath("view", "custom/view");
		jQuery.sap.registerModulePath("model", "custom/model");
		jQuery.sap.registerModulePath("utils", "custom/utils");

		// 2. call overridden init (calls createContent)
		sap.ui.core.UIComponent.prototype.init.apply(this, arguments);
        
		var oI18nModel = new sap.ui.model.resource.ResourceModel({
			bundleUrl: [this.getMetadata().getConfig().resourceBundle]
		});
		this.setModel(oI18nModel, "i18n");
        if (sessionStorage.getItem("language")){
            sap.ui.getCore().getConfiguration().setLanguage(sessionStorage.getItem("language"));
        }
        //sap.ui.getCore().setModel(oI18nModel, "i18n");
       
		// 3a. monkey patch the router
		jQuery.sap.require("sap.m.routing.RouteMatchedHandler");
		var router = this.getRouter();
		router.myNavBack = icms.MyRouter.myNavBack;
		router.myNavToWithoutHash = icms.MyRouter.myNavToWithoutHash;
		icms.MyRouter.router = router;

		this.routeHandler = new sap.m.routing.RouteMatchedHandler(router);
		router.initialize();


		// set device model
		var oDeviceModel = new sap.ui.model.json.JSONModel({
			isTouch : sap.ui.Device.support.touch,
			isNoTouch : !sap.ui.Device.support.touch,
			isPhone : sap.ui.Device.system.phone,
			isNoPhone : !sap.ui.Device.system.phone,
			listMode : (sap.ui.Device.system.phone) ? "None" : "SingleSelectMaster",
			listItemType : (sap.ui.Device.system.phone) ? "Active" : "Inactive"
		});
		oDeviceModel.setDefaultBindingMode("OneWay");
		this.setModel(oDeviceModel, "device");

		var appStatusModel  = new sap.ui.model.json.JSONModel();
        appStatusModel.setProperty("/navBackVisible", false);
		this.setModel(appStatusModel,"appStatus" );

	},

	destroy: function() {
		if (this.routeHandler) {
			this.routeHandler.destroy();
		}

		// call overridden destroy
		sap.ui.core.UIComponent.prototype.destroy.apply(this, arguments);
	},

	createContent: function() {
		// create root view
		var oView = sap.ui.view({
			id: "mainApp",
			viewName: "view.App",
			type: "JS",
			viewData: {
				component: this
			}
		});

		return oView;
	}
});
